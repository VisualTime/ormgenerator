﻿using Robotics.DataLayer.ORM.Field;
using System;
using System.Text;

namespace ORMGenerator.Random
{
    public class RandomField
    {
        private System.Random rnd;

        public RandomField()
        {
            rnd = new System.Random();
        }

        /// <summary>
        /// Generate Random values for ORMIntegerFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMField pField)
        {
            switch (pField.ClrType.ToString())
            {
                case "System.Int32":
                    var itemInt = pField as ORMIntegerField;
                    GenerateRandom(ref itemInt);
                    break;
                case "System.Int16":
                    var itemSmallInt = pField as ORMSmallIntegerField;
                    GenerateRandom(ref itemSmallInt);
                    break;
                case "System.Int64":
                    var itemBigInt = pField as ORMBigIntegerField;
                    GenerateRandom(ref itemBigInt);
                    break;
                case "System.DateTime":
                    var itemDate = pField as ORMDateTimeField;
                    GenerateRandom(ref itemDate);
                    break;
                case "System.Byte":
                    var itemByte = pField as ORMByteField;
                    GenerateRandom(ref itemByte);
                    break;
                case "System.Byte[]":
                    var itemBinary = pField as ORMBinaryField;
                    GenerateRandom(ref itemBinary);
                    break;
                case "System.String":
                    var itemString = pField as ORMStringField;
                    var size = itemString.Size > 0 ? itemString.Size : 8;
                    GenerateRandom(ref itemString, size);
                    break;
                case "System.Double":
                    var itemDouble = pField as ORMFloatField;
                    GenerateRandom(ref itemDouble);
                    break;
                case "System.Decimal":
                    var itemDecimal = pField as ORMDecimalField;
                    GenerateRandom(ref itemDecimal);
                    break;
                case "System.Boolean":
                    var itemBoolean = pField as ORMBooleanField;
                    GenerateRandom(ref itemBoolean);
                    break;
                case "System.Guid":
                    var itemGuid = pField as ORMGuidField;
                    GenerateRandom(ref itemGuid);
                    break;
            }
        }

        /// <summary>
        /// Generate Random values for ORMIntegerFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMIntegerField pField)
        {
            pField.AsString = rnd.Next(int.MinValue, int.MaxValue).ToString();
        }

        /// <summary>
        /// Generate Random values for ORMBigIntegerFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMBigIntegerField pField)
        {
            var buffer = new byte[sizeof(long)];
            rnd.NextBytes(buffer);
            pField.AsString = BitConverter.ToInt64(buffer, 0).ToString();
        }

        /// <summary>
        /// Generate Random values for ORMSmallIntegerFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMSmallIntegerField pField)
        {
            pField.AsString = rnd.Next(short.MinValue, short.MaxValue).ToString();
        }

        /// <summary>
        /// Generate Random values for ORMBinaryFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMBinaryField pField)
        {
            var buffer = new byte[rnd.Next(1, 100)];
            rnd.NextBytes(buffer);
            pField.Value = buffer;
        }

        /// <summary>
        /// Generate Random values for ORMByteFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMByteField pField)
        {
            pField.AsString = rnd.Next(byte.MinValue, byte.MaxValue).ToString();
        }

        /// <summary>
        /// Generate Random values for ORMBooleanFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMBooleanField pField)
        {
            var result = rnd.Next(1, 10);
            pField.Value = result < 5;
        }

        /// <summary>
        /// Generate Random values for ORMDateTimeFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMDateTimeField pField)
        {
            pField.Value = new DateTime(rnd.Next(1900, 2010), rnd.Next(1, 12), rnd.Next(1, 28), rnd.Next(0, 23), rnd.Next(0, 60), rnd.Next(0, 60));
        }

        /// <summary>
        /// Generate Random values for ORMDecimalFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMDecimalField pField)
        {
            //The low 32 bits of a 96-bit integer. 
            int lo = rnd.Next(int.MinValue, int.MaxValue);
            //The middle 32 bits of a 96-bit integer. 
            int mid = rnd.Next(int.MinValue, int.MaxValue);
            //The high 32 bits of a 96-bit integer. 
            int hi = rnd.Next(int.MinValue, int.MaxValue);
            //The sign of the number; 1 is negative, 0 is positive. 
            bool isNegative = (rnd.Next(2) == 0);
            //A power of 10 ranging from 0 to 28. 
            byte scale = Convert.ToByte(rnd.Next(29));

            pField.Value = new Decimal(lo, mid, hi, isNegative, scale);

        }

        /// <summary>
        /// Generate Random values for ORMFloatFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMFloatField pField)
        {
            pField.Value = rnd.NextDouble();
        }

        /// <summary>
        /// Generate Random values for ORMGuidFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMGuidField pField)
        {
            pField.Value = Guid.NewGuid();
        }

        /// <summary>
        /// Generate Random values for ORMStringFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMStringField pField)
        {
            GenerateRandom(ref pField, 8);
        }
        /// <summary>
        /// Generate Random values for ORMStringFields
        /// </summary>
        /// <param name="pField"></param>
        /// <returns></returns>
        public void GenerateRandom(ref ORMStringField pField, int pSize)
        {
            var result = new StringBuilder();
            var count = 0;

            while (count < pSize)
            {
                result.Append(Guid.NewGuid().ToString().Replace("-", string.Empty));
                count = result.Length;
            }

            pField.Value = result.ToString().Substring(0,pSize);
        }
    }
}
