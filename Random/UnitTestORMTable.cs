﻿using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace ORMGenerator
{
    [TestFixture]
    public class UnitTestORMTable
    {
        private RandomField rnd;

        public UnitTestORMTable()
        {
            rnd = new RandomField();
        }

        [Test]
        public void RandomIntegerField()
        {
            var table = new ORMAbsenceTrackingBase();
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }
        }
    }
}
