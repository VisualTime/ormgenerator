﻿using NUnit.Framework;
using Robotics.DataLayer.ORM.Field;

namespace ORMGenerator.Random
{
    [TestFixture]
    public class UnitTestRandom
    {
        [Test]
        public void RandomIntegerField()
        {
            var random = new RandomField();
            var field = new ORMIntegerField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");
            Assert.IsTrue(field.Value >= int.MinValue, "Field Integer Random: IsNull");
            Assert.IsTrue(field.Value <= int.MaxValue, "Field Integer Random: IsNull");

        }

        [Test]
        public void RandomBigIntegerField()
        {
            var random = new RandomField();
            var field = new ORMBigIntegerField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");
            Assert.IsTrue(field.Value >= long.MinValue, "Field Integer Random: IsNull");
            Assert.IsTrue(field.Value <= long.MaxValue, "Field Integer Random: IsNull");

        }

        [Test]
        public void RandomSmallIntegerField()
        {
            var random = new RandomField();
            var field = new ORMSmallIntegerField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");
            Assert.IsTrue(field.Value >= short.MinValue, "Field Integer Random: IsNull");
            Assert.IsTrue(field.Value <= short.MaxValue, "Field Integer Random: IsNull");

        }

        [Test]
        public void RandomBinaryField()
        {
            var random = new RandomField();
            var field = new ORMBinaryField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");

        }

        [Test]
        public void RandomByteField()
        {
            var random = new RandomField();
            var field = new ORMByteField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");
            Assert.IsTrue(field.Value >= byte.MinValue, "Field Integer Random: IsNull");
            Assert.IsTrue(field.Value <= byte.MaxValue, "Field Integer Random: IsNull");

        }

        [Test]
        public void RandomBooleanField()
        {
            var random = new RandomField();
            var field = new ORMBooleanField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");

        }

        [Test]
        public void RandomDateTimeField()
        {
            var random = new RandomField();
            var field = new ORMDateTimeField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");

        }

        [Test]
        public void RandomDecimalField()
        {
            var random = new RandomField();
            var field = new ORMDecimalField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");

        }

        [Test]
        public void RandomFloatField()
        {
            var random = new RandomField();
            var field = new ORMFloatField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");

        }

        [Test]
        public void RandomGuidField()
        {
            var random = new RandomField();
            var field = new ORMGuidField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");

        }

        [Test]
        public void RandomStringField()
        {
            var random = new RandomField();
            var field = new ORMStringField("Total");
            random.GenerateRandom(ref field);
            Assert.IsTrue(field.IsModified == true, "Field Integer Random: Modified");
            Assert.IsTrue(field.IsNull == false, "Field Integer Random: IsNull");

        }
    }
}
