using System;
using Robotics.DataLayer.ORM.Database;
using Robotics.DataLayer.ORM.Field;
using Robotics.DataLayer.ORM.Query;
using Robotics.DataLayer.ORM.Record;

namespace Robotics.DataLayer.ORM.Table
{
	#region ORMEmployeesRecord
	/// <summary>
	/// Class to store the data information for a Employees record
	/// </summary>
	public class ORMEmployeesRecord:ORMRecordBase
	{	
		#region Variables
		public ORMIntegerField ID;
		public ORMStringField NAME;
		public ORMStringField TYPE;
		public ORMSmallIntegerField IDACCESSGROUP;
		public ORMBooleanField ALLOWQUERIESONTERMINAL;
		public ORMBooleanField ALLOWQUERYACCRUALSONTERMINAL;
		public ORMBooleanField ALLOWQUERYSHIFTSONTERMINAL;
		public ORMBooleanField ALIASTYPE;
		public ORMStringField ALIAS;
		public ORMStringField PIN;
		public ORMBinaryField IMAGE;
		public ORMBinaryField BIOMETRICDATA;
		public ORMDateTimeField BIOMETRICTIMESTAMP;
		public ORMBooleanField ALLOWBIOMETRIC;
		public ORMBooleanField ALLOWCARDS;
		public ORMBooleanField ALLOWWEB;
		public ORMStringField WEBLOGIN;
		public ORMStringField WEBPASSWORD;
		public ORMBooleanField WEBINTEGRATEDSECURITY;
		public ORMIntegerField BIOMETRICIDTERMINAL;
		public ORMStringField USR_HORARIO;
		public ORMStringField USR_CATEGORIA;
		public ORMStringField WEBWINDOWSSESSION;
		public ORMIntegerField BIOMETRICID;
		public ORMBooleanField ALLOWVISITS;
		public ORMBooleanField ALLOWVISITSPLAN;
		public ORMBooleanField ALLOWVISITSADMIN;
		public ORMBooleanField ALLOWVISITSCONTROL;
		public ORMBooleanField ALLOWCARDPLUSBIO;
		public ORMBooleanField ALLOWBIOPRIORITY;
		public ORMBooleanField MACHINESCONTROLLER;
		public ORMBooleanField ALLOWQUERYMOVESONTERMINAL;
		public ORMStringField TERMINALQUERYPERIOD;
		public ORMBooleanField ALLOWREQUESTSONTERMINAL;
		public ORMBooleanField ALLOWREQUESTLEAVE;
		public ORMBooleanField ALLOWREQUESTSHIFT;
		public ORMSmallIntegerField HOLIDAYSSHIFT;
		public ORMBooleanField ALLOWREQUESTPROGRAMMEDABS;
		public ORMBooleanField ALLOWREQUESTANTICIPATEDINC;
		public ORMBooleanField ALLOWREQUESTMOVE;
		public ORMBooleanField ALLOWREQUESTEXTERNALJOB;
		public ORMBooleanField ALLOWREQUESTSHIFTCHANGE;
		public ORMBooleanField ALLOWREQUESTMOVEREASON;
		public ORMBooleanField ATTCONTROLLED;
		public ORMBooleanField ACCCONTROLLED;
		public ORMBooleanField JOBCONTROLLED;
		public ORMBooleanField EXTCONTROLLED;
		public ORMBooleanField RISKCONTROLLED;
		public ORMBooleanField ACTIVEDIRECTORY;
		public ORMIntegerField HIGHLIGHTCOLOR;
		#endregion

		#region Constructor
		/// <summary>
		/// Class constructor
		/// </summary>
		public ORMEmployeesRecord():base("Employees")
		{
			ID = CreateIntegerField("ID");
			NAME = CreateStringField("NAME",500);
			TYPE = CreateStringField("TYPE",1);
			IDACCESSGROUP = CreateSmallIntegerField("IDACCESSGROUP");
			ALLOWQUERIESONTERMINAL = CreateBooleanField("ALLOWQUERIESONTERMINAL");
			ALLOWQUERYACCRUALSONTERMINAL = CreateBooleanField("ALLOWQUERYACCRUALSONTERMINAL");
			ALLOWQUERYSHIFTSONTERMINAL = CreateBooleanField("ALLOWQUERYSHIFTSONTERMINAL");
			ALIASTYPE = CreateBooleanField("ALIASTYPE");
			ALIAS = CreateStringField("ALIAS",50);
			PIN = CreateStringField("PIN",4);
			IMAGE = CreateBinaryField("IMAGE");
			BIOMETRICDATA = CreateBinaryField("BIOMETRICDATA");
			BIOMETRICTIMESTAMP = CreateDateTimeField("BIOMETRICTIMESTAMP");
			ALLOWBIOMETRIC = CreateBooleanField("ALLOWBIOMETRIC");
			ALLOWCARDS = CreateBooleanField("ALLOWCARDS");
			ALLOWWEB = CreateBooleanField("ALLOWWEB");
			WEBLOGIN = CreateStringField("WEBLOGIN",50);
			WEBPASSWORD = CreateStringField("WEBPASSWORD",50);
			WEBINTEGRATEDSECURITY = CreateBooleanField("WEBINTEGRATEDSECURITY");
			BIOMETRICIDTERMINAL = CreateIntegerField("BIOMETRICIDTERMINAL");
			USR_HORARIO = CreateStringField("USR_HORARIO",16);
			USR_CATEGORIA = CreateStringField("USR_CATEGORIA",16);
			WEBWINDOWSSESSION = CreateStringField("WEBWINDOWSSESSION",50);
			BIOMETRICID = CreateIntegerField("BIOMETRICID");
			ALLOWVISITS = CreateBooleanField("ALLOWVISITS");
			ALLOWVISITSPLAN = CreateBooleanField("ALLOWVISITSPLAN");
			ALLOWVISITSADMIN = CreateBooleanField("ALLOWVISITSADMIN");
			ALLOWVISITSCONTROL = CreateBooleanField("ALLOWVISITSCONTROL");
			ALLOWCARDPLUSBIO = CreateBooleanField("ALLOWCARDPLUSBIO");
			ALLOWBIOPRIORITY = CreateBooleanField("ALLOWBIOPRIORITY");
			MACHINESCONTROLLER = CreateBooleanField("MACHINESCONTROLLER");
			ALLOWQUERYMOVESONTERMINAL = CreateBooleanField("ALLOWQUERYMOVESONTERMINAL");
			TERMINALQUERYPERIOD = CreateStringField("TERMINALQUERYPERIOD",100);
			ALLOWREQUESTSONTERMINAL = CreateBooleanField("ALLOWREQUESTSONTERMINAL");
			ALLOWREQUESTLEAVE = CreateBooleanField("ALLOWREQUESTLEAVE");
			ALLOWREQUESTSHIFT = CreateBooleanField("ALLOWREQUESTSHIFT");
			HOLIDAYSSHIFT = CreateSmallIntegerField("HOLIDAYSSHIFT");
			ALLOWREQUESTPROGRAMMEDABS = CreateBooleanField("ALLOWREQUESTPROGRAMMEDABS");
			ALLOWREQUESTANTICIPATEDINC = CreateBooleanField("ALLOWREQUESTANTICIPATEDINC");
			ALLOWREQUESTMOVE = CreateBooleanField("ALLOWREQUESTMOVE");
			ALLOWREQUESTEXTERNALJOB = CreateBooleanField("ALLOWREQUESTEXTERNALJOB");
			ALLOWREQUESTSHIFTCHANGE = CreateBooleanField("ALLOWREQUESTSHIFTCHANGE");
			ALLOWREQUESTMOVEREASON = CreateBooleanField("ALLOWREQUESTMOVEREASON");
			ATTCONTROLLED = CreateBooleanField("ATTCONTROLLED");
			ACCCONTROLLED = CreateBooleanField("ACCCONTROLLED");
			JOBCONTROLLED = CreateBooleanField("JOBCONTROLLED");
			EXTCONTROLLED = CreateBooleanField("EXTCONTROLLED");
			RISKCONTROLLED = CreateBooleanField("RISKCONTROLLED");
			ACTIVEDIRECTORY = CreateBooleanField("ACTIVEDIRECTORY");
			HIGHLIGHTCOLOR = CreateIntegerField("HIGHLIGHTCOLOR");

			vPKExplicit =true;

			PrimaryKeyList.Add(ID);
			
		}
		#endregion

		#region Destructor
		/// <summary>
		/// Override Method for free all resources
		/// Controls that execute this process only one time and stop, if is necessary, the call of destructor by Garbage Collector
		/// </summary>
		/// <param name="pDisposable">boolean that indicates if free managed resources</param>
		protected override void Dispose(bool pDisposable)
		{
			if (!vDisposed)
			{
				//call father Dispose
				base.Dispose(true);
				GC.SuppressFinalize(this);
			}
		}
		#endregion
	}
	#endregion

	#region ORMEmployeesRecordSet
	/// <summary>
	/// Class to create a list of Employees records
	/// </summary>
	public class ORMEmployeesRecordSet:ORMRecordsetBase<ORMEmployeesRecord>
	{
		#region CreateRecord
		/// <summary>
		/// Creates a ORMEmployees and adds the object to the list
		/// </summary>
		public ORMEmployeesRecord CreateRecord()
		{
			this.Add(new ORMEmployeesRecord());
			return this[this.Count - 1];
		}
		#endregion
	}
	#endregion

	#region ORMEmployeesBase
	/// <summary>
	/// Class base for basic operations with Employees records
	/// </summary>
	public class ORMEmployeesBase : DBTableBase
	{
		#region Constructor
		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="pDBManager">Database manager who creates the object</param>
		public ORMEmployeesBase() : base("Employees")
		{
			PKType=DBTableBase.PK_TYPE_EXPLICIT;
		}
		#endregion

		#region Create
		/// <summary>
		/// Creates a ORMEmployees record
		/// </summary>
		/// <returns>ORMEmployees record</returns>
        public ORMEmployeesRecord CreateRecord()
        {
            ORMEmployeesRecord vRecord;
            vRecord = new ORMEmployeesRecord();

			return vRecord;
		}
		/// <summary>
		/// Create a list of ORMEmployees record
		/// </summary>
		/// <returns>List of ORMEmployees record</returns>
		public ORMEmployeesRecordSet CreateRecordSet()
		{
			ORMEmployeesRecordSet vRecordSet;
			vRecordSet = new ORMEmployeesRecordSet();

			return vRecordSet;
		}
		#endregion

		//public functions

		#region Insert
		/// <summary>
		/// Insert a tblEmployees record to the table. Use a database resource
		/// </summary>
		/// <param name="pORMEmployeesRecord">Record to insert</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMEmployeesRecord pORMRecord, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMInsertQuery vQuery;

			vTrace.TraceVerbose(string.Format("Enter ORMEmployeesBase.Insert: {0}", TraceRecord(pORMRecord)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery = new ORMInsertQuery();

						try
						{
							vQuery.InsertInto(this).Fields(pORMRecord);
							vQuery.Execute(pDBConnection);
						}
						catch (Exception ex)
						{
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMEmployeesBase.Insert: {0}", ex.Message));
						}
						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Insert: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Insert a tblEmployees record to the table. Create a database resource
		/// </summary>
		/// <param name="pORMRecord">Record to insert</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMEmployeesRecord pORMRecord)
		{
			return Insert(pORMRecord, null);
		}
		/// <summary>
		/// Insert the tblEmployees records to the table. Use a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMEmployeesRecordSet pDBRecordSet,DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			bool TransactionOK;
			ORMEmployeesRecord vDBRecord;
			bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Insert: {0}",  pDBRecordSet.Count.ToString()));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						TransactionOK = pDBConnection.StartTransaction();
						try
						{
							for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
							{
								vDBRecord=pDBRecordSet[vIndex];
								vResult = Insert(vDBRecord, pDBConnection);

								if (vResult != ErrorCode.NO_ERROR) // Exit for.
								{
									break;
								}
							}
						}
						finally
						{
							if (TransactionOK)
							{
								pDBConnection.EndTransaction(vResult == ErrorCode.NO_ERROR);
							}
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Insert: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Insert the tblEmployees records to the table. Create a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record to insert</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMEmployeesRecordSet pDBRecordSet)
		{
			return Insert(pDBRecordSet, null);
		}
		#endregion
		
		#region Get
		/// <summary>
		/// Select records to the table Employees. Select by primary key using Record. Use a database resource
		/// </summary>
		/// <param name="pORMRecord">Record to get</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMEmployeesRecord pORMRecord, ORMField[] pSelectFields, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMSelectQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Get: {0}", TraceFields(pORMRecord.PrimaryKeyList)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						if (pORMRecord.PrimaryKeyNotNull()) // Primary key constraint.
						{
							vORMFilter = ORMRecordToORMFilter(pORMRecord, true);
							vQuery = CreateSelect();

							try
							{
								//Select all records or a specific list of fields
								if (ReferenceEquals(pSelectFields,null) || pSelectFields.Length==0)
								{
									vQuery.Select(this);
								}
								else
								{
									vQuery.Select(pSelectFields);
								}
								vQuery.From(this).Where(vORMFilter, false);

								vQuery.Open(pDBConnection);
								if (vQuery.Next())
								{
									vQuery.GetRecordValues(pORMRecord);
								}
								else
								{
									vResult = ErrorCode.ErrorDatabase.DBF_RECORD_NOT_EXISTS;
								}

							}
							catch (Exception ex)
							{ 
								vResult = ErrorCode.ERROR_EXCEPTION;
								vTrace.TraceException(string.Format("Exception ORMEmployeesBase.Get: {0}", ex.Message));
							}

							vORMFilter.Dispose();
							vORMFilter=null;
							vQuery.Dispose();
							vQuery=null;
						}
						else
						{
							vResult = ErrorCode.ErrorDatabase.DBF_PRIMARY_KEY_IS_NULL;
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Get: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Select records to the table Employees. Select by primary key using Record. Create a database resource
		/// </summary>
		/// <param name="pORMRecord">Record to get</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMEmployeesRecord pORMRecord, ORMField[] pSelectFields)
		{
			DBConnection vDBConnection = null;

			return Get(pORMRecord, pSelectFields, vDBConnection);
		}
		/// <summary>
		/// Select records to the table Employees. Select by primary key using RecordSet. Use a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to get the records</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMEmployeesRecordSet pDBRecordSet, ORMField[] pSelectFields, DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			ORMEmployeesRecord vDBRecord;
			bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Get: {0}",  pDBRecordSet.Count.ToString()));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
						{
							vDBRecord=pDBRecordSet[vIndex];
							vResult = Get(vDBRecord, pSelectFields, pDBConnection);

							if (vResult != ErrorCode.NO_ERROR) // Exit for.
							{
								break;
							}
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Get: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Select records to the table Employees. Select by primary key using RecordSet. Create a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to get the records</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMEmployeesRecordSet pDBRecordSet, ORMField[] pSelectFields)
		{
			return Get(pDBRecordSet, pSelectFields, null);
		}
		/// <summary>
		/// Select records to the table Employees. Select by a filter using Record with order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMEmployeesRecord pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMEmployeesRecordSet pDBRecordSet,DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;
			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			vResult = Get(vORMFilter, pSelectFields, pOrderFields, GetOrderByArray(pDescendent,pOrderFields), pDBRecordSet, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Select records to the table Employees. Select by a filter using Record with order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMEmployeesRecord pORMFilter, ORMField[] pSelectFields,ORMField[] pOrderFields,
			bool pDescendent, ORMEmployeesRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table Employees without order. Select by a filter using Record without order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMEmployeesRecord pORMFilter, ORMField[] pSelectFields,
			ORMEmployeesRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			return Get(pORMFilter, pSelectFields, null, false, pDBRecordSet, pDBConnection);
		}
		/// <summary>
		/// Select records to the table Employees without order. Select by a filter using Record without order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMEmployeesRecord pORMFilter, ORMField[] pSelectFields, ORMEmployeesRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table Employees. Select by a filter with order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array with Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMEmployeesRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			ORMEmployeesRecord vDBRecord;
			bool vDBNotAssigned;
			ORMSelectQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Get: {0}",  TraceFilter(pORMFilter)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery = CreateSelect();
						try
						{
							//Select all records or a specific list of fields
							if (ReferenceEquals(pSelectFields,null) || pSelectFields.Length==0)
							{
								vQuery.Select(this);
							}
							else
							{
								vQuery.Select(pSelectFields);
							}
							vQuery.From(this).Where(pORMFilter, false).OrderBy(pOrderFields,pDescendent);

							vQuery.Open(pDBConnection);
							while (vQuery.Next())
							{
								vDBRecord=pDBRecordSet.CreateRecord();
								vQuery.GetRecordValues(vDBRecord);
							}
						}
						catch (Exception ex)
						{ 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMEmployeesBase.Get: {0}", ex.Message));
						}

						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Get: {0}",   ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Select records to the table Employees. Select by a filter with order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMEmployeesRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pOrderFields, GetOrderByArray(pDescendent, pOrderFields), pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table Employees. Select by a filter with order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMEmployeesRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table Employees without order. Select by a filter without order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter,ORMField[] pSelectFields,
			ORMEmployeesRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			return Get(pORMFilter, pSelectFields, null, null, pDBRecordSet, pDBConnection);
		}
		/// <summary>
		/// Select records to the table Employees without order. Select by a filter without order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields,
			ORMEmployeesRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table Employees. Select all records with order by. Use a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMEmployeesRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			ORMFilter vORMFilter;

			vORMFilter = CreateFilter();
			vResult = Get(vORMFilter, pSelectFields, pOrderFields, GetOrderByArray(pDescendent, pOrderFields), pDBRecordSet, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Select records to the table Employees. Select all records with order by. Use a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMEmployeesRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			ORMFilter vORMFilter;

			vORMFilter = CreateFilter();
			vResult = Get(vORMFilter, pSelectFields, pOrderFields, pDescendent, pDBRecordSet, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Select records to the table Employees. Select all records with order by. Create a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMEmployeesRecordSet pDBRecordSet)
		{
			return Get(pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table Employees. Select all records with order by. Create a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMEmployeesRecordSet pDBRecordSet)
		{
			return Get(pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table Employees. Select all records without order by. Use a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMField[] pSelectFields, ORMEmployeesRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			return Get(pSelectFields, null, false, pDBRecordSet, pDBConnection);
		}
		/// <summary>
		/// Select records to the table Employees. Select all records without order by. Create a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMField[] pSelectFields, ORMEmployeesRecordSet pDBRecordSet)
		{
			return Get(pSelectFields, pDBRecordSet, null);
		}
        /// <summary>
        /// Select records to the view Employees. Select owner database select query. Use a database resource
        /// </summary>
        /// <param name="pQuery">Query to execute</param>
        /// <param name="pSelectFields">Fields for the selection</param>
        /// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
        /// <param name="pDBConnection">Database resource to use</param>
        /// <returns>The result of execute get process</returns>
        public int Get(ORMSelectQuery pQuery, ORMField[] pSelectFields, ORMEmployeesRecordSet pDBRecordSet, DBConnection pDBConnection)
        {
        	int vResult =  ErrorCode.NO_ERROR;
        	ORMEmployeesRecord vDBRecord;
        	bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Get: {0}",  TraceFilter(pQuery.ORMFilter)));

        	vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

        	if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
        	{
        		try
        		{
        			if (pDBConnection.Connected) // Check if database is connected.
        			{
                        try
                        {
                            pQuery.Open(pDBConnection);
                            while (pQuery.Next())
                            {
                                vDBRecord=pDBRecordSet.CreateRecord();
                                pQuery.GetRecordValues(vDBRecord);
                            }
                        }
                        catch (Exception ex)
                        { 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMEmployeesBase.Get: {0}", ex.Message));
						}

                        pQuery.Dispose();
                        pQuery=null;
        			}
        			else
					{
			        	vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
	        	}
	        	finally
	        	{
        			ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
        		}
        	}
        	else
			{
	        	vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Get: {0}",  ErrorCode.GetError(vResult)));
			return vResult;
		}
        /// <summary>
        /// Select records to the view Employees. Select owner database select query.
        /// </summary>
        /// <param name="pQuery">Query to execute</param>
        /// <param name="pSelectFields">Fields for the selection</param>
        /// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
        /// <returns>The result of execute get process</returns>
        public int Get(ORMSelectQuery pQuery, ORMField[] pSelectFields, ORMEmployeesRecordSet pDBRecordSet)
        {
        	return Get(pQuery,pSelectFields, pDBRecordSet, null);
        }
        #endregion

		#region Update
		/// <summary>
		/// Update the Employees records. Update by primary key using Record. Use a Database resource
		/// </summary>
		/// <param name="pORMRecord">Record to update</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMEmployeesRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;

			if (pORMFilter.PrimaryKeyNotNull()) // Primary key constraint.
			{
				vORMFilter = ORMRecordToORMFilter(pORMFilter, true);
				vResult = Update(pORMFilter, vORMFilter, pDBConnection);
				vORMFilter.Dispose();
				vORMFilter=null;
			}
			else
			{
				vResult=ErrorCode.ErrorDatabase.DBF_PRIMARY_KEY_IS_NULL;
			}

			return vResult;
		}
		/// <summary>
		/// Update the Employees records. Update by primary key using Record. Create a Database resource
		/// </summary>
		/// <param name="pORMRecord">Record to update</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMEmployeesRecord pORMRecord)
		{
			DBConnection vDBConnection = null;

			return Update(pORMRecord, vDBConnection);
		}
		/// <summary>
		/// Update the Employees records. Update by primary key using RecordSet. Use a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to update the records</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMEmployeesRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			bool TransactionOK;
			ORMEmployeesRecord vDBRecord;
			bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Update: {0}",  pDBRecordSet.Count.ToString()));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						TransactionOK = pDBConnection.StartTransaction();
						try
						{
							for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
							{
								vDBRecord=pDBRecordSet[vIndex];
								vResult = Update(vDBRecord, pDBConnection);

								if (vResult != ErrorCode.NO_ERROR) // Exit for.
								{
									break;
								}
							}
						}
						finally
						{
							if (TransactionOK)
							{
								pDBConnection.EndTransaction(vResult == ErrorCode.NO_ERROR);
							}
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Update: {0}",  ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Update the Employees records. Update by primary key using RecordSet. Create a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to update the records</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMEmployeesRecordSet pDBRecordSet)
		{
			return Update(pDBRecordSet, null);
		}
		/// <summary>
		/// Update the Employees records by filter. Update by a filter using Record. Use a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMEmployeesRecord pORMRecord, ORMEmployeesRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			//check if filter has elements
			if (vORMFilter.Params.Count == 0)
			{
				//TraceError(vTableName + ": Update by Record-Filter","ORMFilter is empty");
				vResult = ErrorCode.ErrorDatabase.DBF_FILTER_EMPTY;
			}
			vResult = Update(pORMRecord, vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Update the Employees records by filter. Update by a filter using Record. Create a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMEmployeesRecord pORMRecord, ORMEmployeesRecord pORMFilter)
		{
			return Update(pORMRecord, pORMFilter, null);
		}
		/// <summary>
		/// Update the Employees records by filter. Update by a filter. Use a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pORMFilter">Filter for the update sentence</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMEmployeesRecord pORMRecord, ORMFilter pORMFilter, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMUpdateQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Update: {0}", TraceRecord(pORMRecord)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery = new ORMUpdateQuery();
						try
						{
							vQuery.Update(this).Set_(pORMRecord).Where(pORMFilter, false);
							vQuery.Execute(pDBConnection);
						}
						catch (Exception ex)
						{ 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMEmployeesBase.Update: {0}", ex.Message));
						}
						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Update: {0}",  ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Update the Employees records by filter. Update by a filter. Create a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMEmployeesRecord pORMRecord, ORMFilter pORMFilter)
		{
			return Update(pORMRecord, pORMFilter, null);
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes the Employees records. Delete by primary key using Recordset. Use a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to delete the records</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMEmployeesRecordSet pDBRecordSet,DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			bool TransactionOK, vDBNotAssigned;
			ORMEmployeesRecord vDBRecord;
			ORMFilter vORMFilter;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Delete: {0}", pDBRecordSet.Count.ToString()));
			
			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						TransactionOK = pDBConnection.StartTransaction();
						try
						{
							for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
							{
								vDBRecord=pDBRecordSet[vIndex];

								if (vDBRecord.PrimaryKeyNotNull()) // Primary key constraint.
								{
									vORMFilter = ORMRecordToORMFilter(vDBRecord, true);
									vResult = Delete(vORMFilter, pDBConnection);
									vORMFilter.Dispose();
									vORMFilter=null;
								}
								else
								{
									vResult=ErrorCode.ErrorDatabase.DBF_PRIMARY_KEY_IS_NULL;
								}

								if (vResult != ErrorCode.NO_ERROR) // Exit for.
								{
									break;
								}
							}
						}
						finally
						{
							if (TransactionOK)
							{
								pDBConnection.EndTransaction(vResult == ErrorCode.NO_ERROR);
							}
						}
					}
					else
					{
						vResult= ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult= ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Delete: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Deletes the Employees records. Delete by primary key using Recordset. Create a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to delete the records</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMEmployeesRecordSet pDBRecordSet)
		{
			return Delete(pDBRecordSet, null);
		}
		/// <summary>
		/// Deletes the Employees records. Delete by a filter using Record. Using Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMEmployeesRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);

			//check if filter has elements
			if (vORMFilter.Params.Count == 0)
			{
				//TraceError(vTableName + ": Delete by Record-Filter","ORMFilter is empty");
				vResult= ErrorCode.ErrorDatabase.DBF_FILTER_EMPTY;
			}
			vResult = Delete(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Deletes the Employees records. Delete by a filter using Record. Create Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMEmployeesRecord pORMFilter)
		{
			return Delete(pORMFilter, null);
		}
		/// <summary>
		/// Deletes the Employees records. Delete by a filter. Using a Datbase Resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMFilter pORMFilter,DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMDeleteQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Delete: {0}", TraceFilter(pORMFilter)));
			

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery=new ORMDeleteQuery();
						try
						{
							vQuery.DeleteFrom(this).Where(pORMFilter,false);
							vQuery.Execute(pDBConnection);

						}
						catch (Exception ex)
						{ 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMEmployeesBase.Delete: {0}", ex.Message));
						}
						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult= ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult= ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Delete: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Deletes the Employees records. Delete by a filter. Create a Datbase Resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMFilter pORMFilter)
		{
			return Delete(pORMFilter, null);
		}
		/// <summary>
		/// Deletes Employees records. Delete All. Using a Database Resource
		/// </summary>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(DBConnection pDBConnection)
        {
            int vResult = ErrorCode.NO_ERROR;
            bool vDBNotAssigned;
            ORMDeleteQuery vQuery;

            vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.DeleteAll"));


            vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

            if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
            {
                try
                {
                    if (pDBConnection.Connected) // Check if database is connected.
                    {
                        vQuery = new ORMDeleteQuery();
                        try
                        {
                            vQuery.DeleteFrom(this);
                            vQuery.Execute(pDBConnection);

                        }
                        catch (Exception ex)
                        {
                            vResult = ErrorCode.ERROR_EXCEPTION;
                            vTrace.TraceException(string.Format("Exception ORMEmployeesBase.DeleteAll: {0}", ex.Message));
                        }
                        vQuery.Dispose();
                        vQuery = null;
                    }
                    else
                    {
                        vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
                    }
                }
                finally
                {
                    ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
                }
            }
            else
            {
                vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
            }

            vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.DeleteAll: {0}", ErrorCode.GetError(vResult)));
            return vResult;
        }
        /// <summary>
        /// Deletes Employees records. Delete All.
        /// </summary>
        /// <param name="pDBConnection">Database resource to use</param>
        /// <returns>The result of execute delete process</returns>
        public int Delete()
        {
            DBConnection vDBConnection = null;

            return Delete(vDBConnection);
        }
		#endregion

		#region Count
		/// <summary>
		/// Return the number of records by a filter. Using Recond and Database Resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMEmployeesRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  0;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			vResult = Count(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Return the number of records by a filter. Using Recond and Create Database Resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMEmployeesRecord pORMFilter)
		{
			return Count(pORMFilter, null);
		}
		/// <summary>
		/// Return the number of records by a filter. Using Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMFilter pORMFilter,DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMSelectQuery vQuery;
			ORMIntegerField vDBTotal;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Count: {0}", TraceFilter(pORMFilter)));
			
			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vDBTotal= new ORMIntegerField("TOTAL");

						vQuery=CreateSelect();
						try
						{
							vQuery.Select("COUNT(*) AS TOTAL").From(this).Where(pORMFilter, false);
							vQuery.Open(pDBConnection);
							if (vQuery.Next())
							{
								vQuery.GetFieldValue(vDBTotal);
							}
							vResult = vDBTotal.Value;
						}
						catch (Exception ex)
						{
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMEmployeesgBase.Count: {0}", ex.Message)); 
						}
						vQuery.Dispose();
						vQuery=null;
						vDBTotal=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Count: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Return the number of records by a filter. Create Database resource
		/// </summary>
		/// <param name="pFilter">Record to create the filter</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMFilter pORMFilter)
		{
			return Count(pORMFilter, null);
		}
		/// <summary>
		/// Return the number of records in the table. Using database resource
		/// </summary>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  0;

			vORMFilter = CreateFilter();
			vResult = Count(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Return the number of records in the table. Create database resource
		/// </summary>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count()
		{
			DBConnection vDBConnection = null;

			return Count(vDBConnection);
		}
		#endregion

		#region Exist
		/// <summary>
		/// Returns if there are filtered records. Using Record and Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMEmployeesRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			bool vResult = false;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			vResult = Exists(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Returns if there are filtered records. Unsing Record and create a Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMEmployeesRecord pORMFilter)
		{
			return Exists(pORMFilter, null);
		}
		/// <summary>
		/// Returns if there are filtered records. Using Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMFilter pORMFilter,DBConnection pDBConnection)
		{
			bool vResult;

			vTrace.TraceInfo(string.Format("Enter ORMEmployeesBase.Exists: {0}", TraceFilter(pORMFilter)));
			vResult = (Count(pORMFilter, pDBConnection) > 0);
			vTrace.TraceInfo(string.Format("Leave ORMEmployeesBase.Exists: {0}",  vResult.ToString()));
			
			return vResult;
		}
		/// <summary>
		/// Returns if there are filtered records. Create Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMFilter pORMFilter)
		{
			return Exists(pORMFilter, null);
		}
		/// <summary>
		/// Returns if table is empty. Using Database resource
		/// </summary>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>True if the table is empty</returns>
		public bool isEmpty(DBConnection pDBConnection)
		{
			return (Count(pDBConnection)==0);
		}
		/// <summary>
		/// Returns if table is empty. Create Database resource
		/// </summary>
		/// <returns>True if the table is empty</returns>
		public bool isEmpty()
		{
			return isEmpty(null);
		}
		#endregion
	}
	#endregion
}
