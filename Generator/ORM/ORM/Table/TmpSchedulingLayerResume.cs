using System;
using Robotics.DataLayer.ORM.Database;
using Robotics.DataLayer.ORM.Field;
using Robotics.DataLayer.ORM.Query;
using Robotics.DataLayer.ORM.Record;

namespace Robotics.DataLayer.ORM.Table
{
	#region ORMTmpSchedulingLayerResumeRecord
	/// <summary>
	/// Class to store the data information for a TmpSchedulingLayerResume record
	/// </summary>
	public class ORMTmpSchedulingLayerResumeRecord:ORMRecordBase
	{	
		#region Variables
		public ORMDecimalField IDGROUP;
		public ORMDateTimeField DATE;
		public ORMDecimalField LAYER0_VALUE1;
		public ORMDecimalField LAYER0_VALUE2;
		public ORMDecimalField LAYER0_VALUE3;
		public ORMDecimalField LAYER0_VALUE4;
		public ORMDecimalField LAYER0_VALUE5;
		public ORMDecimalField LAYER0_VALUE6;
		public ORMDecimalField LAYER0_VALUE7;
		public ORMDecimalField LAYER0_VALUE8;
		public ORMDecimalField LAYER0_VALUE9;
		public ORMDecimalField LAYER0_VALUE10;
		public ORMDecimalField LAYER0_VALUE11;
		public ORMDecimalField LAYER0_VALUE12;
		public ORMDecimalField LAYER0_VALUE13;
		public ORMDecimalField LAYER0_VALUE14;
		public ORMDecimalField LAYER0_VALUE15;
		public ORMDecimalField LAYER0_VALUE16;
		public ORMDecimalField LAYER0_VALUE17;
		public ORMDecimalField LAYER0_VALUE18;
		public ORMDecimalField LAYER0_VALUE19;
		public ORMDecimalField LAYER0_VALUE20;
		public ORMDecimalField LAYER0_VALUE21;
		public ORMDecimalField LAYER0_VALUE22;
		public ORMDecimalField LAYER0_VALUE23;
		public ORMDecimalField LAYER0_VALUE24;
		public ORMDecimalField LAYER0_VALUE25;
		public ORMDecimalField LAYER0_VALUE26;
		public ORMDecimalField LAYER0_VALUE27;
		public ORMDecimalField LAYER0_VALUE28;
		public ORMDecimalField LAYER0_VALUE29;
		public ORMDecimalField LAYER0_VALUE30;
		public ORMDecimalField LAYER0_VALUE31;
		public ORMDecimalField LAYER1_VALUE1;
		public ORMDecimalField LAYER1_VALUE2;
		public ORMDecimalField LAYER1_VALUE3;
		public ORMDecimalField LAYER1_VALUE4;
		public ORMDecimalField LAYER1_VALUE5;
		public ORMDecimalField LAYER1_VALUE6;
		public ORMDecimalField LAYER1_VALUE7;
		public ORMDecimalField LAYER1_VALUE8;
		public ORMDecimalField LAYER1_VALUE9;
		public ORMDecimalField LAYER1_VALUE10;
		public ORMDecimalField LAYER1_VALUE11;
		public ORMDecimalField LAYER1_VALUE12;
		public ORMDecimalField LAYER1_VALUE13;
		public ORMDecimalField LAYER1_VALUE14;
		public ORMDecimalField LAYER1_VALUE15;
		public ORMDecimalField LAYER1_VALUE16;
		public ORMDecimalField LAYER1_VALUE17;
		public ORMDecimalField LAYER1_VALUE18;
		public ORMDecimalField LAYER1_VALUE19;
		public ORMDecimalField LAYER1_VALUE20;
		public ORMDecimalField LAYER1_VALUE21;
		public ORMDecimalField LAYER1_VALUE22;
		public ORMDecimalField LAYER1_VALUE23;
		public ORMDecimalField LAYER1_VALUE24;
		public ORMDecimalField LAYER1_VALUE25;
		public ORMDecimalField LAYER1_VALUE26;
		public ORMDecimalField LAYER1_VALUE27;
		public ORMDecimalField LAYER1_VALUE28;
		public ORMDecimalField LAYER1_VALUE29;
		public ORMDecimalField LAYER1_VALUE30;
		public ORMDecimalField LAYER1_VALUE31;
		public ORMDecimalField LAYER2_VALUE1;
		public ORMDecimalField LAYER2_VALUE2;
		public ORMDecimalField LAYER2_VALUE3;
		public ORMDecimalField LAYER2_VALUE4;
		public ORMDecimalField LAYER2_VALUE5;
		public ORMDecimalField LAYER2_VALUE6;
		public ORMDecimalField LAYER2_VALUE7;
		public ORMDecimalField LAYER2_VALUE8;
		public ORMDecimalField LAYER2_VALUE9;
		public ORMDecimalField LAYER2_VALUE10;
		public ORMDecimalField LAYER2_VALUE11;
		public ORMDecimalField LAYER2_VALUE12;
		public ORMDecimalField LAYER2_VALUE13;
		public ORMDecimalField LAYER2_VALUE14;
		public ORMDecimalField LAYER2_VALUE15;
		public ORMDecimalField LAYER2_VALUE16;
		public ORMDecimalField LAYER2_VALUE17;
		public ORMDecimalField LAYER2_VALUE18;
		public ORMDecimalField LAYER2_VALUE19;
		public ORMDecimalField LAYER2_VALUE20;
		public ORMDecimalField LAYER2_VALUE21;
		public ORMDecimalField LAYER2_VALUE22;
		public ORMDecimalField LAYER2_VALUE23;
		public ORMDecimalField LAYER2_VALUE24;
		public ORMDecimalField LAYER2_VALUE25;
		public ORMDecimalField LAYER2_VALUE26;
		public ORMDecimalField LAYER2_VALUE27;
		public ORMDecimalField LAYER2_VALUE28;
		public ORMDecimalField LAYER2_VALUE29;
		public ORMDecimalField LAYER2_VALUE30;
		public ORMDecimalField LAYER2_VALUE31;
		public ORMDecimalField IDREPORTTASK;
		#endregion

		#region Constructor
		/// <summary>
		/// Class constructor
		/// </summary>
		public ORMTmpSchedulingLayerResumeRecord():base("TmpSchedulingLayerResume")
		{
			IDGROUP = CreateDecimalField("IDGROUP",18,0);
			DATE = CreateDateTimeField("DATE");
			LAYER0_VALUE1 = CreateDecimalField("LAYER0_VALUE1",10,2);
			LAYER0_VALUE2 = CreateDecimalField("LAYER0_VALUE2",16,6);
			LAYER0_VALUE3 = CreateDecimalField("LAYER0_VALUE3",16,6);
			LAYER0_VALUE4 = CreateDecimalField("LAYER0_VALUE4",16,6);
			LAYER0_VALUE5 = CreateDecimalField("LAYER0_VALUE5",16,6);
			LAYER0_VALUE6 = CreateDecimalField("LAYER0_VALUE6",16,6);
			LAYER0_VALUE7 = CreateDecimalField("LAYER0_VALUE7",16,6);
			LAYER0_VALUE8 = CreateDecimalField("LAYER0_VALUE8",16,6);
			LAYER0_VALUE9 = CreateDecimalField("LAYER0_VALUE9",16,6);
			LAYER0_VALUE10 = CreateDecimalField("LAYER0_VALUE10",16,6);
			LAYER0_VALUE11 = CreateDecimalField("LAYER0_VALUE11",16,6);
			LAYER0_VALUE12 = CreateDecimalField("LAYER0_VALUE12",16,6);
			LAYER0_VALUE13 = CreateDecimalField("LAYER0_VALUE13",16,6);
			LAYER0_VALUE14 = CreateDecimalField("LAYER0_VALUE14",16,6);
			LAYER0_VALUE15 = CreateDecimalField("LAYER0_VALUE15",16,6);
			LAYER0_VALUE16 = CreateDecimalField("LAYER0_VALUE16",16,6);
			LAYER0_VALUE17 = CreateDecimalField("LAYER0_VALUE17",16,6);
			LAYER0_VALUE18 = CreateDecimalField("LAYER0_VALUE18",16,6);
			LAYER0_VALUE19 = CreateDecimalField("LAYER0_VALUE19",16,6);
			LAYER0_VALUE20 = CreateDecimalField("LAYER0_VALUE20",16,6);
			LAYER0_VALUE21 = CreateDecimalField("LAYER0_VALUE21",16,6);
			LAYER0_VALUE22 = CreateDecimalField("LAYER0_VALUE22",16,6);
			LAYER0_VALUE23 = CreateDecimalField("LAYER0_VALUE23",16,6);
			LAYER0_VALUE24 = CreateDecimalField("LAYER0_VALUE24",16,6);
			LAYER0_VALUE25 = CreateDecimalField("LAYER0_VALUE25",16,6);
			LAYER0_VALUE26 = CreateDecimalField("LAYER0_VALUE26",16,6);
			LAYER0_VALUE27 = CreateDecimalField("LAYER0_VALUE27",16,6);
			LAYER0_VALUE28 = CreateDecimalField("LAYER0_VALUE28",16,6);
			LAYER0_VALUE29 = CreateDecimalField("LAYER0_VALUE29",16,6);
			LAYER0_VALUE30 = CreateDecimalField("LAYER0_VALUE30",16,6);
			LAYER0_VALUE31 = CreateDecimalField("LAYER0_VALUE31",16,6);
			LAYER1_VALUE1 = CreateDecimalField("LAYER1_VALUE1",10,2);
			LAYER1_VALUE2 = CreateDecimalField("LAYER1_VALUE2",16,6);
			LAYER1_VALUE3 = CreateDecimalField("LAYER1_VALUE3",16,6);
			LAYER1_VALUE4 = CreateDecimalField("LAYER1_VALUE4",16,6);
			LAYER1_VALUE5 = CreateDecimalField("LAYER1_VALUE5",16,6);
			LAYER1_VALUE6 = CreateDecimalField("LAYER1_VALUE6",16,6);
			LAYER1_VALUE7 = CreateDecimalField("LAYER1_VALUE7",16,6);
			LAYER1_VALUE8 = CreateDecimalField("LAYER1_VALUE8",16,6);
			LAYER1_VALUE9 = CreateDecimalField("LAYER1_VALUE9",16,6);
			LAYER1_VALUE10 = CreateDecimalField("LAYER1_VALUE10",16,6);
			LAYER1_VALUE11 = CreateDecimalField("LAYER1_VALUE11",16,6);
			LAYER1_VALUE12 = CreateDecimalField("LAYER1_VALUE12",16,6);
			LAYER1_VALUE13 = CreateDecimalField("LAYER1_VALUE13",16,6);
			LAYER1_VALUE14 = CreateDecimalField("LAYER1_VALUE14",16,6);
			LAYER1_VALUE15 = CreateDecimalField("LAYER1_VALUE15",16,6);
			LAYER1_VALUE16 = CreateDecimalField("LAYER1_VALUE16",16,6);
			LAYER1_VALUE17 = CreateDecimalField("LAYER1_VALUE17",16,6);
			LAYER1_VALUE18 = CreateDecimalField("LAYER1_VALUE18",16,6);
			LAYER1_VALUE19 = CreateDecimalField("LAYER1_VALUE19",16,6);
			LAYER1_VALUE20 = CreateDecimalField("LAYER1_VALUE20",16,6);
			LAYER1_VALUE21 = CreateDecimalField("LAYER1_VALUE21",16,6);
			LAYER1_VALUE22 = CreateDecimalField("LAYER1_VALUE22",16,6);
			LAYER1_VALUE23 = CreateDecimalField("LAYER1_VALUE23",16,6);
			LAYER1_VALUE24 = CreateDecimalField("LAYER1_VALUE24",16,6);
			LAYER1_VALUE25 = CreateDecimalField("LAYER1_VALUE25",16,6);
			LAYER1_VALUE26 = CreateDecimalField("LAYER1_VALUE26",16,6);
			LAYER1_VALUE27 = CreateDecimalField("LAYER1_VALUE27",16,6);
			LAYER1_VALUE28 = CreateDecimalField("LAYER1_VALUE28",16,6);
			LAYER1_VALUE29 = CreateDecimalField("LAYER1_VALUE29",16,6);
			LAYER1_VALUE30 = CreateDecimalField("LAYER1_VALUE30",16,6);
			LAYER1_VALUE31 = CreateDecimalField("LAYER1_VALUE31",16,6);
			LAYER2_VALUE1 = CreateDecimalField("LAYER2_VALUE1",10,2);
			LAYER2_VALUE2 = CreateDecimalField("LAYER2_VALUE2",16,6);
			LAYER2_VALUE3 = CreateDecimalField("LAYER2_VALUE3",16,6);
			LAYER2_VALUE4 = CreateDecimalField("LAYER2_VALUE4",16,6);
			LAYER2_VALUE5 = CreateDecimalField("LAYER2_VALUE5",16,6);
			LAYER2_VALUE6 = CreateDecimalField("LAYER2_VALUE6",16,6);
			LAYER2_VALUE7 = CreateDecimalField("LAYER2_VALUE7",16,6);
			LAYER2_VALUE8 = CreateDecimalField("LAYER2_VALUE8",16,6);
			LAYER2_VALUE9 = CreateDecimalField("LAYER2_VALUE9",16,6);
			LAYER2_VALUE10 = CreateDecimalField("LAYER2_VALUE10",16,6);
			LAYER2_VALUE11 = CreateDecimalField("LAYER2_VALUE11",16,6);
			LAYER2_VALUE12 = CreateDecimalField("LAYER2_VALUE12",16,6);
			LAYER2_VALUE13 = CreateDecimalField("LAYER2_VALUE13",16,6);
			LAYER2_VALUE14 = CreateDecimalField("LAYER2_VALUE14",16,6);
			LAYER2_VALUE15 = CreateDecimalField("LAYER2_VALUE15",16,6);
			LAYER2_VALUE16 = CreateDecimalField("LAYER2_VALUE16",16,6);
			LAYER2_VALUE17 = CreateDecimalField("LAYER2_VALUE17",16,6);
			LAYER2_VALUE18 = CreateDecimalField("LAYER2_VALUE18",16,6);
			LAYER2_VALUE19 = CreateDecimalField("LAYER2_VALUE19",16,6);
			LAYER2_VALUE20 = CreateDecimalField("LAYER2_VALUE20",16,6);
			LAYER2_VALUE21 = CreateDecimalField("LAYER2_VALUE21",16,6);
			LAYER2_VALUE22 = CreateDecimalField("LAYER2_VALUE22",16,6);
			LAYER2_VALUE23 = CreateDecimalField("LAYER2_VALUE23",16,6);
			LAYER2_VALUE24 = CreateDecimalField("LAYER2_VALUE24",16,6);
			LAYER2_VALUE25 = CreateDecimalField("LAYER2_VALUE25",16,6);
			LAYER2_VALUE26 = CreateDecimalField("LAYER2_VALUE26",16,6);
			LAYER2_VALUE27 = CreateDecimalField("LAYER2_VALUE27",16,6);
			LAYER2_VALUE28 = CreateDecimalField("LAYER2_VALUE28",16,6);
			LAYER2_VALUE29 = CreateDecimalField("LAYER2_VALUE29",16,6);
			LAYER2_VALUE30 = CreateDecimalField("LAYER2_VALUE30",16,6);
			LAYER2_VALUE31 = CreateDecimalField("LAYER2_VALUE31",16,6);
			IDREPORTTASK = CreateDecimalField("IDREPORTTASK",16,0);

			vPKExplicit =true;

			PrimaryKeyList.Add(IDGROUP);
			PrimaryKeyList.Add(DATE);
			
		}
		#endregion

		#region Destructor
		/// <summary>
		/// Override Method for free all resources
		/// Controls that execute this process only one time and stop, if is necessary, the call of destructor by Garbage Collector
		/// </summary>
		/// <param name="pDisposable">boolean that indicates if free managed resources</param>
		protected override void Dispose(bool pDisposable)
		{
			if (!vDisposed)
			{
				//call father Dispose
				base.Dispose(true);
				GC.SuppressFinalize(this);
			}
		}
		#endregion
	}
	#endregion

	#region ORMTmpSchedulingLayerResumeRecordSet
	/// <summary>
	/// Class to create a list of TmpSchedulingLayerResume records
	/// </summary>
	public class ORMTmpSchedulingLayerResumeRecordSet:ORMRecordsetBase<ORMTmpSchedulingLayerResumeRecord>
	{
		#region CreateRecord
		/// <summary>
		/// Creates a ORMTmpSchedulingLayerResume and adds the object to the list
		/// </summary>
		public ORMTmpSchedulingLayerResumeRecord CreateRecord()
		{
			this.Add(new ORMTmpSchedulingLayerResumeRecord());
			return this[this.Count - 1];
		}
		#endregion
	}
	#endregion

	#region ORMTmpSchedulingLayerResumeBase
	/// <summary>
	/// Class base for basic operations with TmpSchedulingLayerResume records
	/// </summary>
	public class ORMTmpSchedulingLayerResumeBase : DBTableBase
	{
		#region Constructor
		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="pDBManager">Database manager who creates the object</param>
		public ORMTmpSchedulingLayerResumeBase() : base("TmpSchedulingLayerResume")
		{
			PKType=DBTableBase.PK_TYPE_EXPLICIT;
		}
		#endregion

		#region Create
		/// <summary>
		/// Creates a ORMTmpSchedulingLayerResume record
		/// </summary>
		/// <returns>ORMTmpSchedulingLayerResume record</returns>
        public ORMTmpSchedulingLayerResumeRecord CreateRecord()
        {
            ORMTmpSchedulingLayerResumeRecord vRecord;
            vRecord = new ORMTmpSchedulingLayerResumeRecord();

			return vRecord;
		}
		/// <summary>
		/// Create a list of ORMTmpSchedulingLayerResume record
		/// </summary>
		/// <returns>List of ORMTmpSchedulingLayerResume record</returns>
		public ORMTmpSchedulingLayerResumeRecordSet CreateRecordSet()
		{
			ORMTmpSchedulingLayerResumeRecordSet vRecordSet;
			vRecordSet = new ORMTmpSchedulingLayerResumeRecordSet();

			return vRecordSet;
		}
		#endregion

		//public functions

		#region Insert
		/// <summary>
		/// Insert a tblTmpSchedulingLayerResume record to the table. Use a database resource
		/// </summary>
		/// <param name="pORMTmpSchedulingLayerResumeRecord">Record to insert</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMTmpSchedulingLayerResumeRecord pORMRecord, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMInsertQuery vQuery;

			vTrace.TraceVerbose(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Insert: {0}", TraceRecord(pORMRecord)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery = new ORMInsertQuery();

						try
						{
							vQuery.InsertInto(this).Fields(pORMRecord);
							vQuery.Execute(pDBConnection);
						}
						catch (Exception ex)
						{
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpSchedulingLayerResumeBase.Insert: {0}", ex.Message));
						}
						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Insert: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Insert a tblTmpSchedulingLayerResume record to the table. Create a database resource
		/// </summary>
		/// <param name="pORMRecord">Record to insert</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMTmpSchedulingLayerResumeRecord pORMRecord)
		{
			return Insert(pORMRecord, null);
		}
		/// <summary>
		/// Insert the tblTmpSchedulingLayerResume records to the table. Use a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet,DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			bool TransactionOK;
			ORMTmpSchedulingLayerResumeRecord vDBRecord;
			bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Insert: {0}",  pDBRecordSet.Count.ToString()));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						TransactionOK = pDBConnection.StartTransaction();
						try
						{
							for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
							{
								vDBRecord=pDBRecordSet[vIndex];
								vResult = Insert(vDBRecord, pDBConnection);

								if (vResult != ErrorCode.NO_ERROR) // Exit for.
								{
									break;
								}
							}
						}
						finally
						{
							if (TransactionOK)
							{
								pDBConnection.EndTransaction(vResult == ErrorCode.NO_ERROR);
							}
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Insert: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Insert the tblTmpSchedulingLayerResume records to the table. Create a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record to insert</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Insert(pDBRecordSet, null);
		}
		#endregion
		
		#region Get
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select by primary key using Record. Use a database resource
		/// </summary>
		/// <param name="pORMRecord">Record to get</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpSchedulingLayerResumeRecord pORMRecord, ORMField[] pSelectFields, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMSelectQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Get: {0}", TraceFields(pORMRecord.PrimaryKeyList)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						if (pORMRecord.PrimaryKeyNotNull()) // Primary key constraint.
						{
							vORMFilter = ORMRecordToORMFilter(pORMRecord, true);
							vQuery = CreateSelect();

							try
							{
								//Select all records or a specific list of fields
								if (ReferenceEquals(pSelectFields,null) || pSelectFields.Length==0)
								{
									vQuery.Select(this);
								}
								else
								{
									vQuery.Select(pSelectFields);
								}
								vQuery.From(this).Where(vORMFilter, false);

								vQuery.Open(pDBConnection);
								if (vQuery.Next())
								{
									vQuery.GetRecordValues(pORMRecord);
								}
								else
								{
									vResult = ErrorCode.ErrorDatabase.DBF_RECORD_NOT_EXISTS;
								}

							}
							catch (Exception ex)
							{ 
								vResult = ErrorCode.ERROR_EXCEPTION;
								vTrace.TraceException(string.Format("Exception ORMTmpSchedulingLayerResumeBase.Get: {0}", ex.Message));
							}

							vORMFilter.Dispose();
							vORMFilter=null;
							vQuery.Dispose();
							vQuery=null;
						}
						else
						{
							vResult = ErrorCode.ErrorDatabase.DBF_PRIMARY_KEY_IS_NULL;
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Get: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select by primary key using Record. Create a database resource
		/// </summary>
		/// <param name="pORMRecord">Record to get</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpSchedulingLayerResumeRecord pORMRecord, ORMField[] pSelectFields)
		{
			DBConnection vDBConnection = null;

			return Get(pORMRecord, pSelectFields, vDBConnection);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select by primary key using RecordSet. Use a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to get the records</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet, ORMField[] pSelectFields, DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			ORMTmpSchedulingLayerResumeRecord vDBRecord;
			bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Get: {0}",  pDBRecordSet.Count.ToString()));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
						{
							vDBRecord=pDBRecordSet[vIndex];
							vResult = Get(vDBRecord, pSelectFields, pDBConnection);

							if (vResult != ErrorCode.NO_ERROR) // Exit for.
							{
								break;
							}
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Get: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select by primary key using RecordSet. Create a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to get the records</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet, ORMField[] pSelectFields)
		{
			return Get(pDBRecordSet, pSelectFields, null);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select by a filter using Record with order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpSchedulingLayerResumeRecord pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet,DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;
			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			vResult = Get(vORMFilter, pSelectFields, pOrderFields, GetOrderByArray(pDescendent,pOrderFields), pDBRecordSet, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select by a filter using Record with order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpSchedulingLayerResumeRecord pORMFilter, ORMField[] pSelectFields,ORMField[] pOrderFields,
			bool pDescendent, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume without order. Select by a filter using Record without order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpSchedulingLayerResumeRecord pORMFilter, ORMField[] pSelectFields,
			ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			return Get(pORMFilter, pSelectFields, null, false, pDBRecordSet, pDBConnection);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume without order. Select by a filter using Record without order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpSchedulingLayerResumeRecord pORMFilter, ORMField[] pSelectFields, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select by a filter with order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array with Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			ORMTmpSchedulingLayerResumeRecord vDBRecord;
			bool vDBNotAssigned;
			ORMSelectQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Get: {0}",  TraceFilter(pORMFilter)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery = CreateSelect();
						try
						{
							//Select all records or a specific list of fields
							if (ReferenceEquals(pSelectFields,null) || pSelectFields.Length==0)
							{
								vQuery.Select(this);
							}
							else
							{
								vQuery.Select(pSelectFields);
							}
							vQuery.From(this).Where(pORMFilter, false).OrderBy(pOrderFields,pDescendent);

							vQuery.Open(pDBConnection);
							while (vQuery.Next())
							{
								vDBRecord=pDBRecordSet.CreateRecord();
								vQuery.GetRecordValues(vDBRecord);
							}
						}
						catch (Exception ex)
						{ 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpSchedulingLayerResumeBase.Get: {0}", ex.Message));
						}

						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Get: {0}",   ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select by a filter with order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pOrderFields, GetOrderByArray(pDescendent, pOrderFields), pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select by a filter with order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume without order. Select by a filter without order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter,ORMField[] pSelectFields,
			ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			return Get(pORMFilter, pSelectFields, null, null, pDBRecordSet, pDBConnection);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume without order. Select by a filter without order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields,
			ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select all records with order by. Use a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			ORMFilter vORMFilter;

			vORMFilter = CreateFilter();
			vResult = Get(vORMFilter, pSelectFields, pOrderFields, GetOrderByArray(pDescendent, pOrderFields), pDBRecordSet, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select all records with order by. Use a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			ORMFilter vORMFilter;

			vORMFilter = CreateFilter();
			vResult = Get(vORMFilter, pSelectFields, pOrderFields, pDescendent, pDBRecordSet, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select all records with order by. Create a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Get(pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select all records with order by. Create a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Get(pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select all records without order by. Use a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMField[] pSelectFields, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			return Get(pSelectFields, null, false, pDBRecordSet, pDBConnection);
		}
		/// <summary>
		/// Select records to the table TmpSchedulingLayerResume. Select all records without order by. Create a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMField[] pSelectFields, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Get(pSelectFields, pDBRecordSet, null);
		}
        /// <summary>
        /// Select records to the view TmpSchedulingLayerResume. Select owner database select query. Use a database resource
        /// </summary>
        /// <param name="pQuery">Query to execute</param>
        /// <param name="pSelectFields">Fields for the selection</param>
        /// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
        /// <param name="pDBConnection">Database resource to use</param>
        /// <returns>The result of execute get process</returns>
        public int Get(ORMSelectQuery pQuery, ORMField[] pSelectFields, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet, DBConnection pDBConnection)
        {
        	int vResult =  ErrorCode.NO_ERROR;
        	ORMTmpSchedulingLayerResumeRecord vDBRecord;
        	bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Get: {0}",  TraceFilter(pQuery.ORMFilter)));

        	vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

        	if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
        	{
        		try
        		{
        			if (pDBConnection.Connected) // Check if database is connected.
        			{
                        try
                        {
                            pQuery.Open(pDBConnection);
                            while (pQuery.Next())
                            {
                                vDBRecord=pDBRecordSet.CreateRecord();
                                pQuery.GetRecordValues(vDBRecord);
                            }
                        }
                        catch (Exception ex)
                        { 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpSchedulingLayerResumeBase.Get: {0}", ex.Message));
						}

                        pQuery.Dispose();
                        pQuery=null;
        			}
        			else
					{
			        	vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
	        	}
	        	finally
	        	{
        			ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
        		}
        	}
        	else
			{
	        	vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Get: {0}",  ErrorCode.GetError(vResult)));
			return vResult;
		}
        /// <summary>
        /// Select records to the view TmpSchedulingLayerResume. Select owner database select query.
        /// </summary>
        /// <param name="pQuery">Query to execute</param>
        /// <param name="pSelectFields">Fields for the selection</param>
        /// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
        /// <returns>The result of execute get process</returns>
        public int Get(ORMSelectQuery pQuery, ORMField[] pSelectFields, ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
        {
        	return Get(pQuery,pSelectFields, pDBRecordSet, null);
        }
        #endregion

		#region Update
		/// <summary>
		/// Update the TmpSchedulingLayerResume records. Update by primary key using Record. Use a Database resource
		/// </summary>
		/// <param name="pORMRecord">Record to update</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpSchedulingLayerResumeRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;

			if (pORMFilter.PrimaryKeyNotNull()) // Primary key constraint.
			{
				vORMFilter = ORMRecordToORMFilter(pORMFilter, true);
				vResult = Update(pORMFilter, vORMFilter, pDBConnection);
				vORMFilter.Dispose();
				vORMFilter=null;
			}
			else
			{
				vResult=ErrorCode.ErrorDatabase.DBF_PRIMARY_KEY_IS_NULL;
			}

			return vResult;
		}
		/// <summary>
		/// Update the TmpSchedulingLayerResume records. Update by primary key using Record. Create a Database resource
		/// </summary>
		/// <param name="pORMRecord">Record to update</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpSchedulingLayerResumeRecord pORMRecord)
		{
			DBConnection vDBConnection = null;

			return Update(pORMRecord, vDBConnection);
		}
		/// <summary>
		/// Update the TmpSchedulingLayerResume records. Update by primary key using RecordSet. Use a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to update the records</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			bool TransactionOK;
			ORMTmpSchedulingLayerResumeRecord vDBRecord;
			bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Update: {0}",  pDBRecordSet.Count.ToString()));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						TransactionOK = pDBConnection.StartTransaction();
						try
						{
							for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
							{
								vDBRecord=pDBRecordSet[vIndex];
								vResult = Update(vDBRecord, pDBConnection);

								if (vResult != ErrorCode.NO_ERROR) // Exit for.
								{
									break;
								}
							}
						}
						finally
						{
							if (TransactionOK)
							{
								pDBConnection.EndTransaction(vResult == ErrorCode.NO_ERROR);
							}
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Update: {0}",  ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Update the TmpSchedulingLayerResume records. Update by primary key using RecordSet. Create a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to update the records</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Update(pDBRecordSet, null);
		}
		/// <summary>
		/// Update the TmpSchedulingLayerResume records by filter. Update by a filter using Record. Use a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpSchedulingLayerResumeRecord pORMRecord, ORMTmpSchedulingLayerResumeRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			//check if filter has elements
			if (vORMFilter.Params.Count == 0)
			{
				//TraceError(vTableName + ": Update by Record-Filter","ORMFilter is empty");
				vResult = ErrorCode.ErrorDatabase.DBF_FILTER_EMPTY;
			}
			vResult = Update(pORMRecord, vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Update the TmpSchedulingLayerResume records by filter. Update by a filter using Record. Create a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpSchedulingLayerResumeRecord pORMRecord, ORMTmpSchedulingLayerResumeRecord pORMFilter)
		{
			return Update(pORMRecord, pORMFilter, null);
		}
		/// <summary>
		/// Update the TmpSchedulingLayerResume records by filter. Update by a filter. Use a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pORMFilter">Filter for the update sentence</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpSchedulingLayerResumeRecord pORMRecord, ORMFilter pORMFilter, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMUpdateQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Update: {0}", TraceRecord(pORMRecord)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery = new ORMUpdateQuery();
						try
						{
							vQuery.Update(this).Set_(pORMRecord).Where(pORMFilter, false);
							vQuery.Execute(pDBConnection);
						}
						catch (Exception ex)
						{ 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpSchedulingLayerResumeBase.Update: {0}", ex.Message));
						}
						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Update: {0}",  ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Update the TmpSchedulingLayerResume records by filter. Update by a filter. Create a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpSchedulingLayerResumeRecord pORMRecord, ORMFilter pORMFilter)
		{
			return Update(pORMRecord, pORMFilter, null);
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes the TmpSchedulingLayerResume records. Delete by primary key using Recordset. Use a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to delete the records</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet,DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			bool TransactionOK, vDBNotAssigned;
			ORMTmpSchedulingLayerResumeRecord vDBRecord;
			ORMFilter vORMFilter;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Delete: {0}", pDBRecordSet.Count.ToString()));
			
			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						TransactionOK = pDBConnection.StartTransaction();
						try
						{
							for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
							{
								vDBRecord=pDBRecordSet[vIndex];

								if (vDBRecord.PrimaryKeyNotNull()) // Primary key constraint.
								{
									vORMFilter = ORMRecordToORMFilter(vDBRecord, true);
									vResult = Delete(vORMFilter, pDBConnection);
									vORMFilter.Dispose();
									vORMFilter=null;
								}
								else
								{
									vResult=ErrorCode.ErrorDatabase.DBF_PRIMARY_KEY_IS_NULL;
								}

								if (vResult != ErrorCode.NO_ERROR) // Exit for.
								{
									break;
								}
							}
						}
						finally
						{
							if (TransactionOK)
							{
								pDBConnection.EndTransaction(vResult == ErrorCode.NO_ERROR);
							}
						}
					}
					else
					{
						vResult= ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult= ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Delete: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Deletes the TmpSchedulingLayerResume records. Delete by primary key using Recordset. Create a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to delete the records</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMTmpSchedulingLayerResumeRecordSet pDBRecordSet)
		{
			return Delete(pDBRecordSet, null);
		}
		/// <summary>
		/// Deletes the TmpSchedulingLayerResume records. Delete by a filter using Record. Using Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMTmpSchedulingLayerResumeRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);

			//check if filter has elements
			if (vORMFilter.Params.Count == 0)
			{
				//TraceError(vTableName + ": Delete by Record-Filter","ORMFilter is empty");
				vResult= ErrorCode.ErrorDatabase.DBF_FILTER_EMPTY;
			}
			vResult = Delete(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Deletes the TmpSchedulingLayerResume records. Delete by a filter using Record. Create Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMTmpSchedulingLayerResumeRecord pORMFilter)
		{
			return Delete(pORMFilter, null);
		}
		/// <summary>
		/// Deletes the TmpSchedulingLayerResume records. Delete by a filter. Using a Datbase Resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMFilter pORMFilter,DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMDeleteQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Delete: {0}", TraceFilter(pORMFilter)));
			

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery=new ORMDeleteQuery();
						try
						{
							vQuery.DeleteFrom(this).Where(pORMFilter,false);
							vQuery.Execute(pDBConnection);

						}
						catch (Exception ex)
						{ 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpSchedulingLayerResumeBase.Delete: {0}", ex.Message));
						}
						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult= ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult= ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Delete: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Deletes the TmpSchedulingLayerResume records. Delete by a filter. Create a Datbase Resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMFilter pORMFilter)
		{
			return Delete(pORMFilter, null);
		}
		/// <summary>
		/// Deletes TmpSchedulingLayerResume records. Delete All. Using a Database Resource
		/// </summary>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(DBConnection pDBConnection)
        {
            int vResult = ErrorCode.NO_ERROR;
            bool vDBNotAssigned;
            ORMDeleteQuery vQuery;

            vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.DeleteAll"));


            vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

            if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
            {
                try
                {
                    if (pDBConnection.Connected) // Check if database is connected.
                    {
                        vQuery = new ORMDeleteQuery();
                        try
                        {
                            vQuery.DeleteFrom(this);
                            vQuery.Execute(pDBConnection);

                        }
                        catch (Exception ex)
                        {
                            vResult = ErrorCode.ERROR_EXCEPTION;
                            vTrace.TraceException(string.Format("Exception ORMTmpSchedulingLayerResumeBase.DeleteAll: {0}", ex.Message));
                        }
                        vQuery.Dispose();
                        vQuery = null;
                    }
                    else
                    {
                        vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
                    }
                }
                finally
                {
                    ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
                }
            }
            else
            {
                vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
            }

            vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.DeleteAll: {0}", ErrorCode.GetError(vResult)));
            return vResult;
        }
        /// <summary>
        /// Deletes TmpSchedulingLayerResume records. Delete All.
        /// </summary>
        /// <param name="pDBConnection">Database resource to use</param>
        /// <returns>The result of execute delete process</returns>
        public int Delete()
        {
            DBConnection vDBConnection = null;

            return Delete(vDBConnection);
        }
		#endregion

		#region Count
		/// <summary>
		/// Return the number of records by a filter. Using Recond and Database Resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMTmpSchedulingLayerResumeRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  0;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			vResult = Count(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Return the number of records by a filter. Using Recond and Create Database Resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMTmpSchedulingLayerResumeRecord pORMFilter)
		{
			return Count(pORMFilter, null);
		}
		/// <summary>
		/// Return the number of records by a filter. Using Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMFilter pORMFilter,DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMSelectQuery vQuery;
			ORMIntegerField vDBTotal;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Count: {0}", TraceFilter(pORMFilter)));
			
			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vDBTotal= new ORMIntegerField("TOTAL");

						vQuery=CreateSelect();
						try
						{
							vQuery.Select("COUNT(*) AS TOTAL").From(this).Where(pORMFilter, false);
							vQuery.Open(pDBConnection);
							if (vQuery.Next())
							{
								vQuery.GetFieldValue(vDBTotal);
							}
							vResult = vDBTotal.Value;
						}
						catch (Exception ex)
						{
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpSchedulingLayerResumegBase.Count: {0}", ex.Message)); 
						}
						vQuery.Dispose();
						vQuery=null;
						vDBTotal=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Count: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Return the number of records by a filter. Create Database resource
		/// </summary>
		/// <param name="pFilter">Record to create the filter</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMFilter pORMFilter)
		{
			return Count(pORMFilter, null);
		}
		/// <summary>
		/// Return the number of records in the table. Using database resource
		/// </summary>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  0;

			vORMFilter = CreateFilter();
			vResult = Count(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Return the number of records in the table. Create database resource
		/// </summary>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count()
		{
			DBConnection vDBConnection = null;

			return Count(vDBConnection);
		}
		#endregion

		#region Exist
		/// <summary>
		/// Returns if there are filtered records. Using Record and Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMTmpSchedulingLayerResumeRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			bool vResult = false;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			vResult = Exists(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Returns if there are filtered records. Unsing Record and create a Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMTmpSchedulingLayerResumeRecord pORMFilter)
		{
			return Exists(pORMFilter, null);
		}
		/// <summary>
		/// Returns if there are filtered records. Using Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMFilter pORMFilter,DBConnection pDBConnection)
		{
			bool vResult;

			vTrace.TraceInfo(string.Format("Enter ORMTmpSchedulingLayerResumeBase.Exists: {0}", TraceFilter(pORMFilter)));
			vResult = (Count(pORMFilter, pDBConnection) > 0);
			vTrace.TraceInfo(string.Format("Leave ORMTmpSchedulingLayerResumeBase.Exists: {0}",  vResult.ToString()));
			
			return vResult;
		}
		/// <summary>
		/// Returns if there are filtered records. Create Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMFilter pORMFilter)
		{
			return Exists(pORMFilter, null);
		}
		/// <summary>
		/// Returns if table is empty. Using Database resource
		/// </summary>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>True if the table is empty</returns>
		public bool isEmpty(DBConnection pDBConnection)
		{
			return (Count(pDBConnection)==0);
		}
		/// <summary>
		/// Returns if table is empty. Create Database resource
		/// </summary>
		/// <returns>True if the table is empty</returns>
		public bool isEmpty()
		{
			return isEmpty(null);
		}
		#endregion
	}
	#endregion
}
