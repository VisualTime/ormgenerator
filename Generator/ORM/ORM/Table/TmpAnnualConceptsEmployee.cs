using System;
using Robotics.DataLayer.ORM.Database;
using Robotics.DataLayer.ORM.Field;
using Robotics.DataLayer.ORM.Query;
using Robotics.DataLayer.ORM.Record;

namespace Robotics.DataLayer.ORM.Table
{
	#region ORMTmpAnnualConceptsEmployeeRecord
	/// <summary>
	/// Class to store the data information for a TmpAnnualConceptsEmployee record
	/// </summary>
	public class ORMTmpAnnualConceptsEmployeeRecord:ORMRecordBase
	{	
		#region Variables
		public ORMIntegerField IDPASSPORT;
		public ORMIntegerField IDEMPLOYEE;
		public ORMIntegerField EJERCICIO;
		public ORMIntegerField PERIODO;
		public ORMIntegerField IDGROUP;
		public ORMStringField GROUPNAME;
		public ORMStringField EMPLOYEENAME;
		public ORMSmallIntegerField IDCONCEPT1;
		public ORMStringField CONCEPTCOLOR1;
		public ORMStringField CONCEPTNAME1;
		public ORMDecimalField CONCEPTVALUE1;
		public ORMDecimalField CONCEPTVALUETOTAL1;
		public ORMSmallIntegerField IDCONCEPT2;
		public ORMStringField CONCEPTCOLOR2;
		public ORMStringField CONCEPTNAME2;
		public ORMDecimalField CONCEPTVALUE2;
		public ORMDecimalField CONCEPTVALUETOTAL2;
		public ORMSmallIntegerField IDCONCEPT3;
		public ORMStringField CONCEPTCOLOR3;
		public ORMStringField CONCEPTNAME3;
		public ORMDecimalField CONCEPTVALUE3;
		public ORMDecimalField CONCEPTVALUETOTAL3;
		public ORMSmallIntegerField IDCONCEPT4;
		public ORMStringField CONCEPTCOLOR4;
		public ORMStringField CONCEPTNAME4;
		public ORMDecimalField CONCEPTVALUE4;
		public ORMDecimalField CONCEPTVALUETOTAL4;
		public ORMSmallIntegerField IDCONCEPT5;
		public ORMStringField CONCEPTCOLOR5;
		public ORMStringField CONCEPTNAME5;
		public ORMDecimalField CONCEPTVALUE5;
		public ORMDecimalField CONCEPTVALUETOTAL5;
		public ORMSmallIntegerField IDCONCEPT6;
		public ORMStringField CONCEPTCOLOR6;
		public ORMStringField CONCEPTNAME6;
		public ORMDecimalField CONCEPTVALUE6;
		public ORMDecimalField CONCEPTVALUETOTAL6;
		public ORMSmallIntegerField IDCONCEPT7;
		public ORMStringField CONCEPTCOLOR7;
		public ORMStringField CONCEPTNAME7;
		public ORMDecimalField CONCEPTVALUE7;
		public ORMDecimalField CONCEPTVALUETOTAL7;
		public ORMSmallIntegerField IDCONCEPT8;
		public ORMStringField CONCEPTCOLOR8;
		public ORMStringField CONCEPTNAME8;
		public ORMDecimalField CONCEPTVALUE8;
		public ORMDecimalField CONCEPTVALUETOTAL8;
		public ORMSmallIntegerField IDCONCEPT9;
		public ORMStringField CONCEPTCOLOR9;
		public ORMStringField CONCEPTNAME9;
		public ORMDecimalField CONCEPTVALUE9;
		public ORMDecimalField CONCEPTVALUETOTAL9;
		public ORMSmallIntegerField IDCONCEPT10;
		public ORMStringField CONCEPTCOLOR10;
		public ORMStringField CONCEPTNAME10;
		public ORMDecimalField CONCEPTVALUE10;
		public ORMDecimalField CONCEPTVALUETOTAL10;
		public ORMSmallIntegerField IDCONCEPT11;
		public ORMStringField CONCEPTCOLOR11;
		public ORMStringField CONCEPTNAME11;
		public ORMDecimalField CONCEPTVALUE11;
		public ORMDecimalField CONCEPTVALUETOTAL11;
		public ORMSmallIntegerField IDCONCEPT12;
		public ORMStringField CONCEPTCOLOR12;
		public ORMStringField CONCEPTNAME12;
		public ORMDecimalField CONCEPTVALUE12;
		public ORMDecimalField CONCEPTVALUETOTAL12;
		public ORMSmallIntegerField IDCONCEPT13;
		public ORMStringField CONCEPTCOLOR13;
		public ORMStringField CONCEPTNAME13;
		public ORMDecimalField CONCEPTVALUE13;
		public ORMDecimalField CONCEPTVALUETOTAL13;
		public ORMSmallIntegerField IDCONCEPT14;
		public ORMStringField CONCEPTCOLOR14;
		public ORMStringField CONCEPTNAME14;
		public ORMDecimalField CONCEPTVALUE14;
		public ORMDecimalField CONCEPTVALUETOTAL14;
		public ORMSmallIntegerField IDCONCEPT15;
		public ORMStringField CONCEPTCOLOR15;
		public ORMStringField CONCEPTNAME15;
		public ORMDecimalField CONCEPTVALUE15;
		public ORMDecimalField CONCEPTVALUETOTAL15;
		public ORMSmallIntegerField IDCONCEPT16;
		public ORMStringField CONCEPTCOLOR16;
		public ORMStringField CONCEPTNAME16;
		public ORMDecimalField CONCEPTVALUE16;
		public ORMDecimalField CONCEPTVALUETOTAL16;
		public ORMSmallIntegerField IDCONCEPT17;
		public ORMStringField CONCEPTCOLOR17;
		public ORMStringField CONCEPTNAME17;
		public ORMDecimalField CONCEPTVALUE17;
		public ORMDecimalField CONCEPTVALUETOTAL17;
		public ORMSmallIntegerField IDCONCEPT18;
		public ORMStringField CONCEPTCOLOR18;
		public ORMStringField CONCEPTNAME18;
		public ORMDecimalField CONCEPTVALUE18;
		public ORMDecimalField CONCEPTVALUETOTAL18;
		public ORMSmallIntegerField IDCONCEPT19;
		public ORMStringField CONCEPTCOLOR19;
		public ORMStringField CONCEPTNAME19;
		public ORMDecimalField CONCEPTVALUE19;
		public ORMDecimalField CONCEPTVALUETOTAL19;
		public ORMSmallIntegerField IDCONCEPT20;
		public ORMStringField CONCEPTCOLOR20;
		public ORMStringField CONCEPTNAME20;
		public ORMDecimalField CONCEPTVALUE20;
		public ORMDecimalField CONCEPTVALUETOTAL20;
		public ORMSmallIntegerField IDCONCEPT21;
		public ORMStringField CONCEPTCOLOR21;
		public ORMStringField CONCEPTNAME21;
		public ORMDecimalField CONCEPTVALUE21;
		public ORMDecimalField CONCEPTVALUETOTAL21;
		public ORMSmallIntegerField IDCONCEPT22;
		public ORMStringField CONCEPTCOLOR22;
		public ORMStringField CONCEPTNAME22;
		public ORMDecimalField CONCEPTVALUE22;
		public ORMDecimalField CONCEPTVALUETOTAL22;
		public ORMSmallIntegerField IDCONCEPT23;
		public ORMStringField CONCEPTCOLOR23;
		public ORMStringField CONCEPTNAME23;
		public ORMDecimalField CONCEPTVALUE23;
		public ORMDecimalField CONCEPTVALUETOTAL23;
		public ORMSmallIntegerField IDCONCEPT24;
		public ORMStringField CONCEPTCOLOR24;
		public ORMStringField CONCEPTNAME24;
		public ORMDecimalField CONCEPTVALUE24;
		public ORMDecimalField CONCEPTVALUETOTAL24;
		public ORMSmallIntegerField IDCONCEPT25;
		public ORMStringField CONCEPTCOLOR25;
		public ORMStringField CONCEPTNAME25;
		public ORMDecimalField CONCEPTVALUE25;
		public ORMDecimalField CONCEPTVALUETOTAL25;
		public ORMSmallIntegerField IDCONCEPT26;
		public ORMStringField CONCEPTCOLOR26;
		public ORMStringField CONCEPTNAME26;
		public ORMDecimalField CONCEPTVALUE26;
		public ORMDecimalField CONCEPTVALUETOTAL26;
		public ORMSmallIntegerField IDCONCEPT27;
		public ORMStringField CONCEPTCOLOR27;
		public ORMStringField CONCEPTNAME27;
		public ORMDecimalField CONCEPTVALUE27;
		public ORMDecimalField CONCEPTVALUETOTAL27;
		public ORMSmallIntegerField IDCONCEPT28;
		public ORMStringField CONCEPTCOLOR28;
		public ORMStringField CONCEPTNAME28;
		public ORMDecimalField CONCEPTVALUE28;
		public ORMDecimalField CONCEPTVALUETOTAL28;
		public ORMSmallIntegerField IDCONCEPT29;
		public ORMStringField CONCEPTCOLOR29;
		public ORMStringField CONCEPTNAME29;
		public ORMDecimalField CONCEPTVALUE29;
		public ORMDecimalField CONCEPTVALUETOTAL29;
		public ORMSmallIntegerField IDCONCEPT30;
		public ORMStringField CONCEPTCOLOR30;
		public ORMStringField CONCEPTNAME30;
		public ORMDecimalField CONCEPTVALUE30;
		public ORMDecimalField CONCEPTVALUETOTAL30;
		public ORMSmallIntegerField IDCONCEPT31;
		public ORMStringField CONCEPTCOLOR31;
		public ORMStringField CONCEPTNAME31;
		public ORMDecimalField CONCEPTVALUE31;
		public ORMDecimalField CONCEPTVALUETOTAL31;
		public ORMStringField ABSENCESTEMPLATE;
		public ORMDecimalField IDREPORTTASK;
		#endregion

		#region Constructor
		/// <summary>
		/// Class constructor
		/// </summary>
		public ORMTmpAnnualConceptsEmployeeRecord():base("TmpAnnualConceptsEmployee")
		{
			IDPASSPORT = CreateIntegerField("IDPASSPORT");
			IDEMPLOYEE = CreateIntegerField("IDEMPLOYEE");
			EJERCICIO = CreateIntegerField("EJERCICIO");
			PERIODO = CreateIntegerField("PERIODO");
			IDGROUP = CreateIntegerField("IDGROUP");
			GROUPNAME = CreateStringField("GROUPNAME",200);
			EMPLOYEENAME = CreateStringField("EMPLOYEENAME",50);
			IDCONCEPT1 = CreateSmallIntegerField("IDCONCEPT1");
			CONCEPTCOLOR1 = CreateStringField("CONCEPTCOLOR1",10);
			CONCEPTNAME1 = CreateStringField("CONCEPTNAME1",3);
			CONCEPTVALUE1 = CreateDecimalField("CONCEPTVALUE1",19,6);
			CONCEPTVALUETOTAL1 = CreateDecimalField("CONCEPTVALUETOTAL1",19,6);
			IDCONCEPT2 = CreateSmallIntegerField("IDCONCEPT2");
			CONCEPTCOLOR2 = CreateStringField("CONCEPTCOLOR2",10);
			CONCEPTNAME2 = CreateStringField("CONCEPTNAME2",3);
			CONCEPTVALUE2 = CreateDecimalField("CONCEPTVALUE2",19,6);
			CONCEPTVALUETOTAL2 = CreateDecimalField("CONCEPTVALUETOTAL2",19,6);
			IDCONCEPT3 = CreateSmallIntegerField("IDCONCEPT3");
			CONCEPTCOLOR3 = CreateStringField("CONCEPTCOLOR3",10);
			CONCEPTNAME3 = CreateStringField("CONCEPTNAME3",3);
			CONCEPTVALUE3 = CreateDecimalField("CONCEPTVALUE3",19,6);
			CONCEPTVALUETOTAL3 = CreateDecimalField("CONCEPTVALUETOTAL3",19,6);
			IDCONCEPT4 = CreateSmallIntegerField("IDCONCEPT4");
			CONCEPTCOLOR4 = CreateStringField("CONCEPTCOLOR4",10);
			CONCEPTNAME4 = CreateStringField("CONCEPTNAME4",3);
			CONCEPTVALUE4 = CreateDecimalField("CONCEPTVALUE4",19,6);
			CONCEPTVALUETOTAL4 = CreateDecimalField("CONCEPTVALUETOTAL4",19,6);
			IDCONCEPT5 = CreateSmallIntegerField("IDCONCEPT5");
			CONCEPTCOLOR5 = CreateStringField("CONCEPTCOLOR5",10);
			CONCEPTNAME5 = CreateStringField("CONCEPTNAME5",3);
			CONCEPTVALUE5 = CreateDecimalField("CONCEPTVALUE5",19,6);
			CONCEPTVALUETOTAL5 = CreateDecimalField("CONCEPTVALUETOTAL5",19,6);
			IDCONCEPT6 = CreateSmallIntegerField("IDCONCEPT6");
			CONCEPTCOLOR6 = CreateStringField("CONCEPTCOLOR6",10);
			CONCEPTNAME6 = CreateStringField("CONCEPTNAME6",3);
			CONCEPTVALUE6 = CreateDecimalField("CONCEPTVALUE6",19,6);
			CONCEPTVALUETOTAL6 = CreateDecimalField("CONCEPTVALUETOTAL6",19,6);
			IDCONCEPT7 = CreateSmallIntegerField("IDCONCEPT7");
			CONCEPTCOLOR7 = CreateStringField("CONCEPTCOLOR7",10);
			CONCEPTNAME7 = CreateStringField("CONCEPTNAME7",3);
			CONCEPTVALUE7 = CreateDecimalField("CONCEPTVALUE7",19,6);
			CONCEPTVALUETOTAL7 = CreateDecimalField("CONCEPTVALUETOTAL7",19,6);
			IDCONCEPT8 = CreateSmallIntegerField("IDCONCEPT8");
			CONCEPTCOLOR8 = CreateStringField("CONCEPTCOLOR8",10);
			CONCEPTNAME8 = CreateStringField("CONCEPTNAME8",3);
			CONCEPTVALUE8 = CreateDecimalField("CONCEPTVALUE8",19,6);
			CONCEPTVALUETOTAL8 = CreateDecimalField("CONCEPTVALUETOTAL8",19,6);
			IDCONCEPT9 = CreateSmallIntegerField("IDCONCEPT9");
			CONCEPTCOLOR9 = CreateStringField("CONCEPTCOLOR9",10);
			CONCEPTNAME9 = CreateStringField("CONCEPTNAME9",3);
			CONCEPTVALUE9 = CreateDecimalField("CONCEPTVALUE9",19,6);
			CONCEPTVALUETOTAL9 = CreateDecimalField("CONCEPTVALUETOTAL9",19,6);
			IDCONCEPT10 = CreateSmallIntegerField("IDCONCEPT10");
			CONCEPTCOLOR10 = CreateStringField("CONCEPTCOLOR10",10);
			CONCEPTNAME10 = CreateStringField("CONCEPTNAME10",3);
			CONCEPTVALUE10 = CreateDecimalField("CONCEPTVALUE10",19,6);
			CONCEPTVALUETOTAL10 = CreateDecimalField("CONCEPTVALUETOTAL10",19,6);
			IDCONCEPT11 = CreateSmallIntegerField("IDCONCEPT11");
			CONCEPTCOLOR11 = CreateStringField("CONCEPTCOLOR11",10);
			CONCEPTNAME11 = CreateStringField("CONCEPTNAME11",3);
			CONCEPTVALUE11 = CreateDecimalField("CONCEPTVALUE11",19,6);
			CONCEPTVALUETOTAL11 = CreateDecimalField("CONCEPTVALUETOTAL11",19,6);
			IDCONCEPT12 = CreateSmallIntegerField("IDCONCEPT12");
			CONCEPTCOLOR12 = CreateStringField("CONCEPTCOLOR12",10);
			CONCEPTNAME12 = CreateStringField("CONCEPTNAME12",3);
			CONCEPTVALUE12 = CreateDecimalField("CONCEPTVALUE12",19,6);
			CONCEPTVALUETOTAL12 = CreateDecimalField("CONCEPTVALUETOTAL12",19,6);
			IDCONCEPT13 = CreateSmallIntegerField("IDCONCEPT13");
			CONCEPTCOLOR13 = CreateStringField("CONCEPTCOLOR13",10);
			CONCEPTNAME13 = CreateStringField("CONCEPTNAME13",3);
			CONCEPTVALUE13 = CreateDecimalField("CONCEPTVALUE13",19,6);
			CONCEPTVALUETOTAL13 = CreateDecimalField("CONCEPTVALUETOTAL13",19,6);
			IDCONCEPT14 = CreateSmallIntegerField("IDCONCEPT14");
			CONCEPTCOLOR14 = CreateStringField("CONCEPTCOLOR14",10);
			CONCEPTNAME14 = CreateStringField("CONCEPTNAME14",3);
			CONCEPTVALUE14 = CreateDecimalField("CONCEPTVALUE14",19,6);
			CONCEPTVALUETOTAL14 = CreateDecimalField("CONCEPTVALUETOTAL14",19,6);
			IDCONCEPT15 = CreateSmallIntegerField("IDCONCEPT15");
			CONCEPTCOLOR15 = CreateStringField("CONCEPTCOLOR15",10);
			CONCEPTNAME15 = CreateStringField("CONCEPTNAME15",3);
			CONCEPTVALUE15 = CreateDecimalField("CONCEPTVALUE15",19,6);
			CONCEPTVALUETOTAL15 = CreateDecimalField("CONCEPTVALUETOTAL15",19,6);
			IDCONCEPT16 = CreateSmallIntegerField("IDCONCEPT16");
			CONCEPTCOLOR16 = CreateStringField("CONCEPTCOLOR16",10);
			CONCEPTNAME16 = CreateStringField("CONCEPTNAME16",3);
			CONCEPTVALUE16 = CreateDecimalField("CONCEPTVALUE16",19,6);
			CONCEPTVALUETOTAL16 = CreateDecimalField("CONCEPTVALUETOTAL16",19,6);
			IDCONCEPT17 = CreateSmallIntegerField("IDCONCEPT17");
			CONCEPTCOLOR17 = CreateStringField("CONCEPTCOLOR17",10);
			CONCEPTNAME17 = CreateStringField("CONCEPTNAME17",3);
			CONCEPTVALUE17 = CreateDecimalField("CONCEPTVALUE17",19,6);
			CONCEPTVALUETOTAL17 = CreateDecimalField("CONCEPTVALUETOTAL17",19,6);
			IDCONCEPT18 = CreateSmallIntegerField("IDCONCEPT18");
			CONCEPTCOLOR18 = CreateStringField("CONCEPTCOLOR18",10);
			CONCEPTNAME18 = CreateStringField("CONCEPTNAME18",3);
			CONCEPTVALUE18 = CreateDecimalField("CONCEPTVALUE18",19,6);
			CONCEPTVALUETOTAL18 = CreateDecimalField("CONCEPTVALUETOTAL18",19,6);
			IDCONCEPT19 = CreateSmallIntegerField("IDCONCEPT19");
			CONCEPTCOLOR19 = CreateStringField("CONCEPTCOLOR19",10);
			CONCEPTNAME19 = CreateStringField("CONCEPTNAME19",3);
			CONCEPTVALUE19 = CreateDecimalField("CONCEPTVALUE19",19,6);
			CONCEPTVALUETOTAL19 = CreateDecimalField("CONCEPTVALUETOTAL19",19,6);
			IDCONCEPT20 = CreateSmallIntegerField("IDCONCEPT20");
			CONCEPTCOLOR20 = CreateStringField("CONCEPTCOLOR20",10);
			CONCEPTNAME20 = CreateStringField("CONCEPTNAME20",3);
			CONCEPTVALUE20 = CreateDecimalField("CONCEPTVALUE20",19,6);
			CONCEPTVALUETOTAL20 = CreateDecimalField("CONCEPTVALUETOTAL20",19,6);
			IDCONCEPT21 = CreateSmallIntegerField("IDCONCEPT21");
			CONCEPTCOLOR21 = CreateStringField("CONCEPTCOLOR21",10);
			CONCEPTNAME21 = CreateStringField("CONCEPTNAME21",3);
			CONCEPTVALUE21 = CreateDecimalField("CONCEPTVALUE21",19,6);
			CONCEPTVALUETOTAL21 = CreateDecimalField("CONCEPTVALUETOTAL21",19,6);
			IDCONCEPT22 = CreateSmallIntegerField("IDCONCEPT22");
			CONCEPTCOLOR22 = CreateStringField("CONCEPTCOLOR22",10);
			CONCEPTNAME22 = CreateStringField("CONCEPTNAME22",3);
			CONCEPTVALUE22 = CreateDecimalField("CONCEPTVALUE22",19,6);
			CONCEPTVALUETOTAL22 = CreateDecimalField("CONCEPTVALUETOTAL22",19,6);
			IDCONCEPT23 = CreateSmallIntegerField("IDCONCEPT23");
			CONCEPTCOLOR23 = CreateStringField("CONCEPTCOLOR23",10);
			CONCEPTNAME23 = CreateStringField("CONCEPTNAME23",3);
			CONCEPTVALUE23 = CreateDecimalField("CONCEPTVALUE23",19,6);
			CONCEPTVALUETOTAL23 = CreateDecimalField("CONCEPTVALUETOTAL23",19,6);
			IDCONCEPT24 = CreateSmallIntegerField("IDCONCEPT24");
			CONCEPTCOLOR24 = CreateStringField("CONCEPTCOLOR24",10);
			CONCEPTNAME24 = CreateStringField("CONCEPTNAME24",3);
			CONCEPTVALUE24 = CreateDecimalField("CONCEPTVALUE24",19,6);
			CONCEPTVALUETOTAL24 = CreateDecimalField("CONCEPTVALUETOTAL24",19,6);
			IDCONCEPT25 = CreateSmallIntegerField("IDCONCEPT25");
			CONCEPTCOLOR25 = CreateStringField("CONCEPTCOLOR25",10);
			CONCEPTNAME25 = CreateStringField("CONCEPTNAME25",3);
			CONCEPTVALUE25 = CreateDecimalField("CONCEPTVALUE25",19,6);
			CONCEPTVALUETOTAL25 = CreateDecimalField("CONCEPTVALUETOTAL25",19,6);
			IDCONCEPT26 = CreateSmallIntegerField("IDCONCEPT26");
			CONCEPTCOLOR26 = CreateStringField("CONCEPTCOLOR26",10);
			CONCEPTNAME26 = CreateStringField("CONCEPTNAME26",3);
			CONCEPTVALUE26 = CreateDecimalField("CONCEPTVALUE26",19,6);
			CONCEPTVALUETOTAL26 = CreateDecimalField("CONCEPTVALUETOTAL26",19,6);
			IDCONCEPT27 = CreateSmallIntegerField("IDCONCEPT27");
			CONCEPTCOLOR27 = CreateStringField("CONCEPTCOLOR27",10);
			CONCEPTNAME27 = CreateStringField("CONCEPTNAME27",3);
			CONCEPTVALUE27 = CreateDecimalField("CONCEPTVALUE27",19,6);
			CONCEPTVALUETOTAL27 = CreateDecimalField("CONCEPTVALUETOTAL27",19,6);
			IDCONCEPT28 = CreateSmallIntegerField("IDCONCEPT28");
			CONCEPTCOLOR28 = CreateStringField("CONCEPTCOLOR28",10);
			CONCEPTNAME28 = CreateStringField("CONCEPTNAME28",3);
			CONCEPTVALUE28 = CreateDecimalField("CONCEPTVALUE28",19,6);
			CONCEPTVALUETOTAL28 = CreateDecimalField("CONCEPTVALUETOTAL28",19,6);
			IDCONCEPT29 = CreateSmallIntegerField("IDCONCEPT29");
			CONCEPTCOLOR29 = CreateStringField("CONCEPTCOLOR29",10);
			CONCEPTNAME29 = CreateStringField("CONCEPTNAME29",3);
			CONCEPTVALUE29 = CreateDecimalField("CONCEPTVALUE29",19,6);
			CONCEPTVALUETOTAL29 = CreateDecimalField("CONCEPTVALUETOTAL29",19,6);
			IDCONCEPT30 = CreateSmallIntegerField("IDCONCEPT30");
			CONCEPTCOLOR30 = CreateStringField("CONCEPTCOLOR30",10);
			CONCEPTNAME30 = CreateStringField("CONCEPTNAME30",3);
			CONCEPTVALUE30 = CreateDecimalField("CONCEPTVALUE30",19,6);
			CONCEPTVALUETOTAL30 = CreateDecimalField("CONCEPTVALUETOTAL30",19,6);
			IDCONCEPT31 = CreateSmallIntegerField("IDCONCEPT31");
			CONCEPTCOLOR31 = CreateStringField("CONCEPTCOLOR31",10);
			CONCEPTNAME31 = CreateStringField("CONCEPTNAME31",3);
			CONCEPTVALUE31 = CreateDecimalField("CONCEPTVALUE31",19,6);
			CONCEPTVALUETOTAL31 = CreateDecimalField("CONCEPTVALUETOTAL31",19,6);
			ABSENCESTEMPLATE = CreateStringField("ABSENCESTEMPLATE",31);
			IDREPORTTASK = CreateDecimalField("IDREPORTTASK",16,0);

			vPKExplicit =true;

			PrimaryKeyList.Add(IDPASSPORT);
			PrimaryKeyList.Add(IDEMPLOYEE);
			PrimaryKeyList.Add(EJERCICIO);
			PrimaryKeyList.Add(PERIODO);
			PrimaryKeyList.Add(IDREPORTTASK);
			
		}
		#endregion

		#region Destructor
		/// <summary>
		/// Override Method for free all resources
		/// Controls that execute this process only one time and stop, if is necessary, the call of destructor by Garbage Collector
		/// </summary>
		/// <param name="pDisposable">boolean that indicates if free managed resources</param>
		protected override void Dispose(bool pDisposable)
		{
			if (!vDisposed)
			{
				//call father Dispose
				base.Dispose(true);
				GC.SuppressFinalize(this);
			}
		}
		#endregion
	}
	#endregion

	#region ORMTmpAnnualConceptsEmployeeRecordSet
	/// <summary>
	/// Class to create a list of TmpAnnualConceptsEmployee records
	/// </summary>
	public class ORMTmpAnnualConceptsEmployeeRecordSet:ORMRecordsetBase<ORMTmpAnnualConceptsEmployeeRecord>
	{
		#region CreateRecord
		/// <summary>
		/// Creates a ORMTmpAnnualConceptsEmployee and adds the object to the list
		/// </summary>
		public ORMTmpAnnualConceptsEmployeeRecord CreateRecord()
		{
			this.Add(new ORMTmpAnnualConceptsEmployeeRecord());
			return this[this.Count - 1];
		}
		#endregion
	}
	#endregion

	#region ORMTmpAnnualConceptsEmployeeBase
	/// <summary>
	/// Class base for basic operations with TmpAnnualConceptsEmployee records
	/// </summary>
	public class ORMTmpAnnualConceptsEmployeeBase : DBTableBase
	{
		#region Constructor
		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="pDBManager">Database manager who creates the object</param>
		public ORMTmpAnnualConceptsEmployeeBase() : base("TmpAnnualConceptsEmployee")
		{
			PKType=DBTableBase.PK_TYPE_EXPLICIT;
		}
		#endregion

		#region Create
		/// <summary>
		/// Creates a ORMTmpAnnualConceptsEmployee record
		/// </summary>
		/// <returns>ORMTmpAnnualConceptsEmployee record</returns>
        public ORMTmpAnnualConceptsEmployeeRecord CreateRecord()
        {
            ORMTmpAnnualConceptsEmployeeRecord vRecord;
            vRecord = new ORMTmpAnnualConceptsEmployeeRecord();

			return vRecord;
		}
		/// <summary>
		/// Create a list of ORMTmpAnnualConceptsEmployee record
		/// </summary>
		/// <returns>List of ORMTmpAnnualConceptsEmployee record</returns>
		public ORMTmpAnnualConceptsEmployeeRecordSet CreateRecordSet()
		{
			ORMTmpAnnualConceptsEmployeeRecordSet vRecordSet;
			vRecordSet = new ORMTmpAnnualConceptsEmployeeRecordSet();

			return vRecordSet;
		}
		#endregion

		//public functions

		#region Insert
		/// <summary>
		/// Insert a tblTmpAnnualConceptsEmployee record to the table. Use a database resource
		/// </summary>
		/// <param name="pORMTmpAnnualConceptsEmployeeRecord">Record to insert</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMTmpAnnualConceptsEmployeeRecord pORMRecord, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMInsertQuery vQuery;

			vTrace.TraceVerbose(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Insert: {0}", TraceRecord(pORMRecord)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery = new ORMInsertQuery();

						try
						{
							vQuery.InsertInto(this).Fields(pORMRecord);
							vQuery.Execute(pDBConnection);
						}
						catch (Exception ex)
						{
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpAnnualConceptsEmployeeBase.Insert: {0}", ex.Message));
						}
						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Insert: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Insert a tblTmpAnnualConceptsEmployee record to the table. Create a database resource
		/// </summary>
		/// <param name="pORMRecord">Record to insert</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMTmpAnnualConceptsEmployeeRecord pORMRecord)
		{
			return Insert(pORMRecord, null);
		}
		/// <summary>
		/// Insert the tblTmpAnnualConceptsEmployee records to the table. Use a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet,DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			bool TransactionOK;
			ORMTmpAnnualConceptsEmployeeRecord vDBRecord;
			bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Insert: {0}",  pDBRecordSet.Count.ToString()));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						TransactionOK = pDBConnection.StartTransaction();
						try
						{
							for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
							{
								vDBRecord=pDBRecordSet[vIndex];
								vResult = Insert(vDBRecord, pDBConnection);

								if (vResult != ErrorCode.NO_ERROR) // Exit for.
								{
									break;
								}
							}
						}
						finally
						{
							if (TransactionOK)
							{
								pDBConnection.EndTransaction(vResult == ErrorCode.NO_ERROR);
							}
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Insert: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Insert the tblTmpAnnualConceptsEmployee records to the table. Create a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record to insert</param>
		/// <returns>The result of execute insert process</returns>
		public int Insert(ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Insert(pDBRecordSet, null);
		}
		#endregion
		
		#region Get
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select by primary key using Record. Use a database resource
		/// </summary>
		/// <param name="pORMRecord">Record to get</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpAnnualConceptsEmployeeRecord pORMRecord, ORMField[] pSelectFields, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMSelectQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Get: {0}", TraceFields(pORMRecord.PrimaryKeyList)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						if (pORMRecord.PrimaryKeyNotNull()) // Primary key constraint.
						{
							vORMFilter = ORMRecordToORMFilter(pORMRecord, true);
							vQuery = CreateSelect();

							try
							{
								//Select all records or a specific list of fields
								if (ReferenceEquals(pSelectFields,null) || pSelectFields.Length==0)
								{
									vQuery.Select(this);
								}
								else
								{
									vQuery.Select(pSelectFields);
								}
								vQuery.From(this).Where(vORMFilter, false);

								vQuery.Open(pDBConnection);
								if (vQuery.Next())
								{
									vQuery.GetRecordValues(pORMRecord);
								}
								else
								{
									vResult = ErrorCode.ErrorDatabase.DBF_RECORD_NOT_EXISTS;
								}

							}
							catch (Exception ex)
							{ 
								vResult = ErrorCode.ERROR_EXCEPTION;
								vTrace.TraceException(string.Format("Exception ORMTmpAnnualConceptsEmployeeBase.Get: {0}", ex.Message));
							}

							vORMFilter.Dispose();
							vORMFilter=null;
							vQuery.Dispose();
							vQuery=null;
						}
						else
						{
							vResult = ErrorCode.ErrorDatabase.DBF_PRIMARY_KEY_IS_NULL;
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Get: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select by primary key using Record. Create a database resource
		/// </summary>
		/// <param name="pORMRecord">Record to get</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpAnnualConceptsEmployeeRecord pORMRecord, ORMField[] pSelectFields)
		{
			DBConnection vDBConnection = null;

			return Get(pORMRecord, pSelectFields, vDBConnection);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select by primary key using RecordSet. Use a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to get the records</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet, ORMField[] pSelectFields, DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			ORMTmpAnnualConceptsEmployeeRecord vDBRecord;
			bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Get: {0}",  pDBRecordSet.Count.ToString()));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
						{
							vDBRecord=pDBRecordSet[vIndex];
							vResult = Get(vDBRecord, pSelectFields, pDBConnection);

							if (vResult != ErrorCode.NO_ERROR) // Exit for.
							{
								break;
							}
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Get: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select by primary key using RecordSet. Create a database resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to get the records</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet, ORMField[] pSelectFields)
		{
			return Get(pDBRecordSet, pSelectFields, null);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select by a filter using Record with order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpAnnualConceptsEmployeeRecord pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet,DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;
			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			vResult = Get(vORMFilter, pSelectFields, pOrderFields, GetOrderByArray(pDescendent,pOrderFields), pDBRecordSet, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select by a filter using Record with order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpAnnualConceptsEmployeeRecord pORMFilter, ORMField[] pSelectFields,ORMField[] pOrderFields,
			bool pDescendent, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee without order. Select by a filter using Record without order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpAnnualConceptsEmployeeRecord pORMFilter, ORMField[] pSelectFields,
			ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			return Get(pORMFilter, pSelectFields, null, false, pDBRecordSet, pDBConnection);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee without order. Select by a filter using Record without order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMTmpAnnualConceptsEmployeeRecord pORMFilter, ORMField[] pSelectFields, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select by a filter with order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array with Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			ORMTmpAnnualConceptsEmployeeRecord vDBRecord;
			bool vDBNotAssigned;
			ORMSelectQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Get: {0}",  TraceFilter(pORMFilter)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery = CreateSelect();
						try
						{
							//Select all records or a specific list of fields
							if (ReferenceEquals(pSelectFields,null) || pSelectFields.Length==0)
							{
								vQuery.Select(this);
							}
							else
							{
								vQuery.Select(pSelectFields);
							}
							vQuery.From(this).Where(pORMFilter, false).OrderBy(pOrderFields,pDescendent);

							vQuery.Open(pDBConnection);
							while (vQuery.Next())
							{
								vDBRecord=pDBRecordSet.CreateRecord();
								vQuery.GetRecordValues(vDBRecord);
							}
						}
						catch (Exception ex)
						{ 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpAnnualConceptsEmployeeBase.Get: {0}", ex.Message));
						}

						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Get: {0}",   ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select by a filter with order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pOrderFields, GetOrderByArray(pDescendent, pOrderFields), pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select by a filter with order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee without order. Select by a filter without order by. Use a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter,ORMField[] pSelectFields,
			ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			return Get(pORMFilter, pSelectFields, null, null, pDBRecordSet, pDBConnection);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee without order. Select by a filter without order by. Create a database resource
		/// </summary>
		/// <param name="pORMFilter">Filter for the select sentence</param>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMFilter pORMFilter, ORMField[] pSelectFields,
			ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Get(pORMFilter, pSelectFields, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select all records with order by. Use a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			ORMFilter vORMFilter;

			vORMFilter = CreateFilter();
			vResult = Get(vORMFilter, pSelectFields, pOrderFields, GetOrderByArray(pDescendent, pOrderFields), pDBRecordSet, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select all records with order by. Use a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			ORMFilter vORMFilter;

			vORMFilter = CreateFilter();
			vResult = Get(vORMFilter, pSelectFields, pOrderFields, pDescendent, pDBRecordSet, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select all records with order by. Create a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Descendent (or ascendent) order</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool pDescendent, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Get(pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select all records with order by. Create a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pOrderFields">Fields to set the record order</param>
		/// <param name="pDescendent">Array Descendent (or ascendent) orders</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		public int Get(ORMField[] pSelectFields, ORMField[] pOrderFields,
			bool[] pDescendent, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Get(pSelectFields, pOrderFields, pDescendent, pDBRecordSet, null);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select all records without order by. Use a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMField[] pSelectFields, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			return Get(pSelectFields, null, false, pDBRecordSet, pDBConnection);
		}
		/// <summary>
		/// Select records to the table TmpAnnualConceptsEmployee. Select all records without order by. Create a database resource
		/// </summary>
		/// <param name="pSelectFields">Fields for the selection</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute get process</returns>
		public int Get(ORMField[] pSelectFields, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Get(pSelectFields, pDBRecordSet, null);
		}
        /// <summary>
        /// Select records to the view TmpAnnualConceptsEmployee. Select owner database select query. Use a database resource
        /// </summary>
        /// <param name="pQuery">Query to execute</param>
        /// <param name="pSelectFields">Fields for the selection</param>
        /// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
        /// <param name="pDBConnection">Database resource to use</param>
        /// <returns>The result of execute get process</returns>
        public int Get(ORMSelectQuery pQuery, ORMField[] pSelectFields, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet, DBConnection pDBConnection)
        {
        	int vResult =  ErrorCode.NO_ERROR;
        	ORMTmpAnnualConceptsEmployeeRecord vDBRecord;
        	bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Get: {0}",  TraceFilter(pQuery.ORMFilter)));

        	vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

        	if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
        	{
        		try
        		{
        			if (pDBConnection.Connected) // Check if database is connected.
        			{
                        try
                        {
                            pQuery.Open(pDBConnection);
                            while (pQuery.Next())
                            {
                                vDBRecord=pDBRecordSet.CreateRecord();
                                pQuery.GetRecordValues(vDBRecord);
                            }
                        }
                        catch (Exception ex)
                        { 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpAnnualConceptsEmployeeBase.Get: {0}", ex.Message));
						}

                        pQuery.Dispose();
                        pQuery=null;
        			}
        			else
					{
			        	vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
	        	}
	        	finally
	        	{
        			ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
        		}
        	}
        	else
			{
	        	vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Get: {0}",  ErrorCode.GetError(vResult)));
			return vResult;
		}
        /// <summary>
        /// Select records to the view TmpAnnualConceptsEmployee. Select owner database select query.
        /// </summary>
        /// <param name="pQuery">Query to execute</param>
        /// <param name="pSelectFields">Fields for the selection</param>
        /// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
        /// <returns>The result of execute get process</returns>
        public int Get(ORMSelectQuery pQuery, ORMField[] pSelectFields, ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
        {
        	return Get(pQuery,pSelectFields, pDBRecordSet, null);
        }
        #endregion

		#region Update
		/// <summary>
		/// Update the TmpAnnualConceptsEmployee records. Update by primary key using Record. Use a Database resource
		/// </summary>
		/// <param name="pORMRecord">Record to update</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpAnnualConceptsEmployeeRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;

			if (pORMFilter.PrimaryKeyNotNull()) // Primary key constraint.
			{
				vORMFilter = ORMRecordToORMFilter(pORMFilter, true);
				vResult = Update(pORMFilter, vORMFilter, pDBConnection);
				vORMFilter.Dispose();
				vORMFilter=null;
			}
			else
			{
				vResult=ErrorCode.ErrorDatabase.DBF_PRIMARY_KEY_IS_NULL;
			}

			return vResult;
		}
		/// <summary>
		/// Update the TmpAnnualConceptsEmployee records. Update by primary key using Record. Create a Database resource
		/// </summary>
		/// <param name="pORMRecord">Record to update</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpAnnualConceptsEmployeeRecord pORMRecord)
		{
			DBConnection vDBConnection = null;

			return Update(pORMRecord, vDBConnection);
		}
		/// <summary>
		/// Update the TmpAnnualConceptsEmployee records. Update by primary key using RecordSet. Use a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to update the records</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet, DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			bool TransactionOK;
			ORMTmpAnnualConceptsEmployeeRecord vDBRecord;
			bool vDBNotAssigned;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Update: {0}",  pDBRecordSet.Count.ToString()));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						TransactionOK = pDBConnection.StartTransaction();
						try
						{
							for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
							{
								vDBRecord=pDBRecordSet[vIndex];
								vResult = Update(vDBRecord, pDBConnection);

								if (vResult != ErrorCode.NO_ERROR) // Exit for.
								{
									break;
								}
							}
						}
						finally
						{
							if (TransactionOK)
							{
								pDBConnection.EndTransaction(vResult == ErrorCode.NO_ERROR);
							}
						}
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Update: {0}",  ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Update the TmpAnnualConceptsEmployee records. Update by primary key using RecordSet. Create a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to update the records</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Update(pDBRecordSet, null);
		}
		/// <summary>
		/// Update the TmpAnnualConceptsEmployee records by filter. Update by a filter using Record. Use a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpAnnualConceptsEmployeeRecord pORMRecord, ORMTmpAnnualConceptsEmployeeRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			//check if filter has elements
			if (vORMFilter.Params.Count == 0)
			{
				//TraceError(vTableName + ": Update by Record-Filter","ORMFilter is empty");
				vResult = ErrorCode.ErrorDatabase.DBF_FILTER_EMPTY;
			}
			vResult = Update(pORMRecord, vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Update the TmpAnnualConceptsEmployee records by filter. Update by a filter using Record. Create a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpAnnualConceptsEmployeeRecord pORMRecord, ORMTmpAnnualConceptsEmployeeRecord pORMFilter)
		{
			return Update(pORMRecord, pORMFilter, null);
		}
		/// <summary>
		/// Update the TmpAnnualConceptsEmployee records by filter. Update by a filter. Use a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pORMFilter">Filter for the update sentence</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpAnnualConceptsEmployeeRecord pORMRecord, ORMFilter pORMFilter, DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMUpdateQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Update: {0}", TraceRecord(pORMRecord)));

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery = new ORMUpdateQuery();
						try
						{
							vQuery.Update(this).Set_(pORMRecord).Where(pORMFilter, false);
							vQuery.Execute(pDBConnection);
						}
						catch (Exception ex)
						{ 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpAnnualConceptsEmployeeBase.Update: {0}", ex.Message));
						}
						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Update: {0}",  ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Update the TmpAnnualConceptsEmployee records by filter. Update by a filter. Create a Database Resource
		/// </summary>
		/// <param name="pORMRecord">Record with the fields to update</param>
		/// <param name="pDBRecordSet">Record list with the records affected by the filter</param>
		/// <returns>The result of execute update process</returns>
		public int Update(ORMTmpAnnualConceptsEmployeeRecord pORMRecord, ORMFilter pORMFilter)
		{
			return Update(pORMRecord, pORMFilter, null);
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes the TmpAnnualConceptsEmployee records. Delete by primary key using Recordset. Use a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to delete the records</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet,DBConnection pDBConnection)
		{
			int vIndex;
			int vResult =  ErrorCode.NO_ERROR;
			bool TransactionOK, vDBNotAssigned;
			ORMTmpAnnualConceptsEmployeeRecord vDBRecord;
			ORMFilter vORMFilter;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Delete: {0}", pDBRecordSet.Count.ToString()));
			
			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						TransactionOK = pDBConnection.StartTransaction();
						try
						{
							for (vIndex = 0; vIndex < pDBRecordSet.Count; vIndex++)
							{
								vDBRecord=pDBRecordSet[vIndex];

								if (vDBRecord.PrimaryKeyNotNull()) // Primary key constraint.
								{
									vORMFilter = ORMRecordToORMFilter(vDBRecord, true);
									vResult = Delete(vORMFilter, pDBConnection);
									vORMFilter.Dispose();
									vORMFilter=null;
								}
								else
								{
									vResult=ErrorCode.ErrorDatabase.DBF_PRIMARY_KEY_IS_NULL;
								}

								if (vResult != ErrorCode.NO_ERROR) // Exit for.
								{
									break;
								}
							}
						}
						finally
						{
							if (TransactionOK)
							{
								pDBConnection.EndTransaction(vResult == ErrorCode.NO_ERROR);
							}
						}
					}
					else
					{
						vResult= ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult= ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Delete: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Deletes the TmpAnnualConceptsEmployee records. Delete by primary key using Recordset. Create a Database Resource
		/// </summary>
		/// <param name="pDBRecordSet">Record list to delete the records</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMTmpAnnualConceptsEmployeeRecordSet pDBRecordSet)
		{
			return Delete(pDBRecordSet, null);
		}
		/// <summary>
		/// Deletes the TmpAnnualConceptsEmployee records. Delete by a filter using Record. Using Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMTmpAnnualConceptsEmployeeRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  ErrorCode.NO_ERROR;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);

			//check if filter has elements
			if (vORMFilter.Params.Count == 0)
			{
				//TraceError(vTableName + ": Delete by Record-Filter","ORMFilter is empty");
				vResult= ErrorCode.ErrorDatabase.DBF_FILTER_EMPTY;
			}
			vResult = Delete(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Deletes the TmpAnnualConceptsEmployee records. Delete by a filter using Record. Create Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMTmpAnnualConceptsEmployeeRecord pORMFilter)
		{
			return Delete(pORMFilter, null);
		}
		/// <summary>
		/// Deletes the TmpAnnualConceptsEmployee records. Delete by a filter. Using a Datbase Resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMFilter pORMFilter,DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMDeleteQuery vQuery;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Delete: {0}", TraceFilter(pORMFilter)));
			

			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vQuery=new ORMDeleteQuery();
						try
						{
							vQuery.DeleteFrom(this).Where(pORMFilter,false);
							vQuery.Execute(pDBConnection);

						}
						catch (Exception ex)
						{ 
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpAnnualConceptsEmployeeBase.Delete: {0}", ex.Message));
						}
						vQuery.Dispose();
						vQuery=null;
					}
					else
					{
						vResult= ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult= ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Delete: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Deletes the TmpAnnualConceptsEmployee records. Delete by a filter. Create a Datbase Resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(ORMFilter pORMFilter)
		{
			return Delete(pORMFilter, null);
		}
		/// <summary>
		/// Deletes TmpAnnualConceptsEmployee records. Delete All. Using a Database Resource
		/// </summary>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>The result of execute delete process</returns>
		public int Delete(DBConnection pDBConnection)
        {
            int vResult = ErrorCode.NO_ERROR;
            bool vDBNotAssigned;
            ORMDeleteQuery vQuery;

            vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.DeleteAll"));


            vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

            if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
            {
                try
                {
                    if (pDBConnection.Connected) // Check if database is connected.
                    {
                        vQuery = new ORMDeleteQuery();
                        try
                        {
                            vQuery.DeleteFrom(this);
                            vQuery.Execute(pDBConnection);

                        }
                        catch (Exception ex)
                        {
                            vResult = ErrorCode.ERROR_EXCEPTION;
                            vTrace.TraceException(string.Format("Exception ORMTmpAnnualConceptsEmployeeBase.DeleteAll: {0}", ex.Message));
                        }
                        vQuery.Dispose();
                        vQuery = null;
                    }
                    else
                    {
                        vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
                    }
                }
                finally
                {
                    ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
                }
            }
            else
            {
                vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
            }

            vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.DeleteAll: {0}", ErrorCode.GetError(vResult)));
            return vResult;
        }
        /// <summary>
        /// Deletes TmpAnnualConceptsEmployee records. Delete All.
        /// </summary>
        /// <param name="pDBConnection">Database resource to use</param>
        /// <returns>The result of execute delete process</returns>
        public int Delete()
        {
            DBConnection vDBConnection = null;

            return Delete(vDBConnection);
        }
		#endregion

		#region Count
		/// <summary>
		/// Return the number of records by a filter. Using Recond and Database Resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMTmpAnnualConceptsEmployeeRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  0;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			vResult = Count(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Return the number of records by a filter. Using Recond and Create Database Resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMTmpAnnualConceptsEmployeeRecord pORMFilter)
		{
			return Count(pORMFilter, null);
		}
		/// <summary>
		/// Return the number of records by a filter. Using Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMFilter pORMFilter,DBConnection pDBConnection)
		{
			int vResult =  ErrorCode.NO_ERROR;
			bool vDBNotAssigned;
			ORMSelectQuery vQuery;
			ORMIntegerField vDBTotal;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Count: {0}", TraceFilter(pORMFilter)));
			
			vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

			if (GetDBConnection(ref pDBConnection, vDBNotAssigned))
			{
				try
				{
					if (pDBConnection.Connected) // Check if database is connected.
					{
						vDBTotal= new ORMIntegerField("TOTAL");

						vQuery=CreateSelect();
						try
						{
							vQuery.Select("COUNT(*) AS TOTAL").From(this).Where(pORMFilter, false);
							vQuery.Open(pDBConnection);
							if (vQuery.Next())
							{
								vQuery.GetFieldValue(vDBTotal);
							}
							vResult = vDBTotal.Value;
						}
						catch (Exception ex)
						{
							vResult = ErrorCode.ERROR_EXCEPTION;
							vTrace.TraceException(string.Format("Exception ORMTmpAnnualConceptsEmployeegBase.Count: {0}", ex.Message)); 
						}
						vQuery.Dispose();
						vQuery=null;
						vDBTotal=null;
					}
					else
					{
						vResult = ErrorCode.ErrorDatabase.DBF_DATABASE_NOT_CONNECTED;
					}
				}
				finally
				{
					ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
				}
			}
			else
			{
				vResult = ErrorCode.ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE;
			}

			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Count: {0}", ErrorCode.GetError(vResult)));
			return vResult;
		}
		/// <summary>
		/// Return the number of records by a filter. Create Database resource
		/// </summary>
		/// <param name="pFilter">Record to create the filter</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(ORMFilter pORMFilter)
		{
			return Count(pORMFilter, null);
		}
		/// <summary>
		/// Return the number of records in the table. Using database resource
		/// </summary>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count(DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			int vResult =  0;

			vORMFilter = CreateFilter();
			vResult = Count(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Return the number of records in the table. Create database resource
		/// </summary>
		/// <returns>Number of records. Negative is there is an error</returns>
		public int Count()
		{
			DBConnection vDBConnection = null;

			return Count(vDBConnection);
		}
		#endregion

		#region Exist
		/// <summary>
		/// Returns if there are filtered records. Using Record and Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMTmpAnnualConceptsEmployeeRecord pORMFilter, DBConnection pDBConnection)
		{
			ORMFilter vORMFilter;
			bool vResult = false;

			vORMFilter = ORMRecordToORMFilter(pORMFilter, false);
			vResult = Exists(vORMFilter, pDBConnection);
			vORMFilter.Dispose();
			vORMFilter=null;
			return vResult;
		}
		/// <summary>
		/// Returns if there are filtered records. Unsing Record and create a Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to use like a filter</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMTmpAnnualConceptsEmployeeRecord pORMFilter)
		{
			return Exists(pORMFilter, null);
		}
		/// <summary>
		/// Returns if there are filtered records. Using Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMFilter pORMFilter,DBConnection pDBConnection)
		{
			bool vResult;

			vTrace.TraceInfo(string.Format("Enter ORMTmpAnnualConceptsEmployeeBase.Exists: {0}", TraceFilter(pORMFilter)));
			vResult = (Count(pORMFilter, pDBConnection) > 0);
			vTrace.TraceInfo(string.Format("Leave ORMTmpAnnualConceptsEmployeeBase.Exists: {0}",  vResult.ToString()));
			
			return vResult;
		}
		/// <summary>
		/// Returns if there are filtered records. Create Database resource
		/// </summary>
		/// <param name="pORMFilter">Record to create the filter</param>
		/// <returns>True if there are filtered records</returns>
		public bool Exists(ORMFilter pORMFilter)
		{
			return Exists(pORMFilter, null);
		}
		/// <summary>
		/// Returns if table is empty. Using Database resource
		/// </summary>
		/// <param name="pDBConnection">Database resource to use</param>
		/// <returns>True if the table is empty</returns>
		public bool isEmpty(DBConnection pDBConnection)
		{
			return (Count(pDBConnection)==0);
		}
		/// <summary>
		/// Returns if table is empty. Create Database resource
		/// </summary>
		/// <returns>True if the table is empty</returns>
		public bool isEmpty()
		{
			return isEmpty(null);
		}
		#endregion
	}
	#endregion
}
