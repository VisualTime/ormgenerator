using Robotics.DataLayer.ORM.Database;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Query
{
    /// <summary>
    /// Class to implement a delete SQL sentence
    /// </summary>
    public class ORMDeleteQuery : ORMQuery
    {
        //public functions 

        #region DeleteFrom
        /// <summary>
        /// Initializes the delete using a table
        /// </summary>
        /// <param name="pORMTable">Table to add into DELETE FROM part</param>
        /// <returns>Delete SQL sentence class</returns>
        public virtual ORMDeleteQuery DeleteFrom(DBTableBase pORMTable)
        {
            this.Clear();

            this.vSQLText.Append(SQL_DELETE);
            this.vSQLText.Append(" ");
            if (pORMTable.DatabaseName.Length > 0)
                this.vSQLText.Append(pORMTable.DatabaseName + "..");
            this.vSQLText.Append(pORMTable.TableName);

            this.vSQLStep = STEP_DELETE;

            return this;
        }
        #endregion

        #region Where
        /// <summary>
        /// Initializes the WHERE sentence including a filter
        /// </summary>
        /// <param name="pORMFilter">Database filter to include</param>
        /// <param name="pFreeOnDestroy">Indicates if the filter is destroyed with the class</param>
        public void Where(ORMFilter pORMFilter, bool pFreeOnDestroy)
        {
            // First assign ORMFilter and then add SQL
            this.AssignORMFilter(pORMFilter, pFreeOnDestroy);
            this.AddSQLORMFilter(" " + SQL_WHERE + " ");

            this.CheckSQLSyntax(new byte[] { STEP_DELETE }, STEP_END);
        }
        /// <summary>
        /// Initializes the WHERE sentence including a filter
        /// </summary>
        /// <param name="pORMFilter">Database filter to include</param>
        public void Where(ORMFilter pORMFilter)
        {
            this.Where(pORMFilter, true);
        }
        #endregion
    }
}