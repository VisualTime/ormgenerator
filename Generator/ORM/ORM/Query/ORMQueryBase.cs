﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Robotics.DataLayer.ORM.Database;
using Robotics.DataLayer.ORM.Field;

namespace Robotics.DataLayer.ORM.Query
{
    /// <summary>
    /// Class to implement the SQL text base for a all SQL querys
    /// </summary>
    public class ORMQueryBase : IDisposable
    {
        #region Constants

        public const string DBF_PARAMETER = "PARAM";

        //Steps for SQL Sentences
        public const byte STEP_NONE = 0x00;

        public const byte STEP_INSERT = 0x01;

        public const byte STEP_FIELDS = 0x02;

        public const byte STEP_VALUES = 0x03;

        public const byte STEP_UPDATE = 0x04;

        public const byte STEP_SET = 0x05;

        public const byte STEP_RECORD_SET = 0x06;

        public const byte STEP_DELETE = 0x07;

        public const byte STEP_SELECT = 0x08;

        public const byte STEP_FROM = 0x09;

        public const byte STEP_WHERE = 0x0A;

        public const byte STEP_ORDER_BY = 0x0D;

        public const byte STEP_FILTER_EXPR = 0x0E;

        public const byte STEP_FILTER_EXPR_WITHOUT_VALUE = 0xF;

        public const byte STEP_FILTER_VALUE = 0x10;

        public const byte STEP_SELECT_TOP = 0x11;

        public const byte STEP_GROUP_BY = 0x12;

        public const byte STEP_END = 0xFF;

        // Constant for SQL sentences.
        public const string SQL_INSERT = "INSERT INTO";

        public const string SQL_VALUES = "VALUES";

        public const string SQL_UPDATE = "UPDATE";

        public const string SQL_SET = "SET";

        public const string SQL_DELETE = "DELETE FROM";

        public const string SQL_SELECT = "SELECT";

        public const string SQL_FROM = "FROM";

        public const string SQL_LJOIN = "LEFT JOIN";

        public const string SQL_IJOIN = "INNER JOIN";

        public const string SQL_WHERE = "WHERE";

        public const string SQL_ORDER_BY = "ORDER BY";

        public const string SQL_ASCENDENT = "ASC";

        public const string SQL_DESCENDENT = "DESC";

        public const string SQL_ALIAS = " AS ";

        public const string SQL_GROUP_BY = "GROUP BY";

        public const string SQL_TOP = "TOP";

        public const string SQL_MIN = "MIN";

        public const string SQL_MAX = "MAX";

        public const string SQL_COUNT = "COUNT";

        public const string SQL_SUM = "SUM";

        public const string SQL_AVG = "AVG";

        // Constant errors.
        public const string ERR_INVALID_SQL_SYNTAX = "Invalid SQL syntax: {0}";

        public const string ERR_SEQUENCE_NOT_EXIST = "Sequence Not Exist: {0}";

        public const string ERR_SQL_NOT_ALLOWED_IN_THE_CURRENT_SECTION = "SQL text not allowed in the current section";

        public const string ERR_DBCONNECTION_NOT_ASSIGNED = "The database connection is not assigned";

        #endregion

        #region Variables

        protected bool vDisposed = false; //variable for free memory

        protected StringBuilder vSQLText;

        protected List<ORMField> vParams;

        protected byte vSQLStep;

        #endregion

        #region Properties

        /// <summary>
        ///  SQL sentence
        /// </summary>
        public string SQLText => vSQLText.ToString();

        /// <summary>
        ///  Params List
        /// </summary>
        public List<ORMField> Params => vParams;

        #endregion

        #region Constructor

        /// <summary>
        ///  Constructor
        /// </summary>
        public ORMQueryBase()
        {
            vSQLText = new StringBuilder();
            vParams = new List<ORMField>();
            vSQLStep = STEP_NONE;
        }

        #endregion

        #region Destructor

        /// <summary>
        ///  Destructor
        /// </summary>
        ~ORMQueryBase()
        {
            Dispose(false);
        }

        /// <summary>
        /// Override Method for free all resources
        /// Controls that execute this process only one time and stop, if is necessary, the call of destructor by Garbage Collector
        /// </summary>
        /// <param name="vDisposed">boolean that indicates if free managed resources</param>
        protected virtual void Dispose(bool pDisposable)
        {
            if (!vDisposed)
            {
                if (pDisposable)
                {
                    vParams.Clear();
                    vSQLText.Clear();
                }

                vDisposed = true;
            }
        }

        /// <summary>
        /// Method IDisposable for destroy the class
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        //protected functions

        #region AppendSQLText

        /// <summary>
        ///  Append new SQL sentence text
        /// </summary>
        /// <param name="pSQLText">SQL sentence to add</param>
        protected void AppendSQLText(string pSQLText)
        {
            vSQLText.Append(pSQLText);
        }

        protected void AppendSQLBlankSpace()
        {
            vSQLText.Append(' ');
        }

        #endregion

        #region Exceptions

        /// <summary>
        /// Raises the exception with the invalid SQL syntax
        /// </summary>
        protected void RaiseInvalidSQLSyntax()
        {
            throw new Exception(string.Format(ERR_INVALID_SQL_SYNTAX, vSQLText));
        }

        /// <summary>
        /// Raises the exception with the invalid SQL text in the section
        /// </summary>
        protected void RaiseSQLNotAllowed()
        {
            throw new Exception(ERR_SQL_NOT_ALLOWED_IN_THE_CURRENT_SECTION);
        }

        /// <summary>
        /// Raises the exception with Sequence not exist
        /// </summary>
        protected void RaiseSequenceNotExist(string SequenceName)
        {
            throw new Exception(string.Format(ERR_SEQUENCE_NOT_EXIST, SequenceName));
        }

        #endregion

        #region CheckSQLSyntax

        /// <summary>
        /// Check the SQL Syntax and raise an exception if the syntax is incorrect
        /// </summary>
        /// <param name="pValidSteps">Valid Steps for the current syntax</param>
        public virtual void CheckSQLSyntax(byte[] pValidSteps)
        {
            var vValidSteps = new HashSet<byte>(pValidSteps);

            if (!vValidSteps.Contains(vSQLStep))
            {
                RaiseInvalidSQLSyntax();
            }
        }

        /// <summary>
        ///  Check the SQL Syntax and raise an exception if the syntax is incorrect. Change state of sentence if is correct
        /// </summary>
        /// <param name="pValidSteps">Valid Steps for the current syntax</param>
        /// <param name="pCurrentStep">Next state of sentence</param>
        public virtual void CheckSQLSyntax(byte[] pValidSteps, byte pCurrentStep)
        {
            CheckSQLSyntax(pValidSteps);
            vSQLStep = pCurrentStep;
        }

        #endregion

        //public functions

        #region SetParamsTo

        /// <summary>
        /// Set the value for all params in the database connection
        /// </summary>
        /// <param name="pDBConnection"> Database connection for the query</param>
        public void SetParamsTo(DBConnection pDBConnection)
        {
            int vIndex;

            for (vIndex = 0; vIndex < vParams.Count; vIndex++) 
			{
				vParams[vIndex].SetValueTo(pDBConnection.DBQuery.Parameters);
			}
        }

        #endregion

        #region AddParams

        /// <summary>
        /// Add a new byte parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddByteParam(string pParamName, byte pValue)
        {
            var vDBParam = new ORMByteField(pParamName) {Value = pValue};
            vParams.Add(vDBParam);
        }

        /// <summary>
        /// Add a new integer parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddIntegerParam(string pParamName, int pValue)
        {
            var vDBParam = new ORMIntegerField(pParamName) {Value = pValue};
            vParams.Add(vDBParam);
        }

        /// <summary>
        /// Add a new unsigned integer parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddIntegerParam(string pParamName, uint pValue)
        {
            var vDBParam = new ORMIntegerField(pParamName) {AsUnInteger = pValue};
            vParams.Add(vDBParam);
        }

        /// <summary>
        /// Add a new small integer parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddSmallIntegerParam(string pParamName, short pValue)
        {
            var vDBParam = new ORMSmallIntegerField(pParamName) {AsSmallInteger = pValue};
            vParams.Add(vDBParam);
        }

        /// <summary>
        /// Add a new big integer parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddBigIntegerParam(string pParamName, long pValue)
        {
            var vDBParam = new ORMBigIntegerField(pParamName) {AsBigInteger = pValue};
            vParams.Add(vDBParam);
        }

        /// <summary>
        /// Add a new float parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddFloatParam(string pParamName, double pValue)
        {
            var vDBParam = new ORMFloatField(pParamName) {Value = pValue};
            vParams.Add(vDBParam);
        }

        /// <summary>
        /// Add a new integer parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddDecimalParam(string pParamName, decimal pValue)
        {
            var vDBParam = new ORMDecimalField(pParamName) {Value = pValue};
            vParams.Add(vDBParam);
        }

        /// <summary>
        /// Add a new string parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddStringParam(string pParamName, string pValue)
        {
            var vDBParam = new ORMStringField(pParamName) {Value = pValue};
            vParams.Add(vDBParam);
        }

        /// <summary>
        /// Add a new datetime parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddDateTimeParam(string pParamName, DateTime pValue)
        {
            var vDBParam = new ORMDateTimeField(pParamName) {Value = pValue};
            vParams.Add(vDBParam);
        }

        /// <summary>
        /// Add a new boolchar parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddBooleanParam(string pParamName, bool pValue)
        {
            var vDBParam = new ORMBooleanField(pParamName) {AsBoolean = pValue};
            vParams.Add(vDBParam);
        }

        /// <summary>
        /// Add a new boolchar parameter to the parameter list
        /// </summary>
        /// <param name="pParamName">Name of the parameter</param>
        /// <param name="pValue">Value for the new parameter</param>
        public void AddGuidParam(string pParamName, Guid pValue)
        {
            var vDBParam = new ORMGuidField(pParamName) {AsGuid = pValue};
            vParams.Add(vDBParam);
        }

        #endregion
    }
}
