using Robotics.DataLayer.ORM.Record;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Query
{
    using Database;
    using System;
    using System.Text;

    /// <summary>
    /// Class to implement a insert SQL sentence
    /// </summary>
    public class ORMInsertQuery : ORMQuery
    {
        #region Property
        /// <summary>
        /// Table tyaht insert info
        /// </summary>
        public DBTableBase ORMTable
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        /// <summary>
        ///  Constructor
        /// </summary>
        public ORMInsertQuery()
            : base()
        {
        }
        #endregion

        #region Destructor
        /// <summary>
        /// Override Method for free all resources
        /// Controls that execute this process only one time and stop, if is necessary, the call of destructor by Garbage Collector
        /// </summary>
        /// <param name="vDisposed">boolean that indicates if free managed resources</param>
        protected override void Dispose(bool pDisposable)
        {
            if (!this.vDisposed)
            {
                //call father Dispose
                base.Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
        #endregion

        //private functions

        #region isInserted
        /// <summary>
        /// Check if field on record put on insert instrucction.i f table is a Sequence calculate NextVal.
        /// </summary>
        /// <param name="pDBRecord">record to inspect</param>
        /// <param name="pIndex">index on record field list</param>
        /// <returns>return if insert or not</returns>
        private bool isInserted(ref ORMRecordBase pDBRecord, int pIndex)
        {
            var isModified = pDBRecord.FieldList.Values[pIndex].IsModified;
            //check type table
            switch (this.ORMTable.PKType)
            {
                case DBTableBase.PK_TYPE_EXPLICIT:
                    //primary key explicit, put all fields
                    return isModified;
                default:
                    //pk implicit, put fields that nor belongs to primary key
                    return !pDBRecord.BelongsToPrimaryKey(pDBRecord.FieldList.Values[pIndex]) && isModified;
            }
        }
        #endregion

        //public functions

        #region InsertInto
        /// <summary>
        /// Initializes the insert using a table
        /// </summary>
        /// <param name="pORMTable">Database table</param>
        /// <returns>SQL sentence class</returns>
        public ORMInsertQuery InsertInto(DBTableBase pORMTable)
        {
            this.Clear();

            //save table object
            this.ORMTable = pORMTable;
            // Use FullName to avoid problems in JOIN
            this.vSQLText.Append(SQL_INSERT);
            this.vSQLText.Append(" ");
            if (pORMTable.DatabaseName.Length > 0)
                this.vSQLText.Append(pORMTable.DatabaseName + "..");
            this.vSQLText.Append(pORMTable.TableName);
            this.vSQLStep = STEP_INSERT;

            return this;
        }
        #endregion

        #region Fields
        /// <summary>
        /// Terminates the insert usign a database record like insert
        /// </summary>
        /// <param name="pDBRecord">Database record to use</param>
        /// <param name="pCheckModified">indicates if check that field is modified</param>
        public virtual void Fields(ORMRecordBase pDBRecord)
        {
            var vSQLFields = new StringBuilder(" (");
            var vSQLValues = new StringBuilder(SQL_VALUES);

            try
            {
                vSQLValues.Append(" (");
                // Only insert the modified fields.
                int vIndex;
                for (vIndex = 0; vIndex < pDBRecord.FieldList.Count; vIndex++)
                {
                    // Check that is not a primary key or is a explicit primary key.
                    if (this.isInserted(ref pDBRecord, vIndex))
                    {
                        this.vParams.Add(pDBRecord.FieldList.Values[vIndex]);
                        if (!vSQLFields.ToString().Equals(" ("))
                        {
                            vSQLFields.Append(",");
                            vSQLValues.Append(",");
                        }
                        vSQLFields.Append(pDBRecord.FieldList.Values[vIndex].FieldName);
                        vSQLValues.Append("@");
                        vSQLValues.Append(pDBRecord.FieldList.Values[vIndex].FieldName);
                    }
                }
                //build fields insert
                this.AppendSQLText(vSQLFields.ToString());
                this.AppendSQLText(") ");
                this.AppendSQLText(vSQLValues.ToString());
                this.AppendSQLText(")");
            }
            finally
            {
                vSQLFields = null;
                vSQLValues = null;
            }
            this.CheckSQLSyntax(new byte[] { STEP_INSERT }, STEP_END);
        }
        #endregion
    }
}