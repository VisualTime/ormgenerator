using Robotics.DataLayer.ORM.Field;
using Robotics.DataLayer.ORM.Record;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Query
{
    using Database;
    using System;

    /// <summary>
    /// Class to implement a update SQL sentence
    /// </summary>
    public class ORMUpdateQuery : ORMQuery
    {
        //private functions

        #region FirstSet
        /// <summary>
        /// Initializes the SET usign a field
        /// </summary>
        /// <param name="pORMField">Field to initialize the SET</param>
        private void FirstSet(ORMField pORMField)
        {
            this.AppendSQLText(" ");
            this.AppendSQLText(SQL_SET);
            this.AppendSQLText(" ");
            this.AppendSQLText(pORMField.FieldName);
            this.AppendSQLText("=@");
            this.AppendSQLText(pORMField.FieldName);

            this.CheckSQLSyntax(new byte[] { STEP_UPDATE }, STEP_SET);
        }
        #endregion

        #region AppendSet
        /// <summary>
        /// Continues the SET usign a field
        /// </summary>
        /// <param name="pORMField">Field to continue the SET</param>
        private void AppendSet(ORMField pORMField)
        {
            this.AppendSQLText(", ");
            this.AppendSQLText(pORMField.FieldName);
            this.AppendSQLText("=@");
            this.AppendSQLText(pORMField.FieldName);

            this.CheckSQLSyntax(new byte[] { STEP_SET });
        }
        #endregion

        //public functions

        #region Update
        /// <summary>
        /// Initializes the update using a table
        /// </summary>
        /// <param name="pORMTable">Database table</param>
        /// <returns>Update SQL sentence class</returns>
        public ORMUpdateQuery Update(DBTableBase pORMTable)
        {
            this.Clear();

            this.vSQLText.Append(SQL_UPDATE);
            this.vSQLText.Append(" ");
            if (pORMTable.DatabaseName.Length > 0)
                this.vSQLText.Append(pORMTable.DatabaseName + "..");
            this.vSQLText.Append(pORMTable.TableName);
            this.vSQLStep = STEP_UPDATE;

            return this;
        }
        #endregion

        #region Set_
        /// <summary>
        /// Initializes the SET usign a field with an integer value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, int pValue)
        {
            this.FirstSet(pORMField);

            this.AddIntegerParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a field with an uninteger value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, uint pValue)
        {
            this.FirstSet(pORMField);

            this.AddIntegerParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a field with an small integer value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, short pValue)
        {
            this.FirstSet(pORMField);

            this.AddSmallIntegerParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a field with an big integer value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, long pValue)
        {
            this.FirstSet(pORMField);

            this.AddBigIntegerParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a field with a string value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, string pValue)
        {
            this.FirstSet(pORMField);

            this.AddStringParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a field with a Datetime value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, DateTime pValue)
        {
            this.FirstSet(pORMField);

            this.AddDateTimeParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a field with a boolean value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, bool pValue)
        {
            this.FirstSet(pORMField);

            this.AddBooleanParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a field with a boolean value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, byte pValue)
        {
            this.FirstSet(pORMField);

            this.AddByteParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a field with a decimal value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, decimal pValue)
        {
            this.FirstSet(pORMField);

            this.AddDecimalParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a field with a float value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, float pValue)
        {
            this.FirstSet(pORMField);

            this.AddFloatParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a field with a float value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMField pORMField, Guid pValue)
        {
            this.FirstSet(pORMField);

            this.AddGuidParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Initializes the SET usign a SQL text
        /// </summary>
        /// <param name="pSQLText">SQL text</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(string pSQLText)
        {
            this.AppendSQLText(" ");
            this.AppendSQLText(SQL_SET);
            this.AppendSQLText(" ");
            this.AppendSQLText(pSQLText);

            return this;
        }
        /// <summary>
        /// Creates the SET usign a Database record
        /// </summary>
        /// <param name="pORMRecordBase">Database record to use</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery Set_(ORMRecordBase pDBRecord)
        {
            int vIndex;
            var vFieldsAdded = false;

            // Only set the modified fields
            for (vIndex = 0; vIndex < pDBRecord.FieldList.Count; vIndex++)
            {
                if (pDBRecord.FieldList.Values[vIndex].IsModified)
                {
                    //set that field is added on SET
                    vFieldsAdded = true;
                    // Check that is not a primary key or is a explicit primary key
                    if (pDBRecord.PKExplicit || !pDBRecord.BelongsToPrimaryKey(pDBRecord.FieldList.Values[vIndex]))
                    {
                        this.vParams.Add(pDBRecord.FieldList.Values[vIndex]);

                        if (this.vSQLStep == STEP_UPDATE)
                        {
                            this.FirstSet(pDBRecord.FieldList.Values[vIndex]);
                        }
                        else
                        {
                            this.AppendSet(pDBRecord.FieldList.Values[vIndex]);
                        }
                    }
                }
            }

            //check if fields added on SET
            if (vFieldsAdded)
            {
                this.vSQLStep = STEP_RECORD_SET;
            }

            return this;
        }
        #endregion

        #region Append_
        /// <summary>
        /// Continues the SET usign a field with an integer value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, int pValue)
        {
            this.AppendSet(pORMField);

            this.AddIntegerParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Continues the SET usign a field with an uninteger value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, uint pValue)
        {
            this.AppendSet(pORMField);

            this.AddIntegerParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Continues the SET usign a field with an integer value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, short pValue)
        {
            this.AppendSet(pORMField);

            this.AddSmallIntegerParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Continues the SET usign a field with an integer value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, long pValue)
        {
            this.AppendSet(pORMField);

            this.AddBigIntegerParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Continues the SET usign a field with a string value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, string pValue)
        {
            this.AppendSet(pORMField);

            this.AddStringParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        ///Continues the SET usign a field with a Datetime value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, DateTime pValue)
        {
            this.AppendSet(pORMField);

            this.AddDateTimeParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Continues the SET usign a field a boolean value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, bool pValue)
        {
            this.AppendSet(pORMField);

            this.AddBooleanParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Continues the SET usign a field a byte value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, byte pValue)
        {
            this.AppendSet(pORMField);

            this.AddByteParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Continues the SET usign a field a decimal value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, decimal pValue)
        {
            this.AppendSet(pORMField);

            this.AddDecimalParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Continues the SET usign a field a float value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, float pValue)
        {
            this.AppendSet(pORMField);

            this.AddFloatParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Continues the SET usign a field a Guid value
        /// </summary>
        /// <param name="pORMField">Field to start the SET part</param>
        /// <param name="pValue">Value for the field</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(ORMField pORMField, Guid pValue)
        {
            this.AppendSet(pORMField);

            this.AddGuidParam(pORMField.FieldName, pValue);
            return this;
        }
        /// <summary>
        /// Continues the SET usign a SQL text
        /// </summary>
        /// <param name="pSQLText">SQL text</param>
        /// <returns>Update SQL sentence class</returns>
        public virtual ORMUpdateQuery _(string pSQLText)
        {
            this.AppendSQLText(", ");
            this.AppendSQLText(pSQLText);

            return this;
        }
        #endregion

        #region Where
        /// <summary>
        /// Initializes the WHERE sentence including a filter
        /// </summary>
        /// <param name="pORMFilter">Database filter to include</param>
        /// <param name="pFreeOnDestroy">Indicates if the filter is destroyed with the class</param>
        public void Where(ORMFilter pORMFilter, bool pFreeOnDestroy)
        {
            // First assign ORMFilter and then add SQL
            this.AssignORMFilter(pORMFilter, pFreeOnDestroy);
            this.AddSQLORMFilter(" " + SQL_WHERE + " ");

            this.CheckSQLSyntax(new byte[] { STEP_SET, STEP_RECORD_SET }, STEP_END);
        }
        /// <summary>
        /// Initializes the WHERE sentence including a filter
        /// </summary>
        /// <param name="pORMFilter">Database filter to include</param>
        public void Where(ORMFilter pORMFilter)
        {
            this.Where(pORMFilter, true);
        }
        #endregion
    }
}