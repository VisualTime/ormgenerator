using System;
using System.Text;
using Robotics.DataLayer.ORM.Field;

namespace Robotics.DataLayer.ORM.Query
{
    /// <summary>
    /// Class to implement a base for a filter (field, expression or value).
    /// </summary>
    public class ORMFilter : ORMQuery
    {
        #region Constructor
        /// <summary>
        ///  constructor
        /// </summary>
        public ORMFilter()
            : base()
        { }
        #endregion

        #region Destructor
        /// <summary>
        /// Override Method for free all resources
        /// Controls that execute this process only one time and stop, if is necessary, the call of destructor by Garbage Collector
        /// </summary>
        /// <param name="vDisposed">boolean that indicates if free managed resources</param>
        protected override void Dispose(bool pDisposable)
        {
            if (!this.vDisposed)
            {
                base.Dispose(true);
                //delete this object from Garbage Collector
                GC.SuppressFinalize(this);
            }
        }
        #endregion

        //private functions

        #region CheckValueStep
        /// <summary>
        ///  Check if the next step is a value, if not raises an exception
        /// </summary>
        /// <returns>The next filter expression</returns>
        public ORMFilter CheckValueStep()
        {
            // The current step can be a normal expression (with value)
            this.CheckSQLSyntax(new byte[] { STEP_FILTER_EXPR }, STEP_FILTER_VALUE);
            return this;
        }
        #endregion

        #region CheckExprStep
        /// <summary>
        ///  Check if the next step is a expression, if not raises an exception
        /// </summary>
        /// <returns>The next filter value</returns>
        public ORMFilter CheckExprStep()
        {
            // The current step can NOT be a normal expression (with value)
            if (this.vSQLStep == STEP_FILTER_EXPR)
			{
                this.RaiseInvalidSQLSyntax();
			}

            this.vSQLStep = STEP_FILTER_EXPR;
            return this;
        }
        #endregion

        #region CheckExprWithoutValueStep
        /// <summary>
        ///  Check if the next step is a expression without value, if not raises an exception
        /// </summary>
        /// <returns>The next filter expression</returns>
        public ORMFilter CheckExprWithoutValueStep()
        {
            // The current step can NOT be a normal expression (with value)
            if (this.vSQLStep == STEP_FILTER_EXPR)
			{
                this.RaiseInvalidSQLSyntax();
			}

            this.vSQLStep = STEP_FILTER_EXPR_WITHOUT_VALUE;
            return this;
        }
        #endregion

        #region Defines
        /// <summary>
        /// Function to define a SQL sentence for equal filter
        /// </summary>
        private void DefineEQ()
        {
            this.AppendSQLText("=");
        }
        /// <summary>
        /// Function to define a SQL sentence for non-equal filter
        /// </summary>
        private void DefineNotEQ()
        {
            this.AppendSQLText("<>");
        }
        /// <summary>
        /// Function to define a SQL sentence for greater filter
        /// </summary>
        private void DefineGreater()
        {
            this.AppendSQLText(">");
        }
        /// <summary>
        /// Function to define a SQL sentence for greater-equal filter
        /// </summary>
        private void DefineGreaterEQ()
        {
            this.AppendSQLText(">=");
        }
        /// <summary>
        /// Function to define a SQL sentence for less filter
        /// </summary>
        private void DefineLess()
        {
            this.AppendSQLText("<");
        }
        /// <summary>
        /// Function to define a SQL sentence for less-equal filter
        /// </summary>
        private void DefineLessEQ()
        {
            this.AppendSQLText("<=");
        }
        /// <summary>
        /// Function to define a SQL sentence for less-equal filter
        /// </summary>
        private void DefineLike()
        {
            this.AppendSQLText(" LIKE ");
        }
        /// <summary>
        /// Function to define a SQL sentence for less-equal filter
        /// </summary>
        private void DefineIn()
        {
            this.AppendSQLText(" IN ");
        }
        /// <summary>
        /// Function to define a SQL sentence for less-equal filter
        /// </summary>
        private void DefineNotIn()
        {
            this.AppendSQLText(" NOT IN ");
        }
        /// <summary>
        /// Function to define a SQL sentence for is null filter
        /// </summary>
        private void DefineIsNull()
        {
            this.AppendSQLText(" IS NULL");
        }
        /// <summary>
        /// Function to define a SQL sentence for is not null filter
        /// </summary>
        private void DefineIsNotNull()
        {
            this.AppendSQLText(" IS NOT NULL");
        }
        #endregion

        //protected functions

        #region Appends
        /// <summary>
        /// Add a new integer value to the SQL sentence
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendIntegerValue(int pValue)
        {
            StringBuilder vParamName = new StringBuilder(DBF_PARAMETER);
            try
            {
                vParamName.Append(this.vParams.Count + 1);
                this.AppendSQLText("@");
                this.AppendSQLText(vParamName.ToString());

                this.AddIntegerParam(vParamName.ToString(), pValue);
            }
            finally
            {
                vParamName = null;
            }
        }
        /// <summary>
        /// Add a small integer value to the SQL sentence
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendIntegerValue(short pValue)
        {
            var vParamName = new StringBuilder(DBF_PARAMETER);
            try
            {
                vParamName.Append(this.vParams.Count + 1);
                this.AppendSQLText("@");
                this.AppendSQLText(vParamName.ToString());

                this.AddSmallIntegerParam(vParamName.ToString(), pValue);
            }
            finally
            {
                vParamName = null;
            }
        }
		/// <summary>
        /// Add a big integer value to the SQL sentence
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendIntegerValue(long pValue)
        {
            var vParamName = new StringBuilder(DBF_PARAMETER);
            try
            {
                vParamName.Append(this.vParams.Count + 1);
                this.AppendSQLText("@");
                this.AppendSQLText(vParamName.ToString());

                this.AddBigIntegerParam(vParamName.ToString(), pValue);
            }
            finally
            {
                vParamName = null;
            }
        }
        /// <summary>
        /// Add a new unsigned integer value to the SQL sentence
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendByteValue(byte pValue)
        {
            StringBuilder vParamName = new StringBuilder(DBF_PARAMETER);
            try
            {
                vParamName.Append(this.vParams.Count + 1);
                this.AppendSQLText("@");
                this.AppendSQLText(vParamName.ToString());

                this.AddByteParam(vParamName.ToString(), pValue);
            }
            finally
            {
                vParamName = null;
            }
        }
        /// <summary>
        /// Add a new integer value to the SQL sentence
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendFloatValue(double pValue)
        {
            StringBuilder vParamName = new StringBuilder(DBF_PARAMETER);
            try
            {
                vParamName.Append(this.vParams.Count + 1);
                this.AppendSQLText("@");
                this.AppendSQLText(vParamName.ToString());

                this.AddFloatParam(vParamName.ToString(), pValue);
            }
            finally
            {
                vParamName = null;
            }
        }
		/// <summary>
        /// Add a new integer value to the SQL sentence
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendDecimalValue(decimal pValue)
        {
            StringBuilder vParamName = new StringBuilder(DBF_PARAMETER);
            try
            {
                vParamName.Append(this.vParams.Count + 1);
                this.AppendSQLText("@");
                this.AppendSQLText(vParamName.ToString());

                this.AddDecimalParam(vParamName.ToString(), pValue);
            }
            finally
            {
                vParamName = null;
            }
        }
        /// <summary>
        /// Add a new string value to the SQL sentence
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendStringValue(string pValue)
        {
            StringBuilder vParamName = new StringBuilder(DBF_PARAMETER);
            try
            {
                vParamName.Append(this.vParams.Count + 1);
                this.AppendSQLText("@");
                this.AppendSQLText(vParamName.ToString());

                this.AddStringParam(vParamName.ToString(), pValue);
            }
            finally
            {
                vParamName = null;
            }
        }
		/// <summary>
        /// Add a new string like '{0}{1}{2}' with array of objects to set
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendStringValue(string format, params object[] args)
        {
            this.AppendStringValue(string.Format(format, args));
        }

        /// <summary>
        /// Add a new date time value to the SQL sentence
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendDateTimeValue(DateTime pValue)
        {
            StringBuilder vParamName = new StringBuilder(DBF_PARAMETER);
            try
            {
                vParamName.Append(this.vParams.Count + 1);
                this.AppendSQLText("@");
                this.AppendSQLText(vParamName.ToString());

                this.AddDateTimeParam(vParamName.ToString(), pValue);
            }
            finally
            {
                vParamName = null;
            }
        }
        /// <summary>
        /// Add a new date time value to the SQL sentence
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendBooleanValue(bool pValue)
        {
            StringBuilder vParamName = new StringBuilder(DBF_PARAMETER);
            try
            {
                vParamName.Append(this.vParams.Count + 1);
                this.AppendSQLText("@");
                this.AppendSQLText(vParamName.ToString());

                this.AddBooleanParam(vParamName.ToString(), pValue);
            }
            finally
            {
                vParamName = null;
            }
        }
		 /// <summary>
        /// Add a new date time value to the SQL sentence
        /// </summary>
        /// <param name="pValue">Value for the SQL sentence</param>
        protected void AppendGuidValue(Guid pValue)
        {
            StringBuilder vParamName = new StringBuilder(DBF_PARAMETER);
            try
            {
                vParamName.Append(this.vParams.Count + 1);
                this.AppendSQLText("@");
                this.AppendSQLText(vParamName.ToString());

                this.AddGuidParam(vParamName.ToString(), pValue);
            }
            finally
            {
                vParamName = null;
            }
        }
        #endregion

        //public functions

        //expression functions, virtual functions

        #region Criteria
        /// <summary>
        /// Prepare the start of a filter   
        /// </summary>
        /// <param name="pORMField">DB Field to create the filter</param>
        /// <returns>A class to add the value for the filter</returns>
        public virtual ORMFilter Criteria(ORMField pORMField)
        {
            this.Clear();

            this.vSQLText.Append(pORMField.FullName);

            this.vSQLStep = STEP_FILTER_EXPR;
            return this;
        }
        /// <summary>
        /// Prepare the start of a filter
        /// </summary>
        /// <param name="pORMFilter">DB Filter to create the filter expression</param>
        /// <returns>A class to add the value for the filter</returns>
        public virtual ORMFilter Criteria(ORMFilter pORMFilter)
        {
            this.Clear();

            this.AppendSubQuery(pORMFilter.SQLText, pORMFilter.Params);

            this.vSQLStep = STEP_FILTER_EXPR_WITHOUT_VALUE;
            return this;
        }
        /// <summary>
        /// Prepare the start of a filter
        /// </summary>
        /// <param name="pSQLText">SQL Text for the filter</param>
        /// <returns>A class to add the value for the filter</returns>
        public virtual ORMFilter Criteria(string pSQLText)
        {
            this.Clear();

            this.AppendSQLText(pSQLText);

            this.vSQLStep = STEP_FILTER_EXPR;
            return this;
        }
        #endregion

        #region And_
        /// <summary>
        /// Prepare the AND clause of a filter expression at the last position
        /// </summary>
        /// <param name="pORMFilter">DB Field to create the filter expression</param>
        /// <returns>class to add the value for the filter expression</returns>
        public virtual ORMFilter And_(ORMField pORMField)
        {
            if (this.vSQLText.ToString().Trim().Length > 0)
            {
                this.AppendSQLText(" AND ");
                this.AppendSQLText(pORMField.FullName);

                return this.CheckExprStep();
            }
            else
			{
                // If not assigned then initialize the filter
                return this.Criteria(pORMField);
			}
        }
        /// <summary>
        /// Prepare the AND clause of a filter expression
        /// </summary>
        /// <param name="pORMFilter">DB Field to create the filter expression</param>
        /// <returns>class to add a new filter expression</returns>
        public virtual ORMFilter And_(ORMFilter pORMFilter)
        {
            if (this.vSQLText.ToString().Trim().Length > 0)
            {
                this.AppendSQLText(" AND ");
                this.AppendSubQuery(pORMFilter.SQLText, pORMFilter.Params);

                return this.CheckExprWithoutValueStep();
            }
            else
			{
                // If not assigned then initialize the filter
                return this.Criteria(pORMFilter);
			}
        }
        /// <summary>
        /// repare the AND clause of a filter expression
        /// </summary>
        /// <param name="pSQLText">SQL text for append in Filter</param>
        /// <returns>class to add a new filter expression</returns>
        public virtual ORMFilter And_(string pSQLText)
        {
            if (this.vSQLText.ToString().Trim().Length > 0)
            {
                this.AppendSQLText(" AND ");
                this.AppendSQLText(pSQLText);

                return this.CheckExprStep();
            }
            else
			{
                // If not assigned then initialize the filter
                return this.Criteria(pSQLText);
			}
        }
        #endregion

        #region Or_
        /// <summary>
        /// Prepare the OR clause of a filter expression
        /// </summary>
        /// <param name="pORMFilter">DB Field to create the filter expression</param>
        /// <returns>class to add the value for the filter expression</returns>
        public virtual ORMFilter Or_(ORMField pORMField)
        {
            if (this.vSQLText.ToString().Trim().Length > 0)
            {
                this.AppendSQLText(" OR ");
                this.AppendSQLText(pORMField.FullName);

                return this.CheckExprStep();
            }
            else
			{
                // If not assigned then initialize the filter
                return this.Criteria(pORMField);
			}
        }
        /// <summary>
        /// Prepare the OR clause of a filter expression
        /// </summary>
        /// <param name="pORMFilter">DB Field to create the filter expression</param>
        /// <returns>class to add a new filter expression</returns>
        public virtual ORMFilter Or_(ORMFilter pORMFilter)
        {
            if (this.vSQLText.ToString().Trim().Length > 0)
            {
                this.AppendSQLText(" OR ");
                this.AppendSubQuery(pORMFilter.SQLText, pORMFilter.Params);

                return this.CheckExprWithoutValueStep();
            }
            else
            {
                // If not assigned then initialize the filter
                return this.Criteria(pORMFilter);
			}
        }
        /// <summary>
        /// Prepare the OR clause of a filter expression
        /// </summary>
        /// <param name="pSqlText">SQL Text to cretae the filter expression</param>
        /// <returns>class to add a new filter expression</returns>
        public virtual ORMFilter Or_(string pSqlText)
        {
            if (this.vSQLText.ToString().Trim().Length > 0)
            {
                this.AppendSQLText(" OR ");
                this.AppendSQLText(pSqlText);

                return this.CheckExprStep();
            }
            else
            {
                // If not assigned then initialize the filter
                return this.Criteria(pSqlText);
			}
        }
        #endregion

        //value functions, virtual functions

        #region EQ
        /// <summary>
        ///  Function to set a integer value to a equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter EQ(int pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a small integer value to a equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter EQ(short pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a small integer value to a equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter EQ(long pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a small integer value to a equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter EQ(uint pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a string value to a equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter EQ(string pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendStringValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a datetime value to a equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter EQ(DateTime pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendDateTimeValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        /// Function to set a float value to a equal filter
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        public virtual ORMFilter EQ(double pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendFloatValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        /// Function to set a float value to a equal filter
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        public virtual ORMFilter EQ(decimal pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendDecimalValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a boolean value to a equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter EQ(bool pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendBooleanValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a boolean value to a equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter EQ(byte pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendByteValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a boolean value to a equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter EQ(Guid pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineEQ();
            this.AppendGuidValue(pValue);

            return this.CheckValueStep();
        }
        #endregion

        #region NotEQ
        /// <summary>
        ///  Function to set a integer value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(int pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a uninteger value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(uint pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a small integer value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(short pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a big integer value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(long pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a datetime value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(DateTime pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendDateTimeValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a float value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(double pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendFloatValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a float value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(decimal pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendDecimalValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a string value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(string pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendStringValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a boolean value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(bool pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendBooleanValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a boolean value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(byte pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendByteValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a boolean value to a non-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotEQ(Guid pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineNotEQ();
            this.AppendGuidValue(pValue);

            return this.CheckValueStep();
        }
        #endregion

        #region Greater
        /// <summary>
        ///  Function to set a integer value to a greater filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(int pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a uninteger value to a greater filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(uint pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a small integer value to a greater filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(short pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a big integer value to a greater filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(long pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a datetime value to a greater filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(DateTime pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendDateTimeValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a float value to a greater filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(double pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendFloatValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a float value to a greater filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(decimal pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendDecimalValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a string value to a greater filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(string pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendStringValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a boolean value to a greater filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(bool pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendBooleanValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        /// Function to set other field to a greater filter
        /// </summary>
        /// <param name="pORMField">other field to add</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(byte pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendByteValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        /// Function to set other field to a greater filter
        /// </summary>
        /// <param name="pORMField">other field to add</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Greater(Guid pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreater();
            this.AppendGuidValue(pValue);

            return this.CheckValueStep();
        }
        #endregion

        #region GreaterEQ
        /// <summary>
        ///  Function to set a integer value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(int pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a uninteger value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(uint pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a short value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(short pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a long value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(long pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a datetime value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(DateTime pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendDateTimeValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a float value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(double pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendFloatValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a float value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(decimal pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendDecimalValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a string value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(string pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendStringValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a boolean value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(bool pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendBooleanValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a byte value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(byte pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendByteValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a byte value to a greater-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter GreaterEQ(Guid pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineGreaterEQ();
            this.AppendGuidValue(pValue);

            return this.CheckValueStep();
        }
        #endregion

        #region Less
        /// <summary>
        ///  Function to set a integer value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(int pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a uninteger value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(uint pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a small integer value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(short pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a big integer value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(long pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a datetime value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(DateTime pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendDateTimeValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a float value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(double pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendFloatValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a float value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(decimal pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendDecimalValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a string value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(string pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendStringValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a boolean value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(bool pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendBooleanValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a byte value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(byte pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendByteValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a byte value to a less filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Less(Guid pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLess();
            this.AppendGuidValue(pValue);

            return this.CheckValueStep();
        }
        #endregion

        #region LessEQ
        /// <summary>
        ///  Function to set a integer value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(int pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a uninteger value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(uint pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a small integer value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(short pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a big integer value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(long pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendIntegerValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a float value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(DateTime pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendDateTimeValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a double value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(double pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendFloatValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a decimal value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(decimal pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendDecimalValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a string value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(string pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendStringValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a boolean value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(bool pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendBooleanValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a byte value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(byte pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendByteValue(pValue);

            return this.CheckValueStep();
        }
        /// <summary>
        ///  Function to set a guid value to a less-equal filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter LessEQ(Guid pValue)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineLessEQ();
            this.AppendGuidValue(pValue);

            return this.CheckValueStep();
        }
        #endregion

        #region Like
        /// <summary>
        ///  Function to set a string value to a like filter (starts with)
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter StartsWith(string text)
        {
            this.DefineLike();
            this.AppendStringValue("{0}%", text);

            return this.CheckValueStep();
        }
		/// <summary>
        ///  Function to set a string value to a like filter (contains)
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter Contains(string text)
        {
            this.DefineLike();
            this.AppendStringValue("%{0}%", text);

            return this.CheckValueStep();
        }
		/// <summary>
        ///  Function to set a string value to a like filter (ends with)
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter EndsWith(string text)
        {
            this.DefineLike();
            this.AppendStringValue("%{0}", text);

            return this.CheckValueStep();
        }
        #endregion

        #region In
        /// <summary>
        ///  Function to set a string value to a like filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter In(string pValue)
        {
            return this.In(pValue, true);
        }
        /// <summary>
        ///  Function to set a string value to a like filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter In(string pValue, bool pAddStringParam)
        {
            // The order is important because AddParam AppendSQLText
            this.DefineIn();

            this.AppendSQLText("(");

            if (pAddStringParam)
			{
                this.AppendStringValue(pValue);
			}
            else
			{
                this.AppendSQLText(pValue);
			}

            this.AppendSQLText(")");

            return this.CheckValueStep();
        }
        #endregion

        #region NotIn
        /// <summary>
        ///  Function to set a string value to a like filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotIn(string pValue)
        {
            return this.NotIn(pValue, true);
        }
        /// <summary>
        ///  Function to set a string value to a like filter
        /// </summary>
        /// <param name="pValue">Value for the filter</param>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter NotIn(string pValue, bool pAddStringParam)
        {
            // The order is important because AddParam AppendSQLText

            
            this.DefineNotIn();

            this.AppendSQLText("(");

            if (pAddStringParam)
			{
                this.AppendStringValue(pValue);
			}
            else
			{
                this.AppendSQLText(pValue);
			}

            this.AppendSQLText(")");

            return this.CheckValueStep();
        }
        #endregion

        #region IsNull
        /// <summary>
        ///  Function to set a null value to a filter
        /// </summary>
        /// <returns>The next filter expression</returns>
        public virtual ORMFilter IsNull()
        {
            this.DefineIsNull();

            return this.CheckValueStep();
        }
        #endregion

        #region IsNotNull
        /// <summary>
        ///  Function to set a not null value to a filter
        /// </summary>
        /// <returns>The next filter expression</returns>
        public ORMFilter IsNotNull()
        {
            this.DefineIsNotNull();

            return this.CheckValueStep();
        }
        #endregion
    }
}