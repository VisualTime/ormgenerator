using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Robotics.DataLayer.ORM.Database;
using Robotics.DataLayer.ORM.Field;
using Robotics.DataLayer.ORM.Record;

namespace Robotics.DataLayer.ORM.Query
{
    /// <summary>
    /// Class to implement a select SQL sentence
    /// </summary>
    public class ORMSelectQuery : ORMQuery
    {
        #region Variables
        private DBConnection vDBConnection;
        private DbDataReader vDBReader;
        #endregion

        #region Constructor
        /// <summary>
        ///  Constructor
        /// </summary>
        public ORMSelectQuery()
            : base()
        {
            this.vDBConnection = null;
        }
        #endregion

        #region Destructor
        /// <summary>
        /// Override Method for free all resources
        /// Controls that execute this process only one time and stop, if is necessary, the call of destructor by Garbage Collector
        /// </summary>
        /// <param name="vDisposed">boolean that indicates if free managed resources</param>
        protected override void Dispose(bool pDisposable)
        {
            if (!this.vDisposed)
            {
                if (pDisposable)
                {
                    if (!ReferenceEquals(this.vDBReader, null) && !this.vDBReader.IsClosed)
                        this.vDBReader.Close();
                }
                //call father Dispose
                base.Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
        #endregion

        //private functions

        #region AddFieldListSQL
        /// <summary>
        /// Add a field list to a SQL text (separated with commas). Only for SELECT and ORDER BY
        /// </summary>
        /// <param name="pORMFields">List of database fields</param>
        private void AddFieldListSQL(ORMField[] pORMFields)
        {
            int vIndex;

            for (vIndex = 0; vIndex < pORMFields.Length; vIndex++)
            {
                switch (this.vSQLStep)
                {
                    case STEP_NONE:
                        this.Select(pORMFields[vIndex]);
                        break;
                    case STEP_SELECT:
                    case STEP_ORDER_BY:
                        this._(pORMFields[vIndex]);
                        break;
                    case STEP_FROM:
                    case STEP_WHERE:
                        this.OrderBy(pORMFields[vIndex]);
                        break;
                    default:
                        this._(pORMFields[vIndex]); // To show the text in the exception
                        break;
                }
            }
        }

        #endregion

        //protected functions

        #region SetOrder
        /// <summary>
        /// Set the order for a field without order type
        /// </summary>
        /// <param name="pORMField">field to set the order</param>
        protected virtual void SetOrder(ORMField pORMField)
        {
            var vValidSteps = new HashSet<byte>(new byte[] { STEP_FROM, STEP_WHERE });
            //Check if it is the first order or not
            if (vValidSteps.Contains(this.vSQLStep))
            {
                //Append Order By
                this.AppendSQLText(" ");
                this.AppendSQLText(SQL_ORDER_BY);
                this.AppendSQLText(" ");
            }
            else
                this.AppendSQLText(", ");
            // Use FullName to avoid problems in JOIN (check case sensitive).
            this.AppendSQLText(pORMField.FullName);
        }
        /// <summary>
        /// Set the order for a field with order type
        /// </summary>
        /// <param name="pORMField">field to set the order</param>
        /// <param name="pAscendent">True if ascedent order</param>
        protected virtual void SetOrder(ORMField pORMField, bool pAscendent)
        {
            this.SetOrder(pORMField);

            this.AppendSQLText(" ");
            // Check the type of the order
            if (pAscendent)
                this.AppendSQLText(SQL_ASCENDENT);
            else
                this.AppendSQLText(SQL_DESCENDENT);
        }
        #endregion

        //public functions

        #region Select
        /// <summary>
        /// Initializes the select using a field
        /// </summary>
        /// <param name="pORMField">Field to add into SELECT part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery Select(ORMField pORMField)
        {
            this.Clear();

            // Use FullName to avoid problems in JOIN
            this.vSQLText.Append(SQL_SELECT);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(pORMField.FullName);
            this.vSQLStep = STEP_SELECT;

            return this;
        }
        /// <summary>
        /// Initializes the select using a table
        /// </summary>
        /// <param name="pORMTableBase">Table to add into SELECT part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery Select(DBTableBase pORMTable)
        {
            this.Clear();

            this.vSQLText.Append(SQL_SELECT);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(pORMTable.TableName);
            this.vSQLText.Append(".*");
            this.vSQLStep = STEP_SELECT;

            return this;
        }
        /// <summary>
        /// Add a sql text in the select query
        /// </summary>
        /// <param name="pSqlText">SQL text to add in the SELECT part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery Select(string pSqlText)
        {
            this.Clear();

            this.vSQLText.Append(SQL_SELECT);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(pSqlText);
            this.vSQLStep = STEP_SELECT;

            return this;
        }
        /// <summary>
        /// Initializes the select using an array of fields
        /// </summary>
        /// <param name="pORMFields">Array of fields</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery Select(ORMField[] pORMFields)
        {
            this.Clear();

            this.AddFieldListSQL(pORMFields);
            return this;
        }
        #endregion

        #region SelectTop
        /// <summary>
        /// Initializes the select using a field
        /// </summary>
        /// <param name="pORMField">Field to add into SELECT part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery SelectTop(int pNumRowsTop, ORMField pORMField)
        {
            this.Clear();

            // Use FullName to avoid problems in JOIN
            this.vSQLText.Append(SQL_SELECT);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(SQL_TOP);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(pNumRowsTop);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(pORMField.FullName);
            this.vSQLStep = STEP_SELECT;

            return this;
        }
        /// <summary>
        /// Initializes the select top using a table
        /// </summary>
        /// <param name="pORMTableBase">Table to add into SELECT part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery SelectTop(int pNumRowsTop, DBTableBase pORMTable)
        {
            this.Clear();

            this.vSQLText.Append(SQL_SELECT);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(SQL_TOP);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(pNumRowsTop);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(pORMTable.TableName);
            this.vSQLText.Append(".*");
            this.vSQLStep = STEP_SELECT;

            return this;
        }
        /// <summary>
        /// Add a sql text in the select query
        /// </summary>
        /// <param name="pSqlText">SQL text to add in the SELECT part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery SelectTop(int pNumRowsTop, string pSqlText)
        {
            this.Clear();

            this.vSQLText.Append(SQL_SELECT);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(SQL_TOP);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(pNumRowsTop);
            this.vSQLText.Append(" ");
            this.vSQLText.Append(pSqlText);
            this.vSQLStep = STEP_SELECT;

            return this;
        }
        /// <summary>
        /// Initializes the select using an array of fields
        /// </summary>
        /// <param name="pORMFields">Array of fields</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery SelectTop(int pNumRowsTop, ORMField[] pORMFields)
        {
            var pResult = new List<ORMField>(pORMFields);
            this.Clear();

            //add first field
            this.SelectTop(pNumRowsTop, pResult[0]);

            //add other fields
            pResult.RemoveAt(0);
            this.AddFieldListSQL(pResult.ToArray());
            return this;
        }
        #endregion

        #region From
        /// <summary>
        /// Initializes the FROM sentence including another select query
        /// </summary>
        /// <param name="pDBQuery">Query to add</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery From(ORMSelectQuery pDBQuery, string pAlias)
        {
            this.AppendSQLText(" ");
            this.AppendSQLText(SQL_FROM);
            this.AppendSQLText("(");
            this.AppendSQLText(pDBQuery.SQLText);
            this.AppendSQLText(") AS ");
            this.AppendSQLText(pAlias);
            //add params
            this.vParams.AddRange(pDBQuery.vParams);

            this.CheckSQLSyntax(new byte[] { STEP_SELECT }, STEP_FROM);
            return this;
        }
        /// <summary>
        /// Initializes the FROM sentence including a table
        /// </summary>
        /// <param name="pORMTable">Table to add into FROM part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery From(DBTableBase pORMTable)
        {
            this.AppendSQLText(" ");
            this.AppendSQLText(SQL_FROM);
            this.AppendSQLText(" ");
            this.AppendSQLText(pORMTable.TableName);
            //check alias
            if (pORMTable.Alias.Length > 0)
            {
                this.AppendSQLText(SQL_ALIAS + pORMTable.Alias);
            }

            this.CheckSQLSyntax(new byte[] { STEP_SELECT }, STEP_FROM);
            return this;
        }
        /// <summary>
        ///  Initializes the FROM sentence including sql text
        /// </summary>
        /// <param name="pDBSelect">select query to include</param>
        /// <returns>SQL sentence class</returns>
        public virtual ORMSelectQuery From(string pDBSelect)
        {
            this.AppendSQLText(" ");
            this.AppendSQLText(SQL_FROM);
            this.AppendSQLText(" ");
            this.AppendSQLText(pDBSelect);

            this.CheckSQLSyntax(new byte[] { STEP_SELECT }, STEP_FROM);
            return this;
        }
        #endregion

        #region Join
        /// <summary>
        /// Append left join in FROM sentense
        /// </summary>
        /// <param name="TypeJoin">type join: inner o left</param>
        /// <param name="pORMTable">Table to add into FROM for left join</param>
        /// <param name="pAlias">alias for use on join</param>
        /// <param name="pORMJoinFieldsOrigin">Fields to join in first table</param>
        /// <param name="pORMJoinFieldsDestiny">Fields to join in second table</param>
        /// <returns>Select SQL sentence class</returns>
        public ORMSelectQuery Join(string TypeJoin, DBTableBase pORMTable, ORMField[] pORMJoinFieldsOrigin, ORMField[] pORMJoinFieldsDestiny)
        {
            this.AppendSQLText(" ");
            this.AppendSQLText(TypeJoin);
            this.AppendSQLText(" ");
            this.AppendSQLText(pORMTable.TableName);

            //check alias
            if (!string.IsNullOrEmpty(pORMTable.Alias))
            {
                this.AppendSQLText(SQL_ALIAS + pORMTable.Alias);
            }

            this.AppendSQLText(" ON ");

            for (var vIdx = 0; vIdx < pORMJoinFieldsOrigin.Length; vIdx++)
            {
                if (vIdx > 0) // Append a new field to Join
                {
                    this.AppendSQLText(" AND ");
                }

                AppendSQLText(string.Format("{0}={1}", pORMJoinFieldsOrigin[vIdx].FullName, string.IsNullOrEmpty(pORMTable.Alias) ? pORMJoinFieldsDestiny[vIdx].FullName : string.Format("{0}.{1}", pORMTable.Alias, pORMJoinFieldsDestiny[vIdx].FieldName)));
            }

            this.CheckSQLSyntax(new byte[] { STEP_FROM });
            return this;
        }

        /// <summary>
        /// Append left join in FROM sentense
        /// </summary>
        /// <param name="pORMTable">Table to add into FROM for left join</param>
        /// <param name="pAlias">alias for use on join</param>
        /// <param name="pORMJoinFieldsOrigin">Fields to join in first table</param>
        /// <param name="pORMJoinFieldsDestiny">Fields to join in second table</param>
        /// <returns>Select SQL sentence class</returns>
        public ORMSelectQuery LJoin(DBTableBase pORMTable, ORMField[] pORMJoinFieldsOrigin, ORMField[] pORMJoinFieldsDestiny)
        {
            return Join(SQL_LJOIN, pORMTable, pORMJoinFieldsOrigin, pORMJoinFieldsDestiny);
        }
        /// <summary>
        /// Append inner join in FROM sentense
        /// </summary>
        /// <param name="pORMTable">Table to add into FROM for inner join</param>
        /// <param name="pORMJoinFieldsOrigin">Fields to join in first table</param>
        /// <param name="pORMJoinFieldsDestiny">Fields to join in second table</param>
        /// <returns>Select SQL sentence class</returns>
        public ORMSelectQuery IJoin(DBTableBase pORMTable, ORMField[] pORMJoinFieldsOrigin, ORMField[] pORMJoinFieldsDestiny)
        {
            return Join(SQL_IJOIN, pORMTable, pORMJoinFieldsOrigin, pORMJoinFieldsDestiny);
        }
        #endregion

            #region Where
            /// <summary>
            /// Initializes the WHERE sentence including a filter
            /// </summary>
            /// <param name="pORMFilter">Database filter to include</param>
            /// <param name="pFreeOnDestroy">Indicates if the filter is destroyed with the class</param>
            /// <returns>Select SQL sentence class</returns>
        public ORMSelectQuery Where(ORMFilter pORMFilter, bool pFreeOnDestroy)
        {
            var vSQLText = new StringBuilder(" ");
            try
            {
                // First assign ORMFilter and then add SQL.
                this.AssignORMFilter(pORMFilter, pFreeOnDestroy);
                vSQLText.Append(" ");
                vSQLText.Append(SQL_WHERE);
                vSQLText.Append(" ");
                this.AddSQLORMFilter(vSQLText.ToString());
            }
            finally
            {
                vSQLText = null;
            }

            this.CheckSQLSyntax(new byte[] { STEP_FROM }, STEP_WHERE);
            return this;
        }
        /// <summary>
        /// Initializes the WHERE sentence including a filter
        /// </summary>
        /// <param name="pORMFilter">Database filter to include</param>
        /// <returns>Select SQL sentence class</returns>
        public ORMSelectQuery Where(ORMFilter pORMFilter)
        {
            return this.Where(pORMFilter, true);
        }
        #endregion

        #region OrderBy
        /// <summary>
        /// Initializes the ORDER BY sentence including a field without order
        /// </summary>
        /// <param name="pORMField">Field to add into ORDER BY part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery OrderBy(ORMField pORMField)
        {
            this.SetOrder(pORMField);

            this.CheckSQLSyntax(new byte[] { STEP_FROM, STEP_WHERE, STEP_GROUP_BY }, STEP_ORDER_BY);
            return this;
        }
        /// <summary>
        /// Initializes the ORDER BY sentence including a field with order
        /// </summary>
        /// <param name="pORMField">Field to add into ORDER BY part</param>
        /// <param name="pAscendent">Indicates if the order is ascendent or descendent</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery OrderBy(ORMField pORMField, bool pAscendent)
        {
            this.SetOrder(pORMField, pAscendent);

            this.CheckSQLSyntax(new byte[] { STEP_FROM, STEP_WHERE, STEP_GROUP_BY }, STEP_ORDER_BY);
            return this;
        }
        /// <summary>
        /// Initializes the ORDER BY sentence including a field list without order
        /// </summary>
        /// <param name="pORMFields">Array of fields</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery OrderBy(ORMField[] pORMFields)
        {
            this.AddFieldListSQL(pORMFields);
            return this;
        }

        /// <summary>
        /// Initializes the ORDER BY sentence including a field list with order
        /// </summary>
        /// <param name="pORMFields">Array of fields</param>
        /// <param name="pAscendent">Indicates if the order is ascendent or descendent</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery OrderBy(ORMField[] pORMFields, bool pAscendent)
        {
            if (!ReferenceEquals(pORMFields, null) && pORMFields.Length > 0)
            {
                this.AddFieldListSQL(pORMFields);

                this.AppendSQLText(" ");
                this.AppendSQLText(pAscendent ? SQL_ASCENDENT : SQL_DESCENDENT);
            }
            return this;
        }
        /// <summary>
        /// Initializes the ORDER BY sentence including a field list with order
        /// </summary>
        /// <param name="pORMFields">Array of fields</param>
        /// <param name="pDescendent">Indicates if the order is ascendent or descendent</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery OrderBy(ORMField[] pORMFields, bool[] pDescendent)
        {
            if (!ReferenceEquals(pORMFields, null) && !ReferenceEquals(pDescendent, null) && pORMFields.Length > 0 && pDescendent.Length > 0)
            {
                //add first order
                this.SetOrder(pORMFields[0], !pDescendent[0]);
                //change sqlstep to order by
                this.CheckSQLSyntax(new byte[] { STEP_FROM, STEP_WHERE, STEP_GROUP_BY }, STEP_ORDER_BY);
                //add others order by fields
                for (var vIndex = 1; vIndex < pORMFields.Length; vIndex++)
                {
                    this.SetOrder(pORMFields[vIndex], !pDescendent[vIndex]);
                }
            }
            return this;
        }
        /// <summary>
        /// nitializes the ORDER BY sentence including a field list with order
        /// </summary>
        /// <param name="pSQLText">SQL Text to add in ORDER BY sentence</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery OrderBy(string pSQLText)
        {
            //Use FullName to avoid problems in JOIN
            this.AppendSQLText(" ");
            this.AppendSQLText(SQL_ORDER_BY);
            this.AppendSQLText(" ");
            this.AppendSQLText(pSQLText);

            this.CheckSQLSyntax(new byte[] { STEP_FROM, STEP_WHERE, STEP_GROUP_BY }, STEP_ORDER_BY);
            return this;
        }
        #endregion

        #region Group by

        /// <summary>
        /// Adds 'GROUP BY' clause to the query.
        /// </summary>
        /// <param name="pField">
        /// The field to group by with.
        /// </param>
        /// <returns>
        /// The query.
        /// </returns>
        public ORMSelectQuery GroupBy(ORMField pField)
        {
            return this.GroupBy(new[] { pField });
        }

        /// <summary>
        /// Adds 'GROUP BY' clause to the query.
        /// </summary>
        /// <param name="pFields">
        /// A set of fields to group by with.
        /// </param>
        /// <returns>
        /// The query.
        /// </returns>
        public ORMSelectQuery GroupBy(IEnumerable<ORMField> pFields)
        {
            if (pFields.Count() == 0)
            {
                throw new ArgumentException
                    (
                    "No fields given",
                    "pFields"
                    );
            }

            this.CheckSQLSyntax(new[] { STEP_FROM, STEP_WHERE }, STEP_GROUP_BY);
            this.AppendSQLBlankSpace();
            this.AppendSQLText(SQL_GROUP_BY);
            this.AppendSQLBlankSpace();

            this.AppendSQLText
                (
                    string.Join(",", pFields.Select(field => string.Format("{0}", field.FieldName)).ToArray())
                );

            return this;
        }

        #endregion Group by

        #region Appends_
        /// <summary>
        /// Add a new field in the current part of the SQL sentence
        /// </summary>
        /// <param name="pORMField">Field to add into the current part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery _(ORMField pORMField)
        {
            this.AppendSQLText(", ");
            this.AppendSQLText(pORMField.FullName);
            this.CheckSQLSyntax(new byte[] { STEP_SELECT, STEP_ORDER_BY });
            return this;
        }
        /// <summary>
        /// Add a list of fields in the current part of the SQL sentence
        /// </summary>
        /// <param name="pORMFields">List of fields</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery _(ORMField[] pORMFields)
        {
            this.CheckSQLSyntax(new byte[] { STEP_SELECT, STEP_ORDER_BY });

            this.AddFieldListSQL(pORMFields);
            return this;
        }
        /// <summary>
        /// Add a new field in the current part of the SQL sentence
        /// </summary>
        /// <param name="pORMTable">Table to add into the current part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery _(DBTableBase pORMTable)
        {
            this.AppendSQLText(", ");
            switch (this.vSQLStep)
            {
                case STEP_SELECT: // Add the field in the select
                    this.AppendSQLText(pORMTable.TableName);
                    this.AppendSQLText(".*");
                    break;
                case STEP_FROM: // Add the table in the from
                    this.AppendSQLText(pORMTable.TableName);
                    break;
                default: // To show the text in the exception
                    this.AppendSQLText(pORMTable.TableName);
                    break;
            }

            this.CheckSQLSyntax(new byte[] { STEP_SELECT, STEP_FROM });
            return this;
        }
        /// <summary>
        /// Add a sql text in the current part of the SQL sentence
        /// </summary>
        /// <param name="pSQLText">SQL text to add in the current part</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery _(string pSQLText)
        {
            this.AppendSQLText(", ");
            this.AppendSQLText(pSQLText);

            return this;
        }
        /// <summary>
        /// Add a new field with order in the current part of the SQL sentence
        /// </summary>
        /// <param name="pORMField">Field to add into the current part</param>
        /// <param name="pAscendent">Indicates if the order is ascendent or descendent</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery _(ORMField pORMField, bool pAscendent)
        {
            this.SetOrder(pORMField, pAscendent);

            this.CheckSQLSyntax(new byte[] { STEP_ORDER_BY });
            return this;
        }
        /// <summary>
        /// Add a list of fields in the current part of the SQL sentence (Order by)
        /// </summary>
        /// <param name="pORMField">List of fields</param>
        /// <param name="pAscendent">Indicates if the order is ascendent or descendent</param>
        /// <returns>Select SQL sentence class</returns>
        public virtual ORMSelectQuery _(ORMField[] pORMFields, bool pAscendent)
        {
            this.CheckSQLSyntax(new byte[] { STEP_ORDER_BY });

            if (!ReferenceEquals(pORMFields, null) && pORMFields.Length > 0)
            {
                this.AddFieldListSQL(pORMFields);

                this.AppendSQLText(" ");
                this.AppendSQLText(pAscendent ? SQL_ASCENDENT : SQL_DESCENDENT);
            }

            return this;
        }
        #endregion

        #region Open
        /// <summary>
        /// Opens the query to select the data
        /// </summary>
        /// <param name="pDBConnection">Database connection to execute the query</param>
        public void Open(DBConnection pDBConnection)
        {
            DbCommand vSQLQuery;
            try
            {
                this.vDBConnection = pDBConnection;
                vSQLQuery = pDBConnection.DBQuery;

                vSQLQuery.Parameters.Clear();
                vSQLQuery.CommandText = this.vSQLText.ToString();

                this.SetParamsTo(pDBConnection); //required for union

                if (pDBConnection.InTransaction)
                    vSQLQuery.Transaction = pDBConnection.BDTransaction;

                this.vDBReader = vSQLQuery.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Opens the query to select the data and fill it in dataset
        /// </summary>
        /// <param name="pDDBataset">Dataset to fill the select data</param>
        /// <param name="pDBConnection">Database connection to execute the query</param>
        public void Open(out DataSet pDBDataset, DBConnection pDBConnection)
        {
            pDBDataset = new DataSet();

            try
            {
                this.vDBConnection = pDBConnection;
                var vSQLQuery = pDBConnection.DBQuery;

                vSQLQuery.Parameters.Clear();
                vSQLQuery.CommandText = this.vSQLText.ToString();

                this.SetParamsTo(pDBConnection); //required for union

                using (var dataReader = vSQLQuery.ExecuteReader())
                {
                    while (!dataReader.IsClosed)
                    {
                        var dataTable = new DataTable();
                        dataTable.Load(dataReader);
                        pDBDataset.Tables.Add(dataTable);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Opens the query to select the data and fill it in dataset. Specify the name of datatable
        /// </summary>
        /// <param name="pDDBataset">Dataset to fill the select data</param>
        /// <param name="pDataTableName">Name of datatable</param>
        /// <param name="pDBConnection">Database connection to execute the query</param>
        public void Open(out DataSet pDBDataset, string pTableName, DBConnection pDBConnection)
        {
            pDBDataset = new DataSet();
            try
            {
                this.vDBConnection = pDBConnection;
                var vSQLQuery = pDBConnection.DBQuery;

                vSQLQuery.Parameters.Clear();
                vSQLQuery.CommandText = this.vSQLText.ToString();

                this.SetParamsTo(pDBConnection); //required for union

                using (var dataReader = vSQLQuery.ExecuteReader())
                {
                    while (!dataReader.IsClosed)
                    {
                        var dataTable = new DataTable();
                        dataTable.Load(dataReader);
                        pDBDataset.Tables.Add(dataTable);
                    }
                }

                pDBDataset.Tables[0].TableName = pTableName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Close
        /// <summary>
        /// Close DataReader associated to SelectQuery
        /// </summary>
        public void Close()
        {
            if (!this.vDBReader.IsClosed)
                this.vDBReader.Close();
        }
        #endregion

        #region Next
        /// <summary>
        /// Set the current position to the next record
        /// </summary>
        public bool Next()
        {
            if (!ReferenceEquals(this.vDBConnection, null))
                return this.vDBReader.Read();
            else
            {
                throw new Exception(ERR_DBCONNECTION_NOT_ASSIGNED);
            }
        }
        #endregion

        #region GetFieldValue
        /// <summary>
        /// Get the field value. Only if the query is assigned
        /// </summary>
        /// <param name="pORMField">Field to retrieve the field value</param>
        public void GetFieldValue(ORMField pORMField)
        {
            if (!ReferenceEquals(this.vDBConnection, null))
            {
                //alias is fieldname by default
                if (this.HasColumn(this.vDBReader, pORMField.FieldName))
                {
                    pORMField.GetValueFrom(this.vDBReader);
                }
            }
            else
                throw new Exception(ERR_DBCONNECTION_NOT_ASSIGNED);
        }

        /// <summary>
        /// HasColumn
        /// </summary>
        /// <param name="dr">
        /// The dr.
        /// </param>
        /// <param name="columnName">
        /// The column name.
        /// </param>
        /// <returns>
        /// return if record has the column
        /// </returns>
        private bool HasColumn(IDataRecord dr, string columnName)
        {
            for (var i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region GetRecordValues
        /// <summary>
        /// Get the values for all fields in the record. Only if the query is assigned
        /// </summary>
        /// <param name="pDBRecord">Record to get all values</param>
        public void GetRecordValues(ORMRecordBase pDBRecord)
        {
            if (!ReferenceEquals(this.vDBConnection, null))
            {
                int vIndex;
                for (vIndex = 0; vIndex < pDBRecord.FieldList.Count; vIndex++)
                    this.GetFieldValue(pDBRecord.FieldList.Values[vIndex]);
            }
            else
                throw new Exception(ERR_DBCONNECTION_NOT_ASSIGNED);
        }
        #endregion
    }
}