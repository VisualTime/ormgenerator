using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using Robotics.DataLayer.ORM.Database;
using Robotics.DataLayer.ORM.Field;

namespace Robotics.DataLayer.ORM.Query
{
    /// <summary>
    /// Class to implement a basic query. All querys can have a filter
    /// </summary>
    public class ORMQuery : ORMQueryBase
    {
        #region Variables
        private bool vFreeORMFilter;
        private ORMFilter vORMFilter; // Filter for the query
        #endregion

        #region Properties
        /// <summary>
        ///  Filter for the query
        /// </summary>
        public ORMFilter ORMFilter => this.vORMFilter;

        #endregion

        #region Constructor
        /// <summary>
        ///  Constructor
        /// </summary>
        public ORMQuery()
            : base()
        {
        }
        #endregion

        #region Destructor
        /// <summary>
        /// Override Method for free all resources
        /// Controls that execute this process only one time and stop, if is necessary, the call of destructor by Garbage Collector
        /// </summary>
        /// <param name="vDisposed">boolean that indicates if free managed resources</param>
        protected override void Dispose(bool pDisposable)
        {
            if (!this.vDisposed)
            {
                if (pDisposable)
                {
                    this.Clear();
                }
                //call father Dispose
                base.Dispose(true);
            }
        }
        #endregion

        //protected functions

        #region AssignORMFilter
        /// <summary>
        /// Assign a database filter to the query
        /// </summary>
        /// <param name="pORMFilter">Database filter to assign</param>
        /// <param name="pFreeORMFilter">When the object is destroyed the filter too?</param>
        protected void AssignORMFilter(ORMFilter pORMFilter, bool pFreeORMFilter)
        {
            this.vORMFilter = pORMFilter;
            this.vFreeORMFilter = pFreeORMFilter;
        }
        #endregion

        #region AddSQLORMFilter
        /// <summary>
        /// Append the database filter SQL (with a prefix) if is not empty
        /// </summary>
        /// <param name="pSQLPrefix">SQL prefix for the database filter</param>
        protected void AddSQLORMFilter(string pSQLPrefix)
        {
            if (!ReferenceEquals(this.vORMFilter, null) && this.vORMFilter.SQLText.Trim().Length > 0)
            {
                this.AppendSQLText(pSQLPrefix);
                this.AppendSubQuery(this.vORMFilter.SQLText, this.vORMFilter.Params);
            }
        }
        #endregion

        #region AppendSubQuery
        /// <summary>
        /// Add a sub filter in the filter
        /// </summary>
        /// <param name="pORMFilter">sub filter to add</param>
        protected void AppendSubQuery(string pSQLText, List<ORMField> pParams)
        {
            StringBuilder vParamName;
            var vSubSQLText = new StringBuilder(pSQLText + " ");

            try
            {
                if (!ReferenceEquals(pParams, null))
                {
                    var vCount = pParams.Count;

                    ORMField vORMField;
                    int vIndex;
                    for (vIndex = vCount; vIndex > 0; vIndex--)
                    {
                        vORMField = pParams[vIndex - 1];

                        // Replace the name of the param.
                        vParamName = new StringBuilder(DBF_PARAMETER);
                        vParamName.Append(this.vParams.Count + vIndex);
                        vSubSQLText.Replace(vORMField.FieldName + " ", vParamName + " ");
                    }

                    // Add the parameters in the same order
                    for (vIndex = 0; vIndex < vCount; vIndex++)
                    {
                        vORMField = pParams[vIndex];

                        vParamName = new StringBuilder(DBF_PARAMETER);
                        vParamName.Append(this.vParams.Count + 1);
                        AddParam(vORMField, vParamName.ToString());
                    }

                }
                this.AppendSQLText(" (");
                this.AppendSQLText(vSubSQLText.ToString());
                this.AppendSQLText(")");
            }
            finally
            {
                vSubSQLText = null;
                vParamName = null;
            }
        }

        /// <summary>
        /// Add PAram on Query
        /// </summary>
        /// <param name="vORMField"></param>
        /// <param name="vParamName"></param>
        private void AddParam(ORMField vORMField, string vParamName)
        {
            if (vORMField is ORMIntegerField)
            {
                this.AddIntegerParam(vParamName, (vORMField as ORMIntegerField).Value);
            }
            if (vORMField is ORMSmallIntegerField)
            {
                this.AddSmallIntegerParam(vParamName, (vORMField as ORMSmallIntegerField).Value);
            }
            if (vORMField is ORMBigIntegerField)
            {
                this.AddBigIntegerParam(vParamName, (vORMField as ORMBigIntegerField).Value);
            }
            else if (vORMField is ORMStringField)
            {
                this.AddStringParam(vParamName, (vORMField as ORMStringField).Value);
            }
            else if (vORMField is ORMDateTimeField)
            {
                this.AddDateTimeParam(vParamName, (vORMField as ORMDateTimeField).Value);
            }
            else if (vORMField is ORMBooleanField)
            {
                this.AddBooleanParam(vParamName, (vORMField as ORMBooleanField).AsBoolean);
            }
            else if (vORMField is ORMFloatField)
            {
                this.AddFloatParam(vParamName, (vORMField as ORMFloatField).Value);
            }
            else if (vORMField is ORMDecimalField)
            {
                this.AddDecimalParam(vParamName, (vORMField as ORMDecimalField).Value);
            }
            else if (vORMField is ORMByteField)
            {
                this.AddByteParam(vParamName, (vORMField as ORMByteField).Value);
            }
            else if (vORMField is ORMGuidField)
            {
                this.AddGuidParam(vParamName, (vORMField as ORMGuidField).Value);
            }
        }

        #endregion

        //public functions

        #region Clear
        /// <summary>
        /// Clear the SQL sentence
        /// </summary>
        public void Clear()
        {
            this.vSQLStep = STEP_NONE;
            this.vSQLText = new StringBuilder("");

            if (!ReferenceEquals(this.vParams,null))
            {
                this.vParams.Clear();
            }

            if (this.vFreeORMFilter)
            {
                this.vORMFilter = null;
            }
        }
        #endregion

        #region Execute
        /// <summary>
        /// Executes the SQL sentence
        /// </summary>
        /// <param name="pDBConnection">Database connection to execute the query</param>
        public void Execute(DBConnection pDBConnection)
        {
            DbCommand vSQLQuery;
            //TO-DO Select suported
            try
            {
                vSQLQuery = pDBConnection.DBQuery;
                vSQLQuery.Parameters.Clear();
                vSQLQuery.CommandText = this.vSQLText.ToString();

                this.SetParamsTo(pDBConnection);
                //set transactrion
                if (pDBConnection.InTransaction)
                {
                    vSQLQuery.Transaction = pDBConnection.BDTransaction;
                }

                vSQLQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}