using System.Collections.Generic;
using System.Data;

namespace Robotics.DataLayer.ORM.Record
{
    /// <summary>
    /// Class Basic for recordset BBDD
    /// </summary>
    public abstract class ORMRecordsetBase<T> : List<T>
    {
        #region Property
        /// <summary>
        /// Get Recordset as a DataTable
        /// </summary>
        public DataTable AsDataTable => this.GetDataTable();

        #endregion

        //private functions

        #region GetDataTable
        /// <summary>
        /// return Recordset as a DataTable
        /// </summary>
        /// <returns>return datatable</returns>
        public DataTable GetDataTable(string pTableName)
        {
            DataTable vResult = null;

            if (this.Count > 0)
            {
                //create datatable
                var vORMRecord = (this[0] as ORMRecordBase);
                vResult = vORMRecord.GetDataTable(pTableName);
                //add datarows
                for (var vIndex = 0; vIndex < this.Count; vIndex++)
                {
                    vORMRecord = (this[vIndex] as ORMRecordBase);
                    vORMRecord.SetDataTable(ref vResult);
                }
            }

            return vResult;
        }
        /// <summary>
        /// return Recordset as a DataTable
        /// </summary>
        /// <returns>return datatable</returns>
        public DataTable GetDataTable()
        {
            return this.Count > 0 ? this.GetDataTable((this[0] as ORMRecordBase).TableName) : null;
        }
        #endregion
    }
}