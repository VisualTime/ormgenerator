﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Robotics.DataLayer.ORM.Field;

namespace Robotics.DataLayer.ORM.Record
{
    #region ORMRecordBase
    /// <summary>
    ///  Class basic for Record BBDD
    /// </summary>
    public class ORMRecordBase : IDisposable
    {
        #region Variable
        protected bool vDisposed = false;
        protected string vTableName;
        protected bool vPKExplicit = false;
        protected List<ORMField> vPrimaryKeyList;
        #endregion

        #region Property
        /// <summary>
        /// Rerturn field by name
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public ORMField this[string fieldName]
        {
            get
            {
                return FieldListToArray.Single(field => string.Equals(field.FieldName, fieldName, StringComparison.CurrentCultureIgnoreCase));
            }
        }
        /// <summary>
        ///  Log Field List
        /// </summary>
        public string TableName => vTableName;

        /// <summary>
        ///  Primary Key List
        /// </summary>
        public List<ORMField> PrimaryKeyList => vPrimaryKeyList;

        /// <summary>
        ///  Field List
        /// </summary>
        public SortedList<string, ORMField> FieldList { get; private set; }

        /// <summary>
        ///  Field List
        /// </summary>
        public ORMField[] FieldListToArray => ToArray();

        /// <summary>
        /// Indicates if table have Explicit PK
        /// </summary>
        public bool PKExplicit => vPKExplicit;

        #endregion

        #region Constructor
        /// <summary>
        ///  Default Constructor
        /// </summary>
        public ORMRecordBase(string pTableName)
        {
            vTableName = pTableName;
            FieldList = new SortedList<string, ORMField>();
            vPrimaryKeyList = new List<ORMField>();
        }
        #endregion

        #region Destructor
        /// <summary>
        ///  destructor de la clase
        /// </summary>
        ~ORMRecordBase()
        {
            Dispose(false);
        }
        /// <summary>
        /// Método sobrecargado de Dispose que será el que libera los recursos.
        /// Controla que solo se ejecute dicha lógica una vez y evita que el GC tenga que llamar al destructor de clase.
        /// </summary>
        /// <param name="vDisposed">boolean that indicates if free managed resources</param>
        protected virtual void Dispose(bool pDisposable)
        {
            if (!vDisposed)
            {
                //indicamos que ya se destruye el objeto
                vDisposed = true;
                if (pDisposable)
                {
                    FieldList.Clear();
                    FieldList = null;
                    vPrimaryKeyList.Clear();
                    vPrimaryKeyList = null;
                }
                GC.SuppressFinalize(this);
            }
        }
        /// <summary>
        /// Método de IDisposable para desechar la clase.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

        //private functions

        #region ToArray
        /// <summary>
        /// return FeldList to array
        /// </summary>
        /// <returns>return field as an array</returns>
        public ORMField[] ToArray()
        {
            return FieldList.Values.ToArray();
        }
        #endregion

        //protected functions

        #region Creates
        /// <summary>
        ///  Create a integer field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an integer field</returns>
        protected ORMIntegerField CreateIntegerField(string pFieldName)
        {
            var vField = new ORMIntegerField(pFieldName, vTableName);
            this.Add(vField);
            return vField;
        }
		/// <summary>
        ///  Create a small integer field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an small integer field</returns>
        protected ORMSmallIntegerField CreateSmallIntegerField(string pFieldName)
        {
            var vField = new ORMSmallIntegerField(pFieldName, vTableName);
            this.Add(vField);
            return vField;
        }
		/// <summary>
        ///  Create a big integer field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an big integer field</returns>
        protected ORMBigIntegerField CreateBigIntegerField(string pFieldName)
        {
            var vField = new ORMBigIntegerField(pFieldName, vTableName);
            this.Add(vField);
            return vField;
        }
        /// <summary>
        ///  Create a string field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an string field</returns>
        protected ORMStringField CreateStringField(string pFieldName)
        {
            var vField = new ORMStringField(pFieldName, vTableName, 255);
            this.Add(vField);
            return vField;
        }
        /// <summary>
        ///  Create a string field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an string field</returns>
        protected ORMStringField CreateStringField(string pFieldName, int pSize)
        {
            var vField = new ORMStringField(pFieldName, vTableName, pSize);
            this.Add(vField);
            return vField;
        }
        /// <summary>
        ///  Create a datetime field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an datetime field</returns>
        protected ORMDateTimeField CreateDateTimeField(string pFieldName)
        {
            var vField = new ORMDateTimeField(pFieldName, vTableName);
            this.Add(vField);
            return vField;
        }
        /// <summary>
        ///  Create a boolchar field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an boolchar field</returns>
        protected ORMBooleanField CreateBooleanField(string pFieldName)
        {
            var vField = new ORMBooleanField(pFieldName, vTableName);
            this.Add(vField);
            return vField;
        }
        /// <summary>
        ///  Create a binary field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an binary field</returns>
        protected ORMByteField CreateByteField(string pFieldName)
        {
            var vField = new ORMByteField(pFieldName, vTableName);
            this.Add(vField);
            return vField;
        }
        /// <summary>
        ///  Create a float field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an float field</returns>
        protected ORMFloatField CreateFloatField(string pFieldName)
        {
            var vField = new ORMFloatField(pFieldName, vTableName);
            this.Add(vField);
            return vField;
        }
		/// <summary>
        ///  Create a decimal field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an decimal field</returns>
        protected ORMDecimalField CreateDecimalField(string pFieldName)
        {
            var vField = new ORMDecimalField(pFieldName, vTableName);
            this.Add(vField);
            return vField;
        }
        /// <summary>
        /// Create a decimal field an adds it to FieldList with precision and scale
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <param name="pPrecision">precision to set</param>
        /// <param name="pScale">scale to set</param>
        /// <returns>The object to manage an decimal field</returns>
        protected ORMDecimalField CreateDecimalField(string pFieldName, int pPrecision, int pScale)
        {
            var vField = new ORMDecimalField(pFieldName, vTableName, pPrecision,pScale);
            this.Add(vField);
            return vField;
        }
        /// <summary>
        ///  Create a binary field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an float field</returns>
        protected ORMBinaryField CreateBinaryField(string pFieldName)
        {
            var vField = new ORMBinaryField(pFieldName, vTableName);
            this.Add(vField);
            return vField;
        }
		/// <summary>
        ///  Create a Guid field an adds it to FieldList
        /// </summary>
        /// <param name="pFieldName">Field name</param>
        /// <returns>The object to manage an guid field</returns>
        protected ORMGuidField CreateGuidField(string pFieldName)
        {
            var vField = new ORMGuidField(pFieldName, vTableName);
            this.Add(vField);
            return vField;
        }
        #endregion

        //public functions

        #region Add
        /// <summary>
        /// Add Field on list
        /// </summary>
        /// <param name="pField">field to add</param>
        /// <returns>return if operation executed sucessfully</returns>
        public bool Add(ORMField pField)
        {
            var vResult = FieldList.ContainsKey(pField.FullName);
            //check if field exist
            if (!vResult)
			{
                FieldList.Add(pField.FullName, pField);
			}
            //return result
            return !vResult;
        }
        /// <summary>
        /// Add range of fields on Record
        /// </summary>
        /// <param name="pField">Field to add</param>
        /// <returns>if True fields added</returns>
        public bool Add(ORMField[] pFields)
        {
            var vResult = true;

            foreach (var item in pFields)
            {
                if (vResult)
                {
                    vResult = vResult && Add(item);
                }
            }

            return vResult;
        }
        #endregion

        #region PrimaryKey
        /// <summary>
        /// Check if the primary key is not null
        /// </summary>
        /// <returns>True if the primary key is not null</returns>
        public bool PrimaryKeyNotNull()
        {
            var vOk = true;
            int vIndex;
            //check null value for all primary keys 
            for (vIndex = 0; vIndex < vPrimaryKeyList.Count; vIndex++)
            {
                vOk = (!vPrimaryKeyList[vIndex].IsNull);
                if (!vOk)
				{  
					break;
				}
            }
            return vOk;
        }
        /// <summary>
        ///  Check if a field belongs to primary key
        /// </summary>
        /// <param name="pField">Field to check if it belongs to primary key</param>
        /// <returns>True if the field belongs to primary key</returns>
        public bool BelongsToPrimaryKey(ORMField pField)
        {
            return (vPrimaryKeyList.IndexOf(pField) >= 0);
        }
        #endregion

        #region SetDirty
        /// <summary>
        /// Initializes all associated fields.
        /// </summary>
        public void SetDirty()
        {
            foreach (var item in FieldList.Values)
            {
                item.SetDirty();
            }
        }

        #endregion

        #region ToString
        /// <summary>
        /// Override generic ToString method
        /// </summary>
        /// <returns>return record as string</returns>
        public override string ToString()
        {
            return string.Format("{0} [{1}]",vTableName,string.Join(", ", FieldListToArray.Select(field => field.FieldName).ToArray()));
        }
        #endregion

        #region GetDataTable
        /// <summary>
        /// Get DataTable from record
        /// </summary>
        /// <param name="pDataTableName">TableName</param>
        /// <returns>return datatable struct with tablename</returns>
        public DataTable GetDataTable(string pDataTableName)
        {
            //create datatable
            var vDataTable = new DataTable(pDataTableName);
            //create columns
            foreach (var item in FieldListToArray)
            {
                if (item is ORMIntegerField)
                {
                    vDataTable.Columns.Add(new DataColumn(item.FieldName, typeof(int)));
                }
                if (item is ORMSmallIntegerField)
                {
                    vDataTable.Columns.Add(new DataColumn(item.FieldName, typeof(short)));
                }
                if (item is ORMBigIntegerField)
                {
                    vDataTable.Columns.Add(new DataColumn(item.FieldName, typeof(long)));
                }
                if (item is ORMFloatField)
                {
                    vDataTable.Columns.Add(new DataColumn(item.FieldName, typeof(double)));
                }
                if (item is ORMDecimalField)
                {
                    vDataTable.Columns.Add(new DataColumn(item.FieldName, typeof(decimal)));
                }
                if (item is ORMGuidField)
                {
                    vDataTable.Columns.Add(new DataColumn(item.FieldName, typeof(Guid)));
                }
                if (item is ORMStringField)
                {
                    vDataTable.Columns.Add(new DataColumn(item.FieldName, typeof(string)));
                }
                if (item is ORMDateTimeField)
                {
                    vDataTable.Columns.Add(new DataColumn(item.FieldName, typeof(DateTime)));
                }
                if (item is ORMBooleanField)
                {
                    vDataTable.Columns.Add(new DataColumn(item.FieldName, typeof(bool)));
                }
                if (item is ORMByteField)
                {
                    vDataTable.Columns.Add(new DataColumn(item.FieldName, typeof(byte)));
                }
            }

            return vDataTable;
        }
        /// <summary>
        /// Get DataTable from record. Select Table Name for datatable name.
        /// </summary>
        /// <returns>return datatable struct with tablename</returns>
        public DataTable GetDataTable()
        {
            return GetDataTable(vTableName);
        }
        #endregion

        #region SetDataTable
        /// <summary>
        /// Set DataTable adding a row with record information
        /// </summary>
        /// <param name="pDataTable">DatatTable to add row</param>
        public void SetDataTable(ref DataTable pDataTable)
        {
            //create row
            var vRow = pDataTable.NewRow();
            //set row
            foreach (var item in FieldListToArray)
            {
                item.SetValueTo(ref vRow);
            }
            //add row
            pDataTable.Rows.Add(vRow);
        }
        #endregion
    }
    #endregion
}