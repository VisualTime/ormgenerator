﻿namespace Robotics.DataLayer.ORM.Database.Log
{
    /// <summary>
    /// public interface for logs
    /// </summary>
    public interface ILog
    {
        bool Initialize(string pClassName);
        bool Initialize(string pFileName, string pClassName);
        void LogException(string pMessage);
        void LogError(string pMessage);
        void LogWarning(string pMessage);
        void LogInfo(string pMessage);
        void LogDebug(string pMessage);
    }
}
