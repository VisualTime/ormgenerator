﻿using Robotics.VTBase;
using System;

namespace Robotics.DataLayer.ORM.Database.Log
{
    public class StreamLog : ILog
    {
        private roLog oLog;

        /// <summary>
        /// Initialize Log Class
        /// </summary>
        /// <param name="pClassName">Class name for log messages</param>
        /// <returns></returns>
        bool ILog.Initialize(string pClassName)
        {
            var result = true;
            try
            {
                oLog = new roLog(pClassName);
            }
            catch
            {
                result = false;
            }

            return result;
        }
        /// <summary>
        /// Initializr Log Class
        /// </summary>
        /// <param name="pClassName">Class name for log messages</param>
        /// <returns></returns>
        bool ILog.Initialize(string pFileName, string pClassName)
        {
            var result = true;
            try
            {
                oLog = new roLog(pFileName,pClassName);
            }
            catch
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Log Debug Message
        /// </summary>
        /// <param name="pMessage">message to log</param>
        void ILog.LogDebug(string pMessage)
        {
            oLog.logMessage(roLog.EventType.roDebug, pMessage);
        }

        /// <summary>
        /// Log Error Message
        /// </summary>
        /// <param name="pMessage">message to log</param>
        void ILog.LogError(string pMessage)
        {
            oLog.logMessage(roLog.EventType.roError, pMessage);
        }

        /// <summary>
        /// Log Exception Message
        /// </summary>
        /// <param name="pMessage">message to log</param>
        void ILog.LogException(string pMessage)
        {
            oLog.logMessage(roLog.EventType.roError, pMessage);
        }

        /// <summary>
        /// Log Info Message
        /// </summary>
        /// <param name="pMessage">message to log</param>
        void ILog.LogInfo(string pMessage)
        {
            oLog.logMessage(roLog.EventType.roInfo, pMessage);
        }

        /// <summary>
        /// Log Warning Message
        /// </summary>
        /// <param name="pMessage">message to log</param>
        void ILog.LogWarning(string pMessage)
        {
            oLog.logMessage(roLog.EventType.roWarning, pMessage);
        }
    }
}
