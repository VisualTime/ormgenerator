﻿namespace Robotics.DataLayer.ORM.Database.Log
{
    public static class LogFactory
    {
        public const string TYPE_STREAM = "TYPE_STREAM";

        /// <summary>
        /// Get Log Instance by Type
        /// </summary>
        /// <param name="pType">type to instance</param>
        /// <returns></returns>
        public static ILog GetInstance(string pType)
        {
            ILog oLog;

            switch (pType)
            {
                case TYPE_STREAM:
                    oLog = new StreamLog();
                    break;
                default:
                    oLog = new StreamLog();
                    break;
            }

            return oLog;
        }
    }
}
