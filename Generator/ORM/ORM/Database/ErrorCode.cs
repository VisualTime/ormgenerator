﻿namespace Robotics.DataLayer.ORM.Database
{
    /// <summary>
    /// Global Errors
    /// </summary>
    public class ErrorCode
    {
        public const int NO_ERROR = 0;
        public const int ERROR_EXCEPTION = -1;

        #region ErrorDatabase
        /// <summary>
        /// Error Database
        /// </summary>
        public class ErrorDatabase
        {
            public const int DBF_PRIMARY_KEY_IS_NULL = -10;
            public const int DBF_DATABASE_NOT_CONNECTED = -11;
            public const int DBF_CONNECTION_NOT_AVAILABLE = -12;
            public const int DBF_RECORD_NOT_EXISTS = -13;
            public const int DBF_SEQUENCE_NOT_EXISTS = -2;
            public const int DBF_FILTER_EMPTY = -3;
        }
        #endregion

        //public functions

        #region GetError
        /// <summary>
        /// Get Error description
        /// </summary>
        /// <param name="pError">error to get description</param>
        /// <returns>return description</returns>
        public static string GetError(int pError)
        {
            switch (pError)
            {
                //global errors
                case NO_ERROR:
                    return "NO ERROR";
                case ERROR_EXCEPTION:
                    return "EXCEPTION NO CONTROLLED";
                //databse errors
                case ErrorDatabase.DBF_PRIMARY_KEY_IS_NULL:
                    return "PRIMARY KEY NULL";
                case ErrorDatabase.DBF_DATABASE_NOT_CONNECTED:
                    return "DATABASE NOT CONNECTED";
                case ErrorDatabase.DBF_CONNECTION_NOT_AVAILABLE:
                    return "CONNECTION NOT AVAIBLE";
                case ErrorDatabase.DBF_RECORD_NOT_EXISTS:
                    return "RECORD NOT EXISTS";
                default:
                    return "UNKNOW ERROR";
            }
        }
        #endregion
    }
}
