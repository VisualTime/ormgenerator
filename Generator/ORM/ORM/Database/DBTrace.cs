﻿using Robotics.DataLayer.ORM.Database.Log;
using Robotics.VTBase;
using System;

namespace Robotics.DataLayer.ORM.Database
{
    public class DBTrace : IDisposable
    {
        public const byte levelTraceDisabled = 0;//None
        public const byte levelTraceException = 1;//Exception messages
        public const byte levelTraceError = 2;//Exception and  error messages
        public const byte levelTraceWarning = 3;//Warning messages and error/exception messages
        public const byte levelTraceInfo = 4;//Informational messages, warning messages, and error/exception messages

        private int vLevel;
        private bool vDisposed;
        private ILog oLog;

        /// <summary>
        /// Constructor by Default
        /// </summary>
        /// <param name="pLevel">level to write logs</param>
        public DBTrace(int pLevel)
        {
            vLevel = pLevel;
            oLog = LogFactory.GetInstance(LogFactory.TYPE_STREAM);
            oLog.Initialize("VisualTime");
        }

        /// <summary>
        ///  Destructor
        /// </summary>
        ~DBTrace()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Override Method for free all resources
        /// Controls that execute this process only one time and stop, if is necessary, the call of destructor by Garbage Collector
        /// </summary>
        /// <param name="pDisposable">
        /// The p Disposable.
        /// </param>
        protected virtual void Dispose(bool pDisposable)
        {
            if (this.vDisposed)
            {
                return;
            }

            this.vDisposed = true;
        }

        /// <summary>
        /// Method IDisposable for destroy the class
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///  Trace for exception functions
        /// </summary>
        /// <param name="pMessage">message to write</param>
        public void TraceException(string pMessage)
        {
            if (vLevel > 0 && vLevel >= levelTraceException)
            {
                oLog.LogException(pMessage);
            }
        }

        /// <summary>
        ///  Trace for error functions
        /// </summary>
        /// /// <param name="pMessage">message to write</param>
        public void TraceError(string pMessage)
        {
            if (vLevel > 0 && vLevel >= levelTraceError)
            {
                oLog.LogError(pMessage);
            }
        }

        /// <summary>
        ///  Trace for warning functions
        /// </summary>
        /// <param name="pMessage">message to write</param>
        public void TraceWarning(string pMessage)
        {
            if (vLevel > 0 && vLevel >= levelTraceWarning)
            {
                oLog.LogWarning(pMessage);
            }
        }

        /// <summary>
        ///  Trace for info functions
        /// </summary>
        /// <param name="pMessage">message to write</param>
        public void TraceInfo(string pMessage)
        {
            if (vLevel > 0 && vLevel >= levelTraceInfo)
            {
                oLog.LogInfo(pMessage);
            }
        }

        /// <summary>
        ///  Trace for verbose functions
        /// </summary>
        /// <param name="pMessage">message to write</param>
        public void TraceVerbose(string pMessage)
        {
            if (vLevel > 0 && vLevel >= levelTraceInfo)
            {
                oLog.LogInfo(pMessage);
            }
        }
    }
}
