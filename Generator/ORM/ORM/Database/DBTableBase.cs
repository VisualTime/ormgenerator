﻿using Robotics.DataLayer.ORM.Database.Log;
using Robotics.DataLayer.ORM.Field;
using Robotics.DataLayer.ORM.Query;
using Robotics.DataLayer.ORM.Record;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Robotics.DataLayer.ORM.Database
{
    /// <summary>
    ///  Class base for Table BBDD
    /// </summary>
    public class DBTableBase : IDisposable
    {
        #region Constants
        public const byte PK_TYPE_EXPLICIT = 1;
        public const byte PK_TYPE_IMPLICIT = 2;
        #endregion

        #region Variables
        private bool vDisposed = false;
        protected string vTableName;
        protected string vAlias;
        protected string vDatabaseName;
        protected DBTrace vTrace;
        protected DBConnection vDBConnection;
        #endregion

        #region Properties
        /// <summary>
        /// Type of primary key (explicit, implicit, sequence)
        /// </summary>
        public byte PKType
        {
            get;
            set;
        }
        /// <summary>
        ///  Get name of table
        /// </summary>
        public string TableName
        {
            get { return this.vTableName; }
        }
        /// <summary>
        ///  Get name of table
        /// </summary>
        public string DatabaseName
        {
            get { return this.vDatabaseName; }
            set { this.vDatabaseName = value; }
        }
        /// <summary>
        ///  Get or set Alias of table
        /// </summary>
        public string Alias
        {
            get { return this.vAlias; }
            set { this.vAlias = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        ///  TORMTableBase Constructor
        /// </summary>
        public DBTableBase(string pTableName)
        {
            this.vTableName = pTableName;
            this.vDatabaseName = "";
            this.vAlias = "";
            this.PKType = PK_TYPE_EXPLICIT;
            this.vTrace = new DBTrace(DBTrace.levelTraceWarning);
            this.vDBConnection = new DBConnection();
        }
        #endregion

        #region Destructor
        /// <summary>
        ///  destructor de la clase
        /// </summary>
        ~DBTableBase()
        {
            this.Dispose(false);
        }
        /// <summary>
        /// Método sobrecargado de Dispose que será el que libera los recursos.
        /// Controla que solo se ejecute dicha lógica una vez y evita que el GC tenga que llamar al destructor de clase.
        /// </summary>
        /// <param name="vDisposed">boolean that indicates if free managed resources</param>
        protected virtual void Dispose(bool pDisposable)
        {
            if (!this.vDisposed)
            {
                //indicamos que ya se destruye el objeto
                this.vDisposed = true;
                GC.SuppressFinalize(this);
            }
        }
        /// <summary>
        /// Método de IDisposable para desechar la clase.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }
        #endregion

        //protected functions

        #region GetOrderByArray
        /// <summary>
        /// Return array of bool with the same order
        /// </summary>
        /// <param name="pValue">bool value to replicate</param>
        /// <param name="pOrderFields">array with order fields</param>
        /// <returns>return array with value replicated</returns>
        protected bool[] GetOrderByArray(bool pValue, ORMField[] pOrderFields)
        {
            List<bool> vResult;
            //check lenght array
            if (!ReferenceEquals(pOrderFields, null) && pOrderFields.Length > 0)
            {
                vResult = new List<bool>();
                //set bool value on all positions
                for (int vIndex = 0; vIndex < pOrderFields.Length; vIndex++)
                {
                    vResult.Add(pValue);
                }

                return vResult.ToArray();
            }
            else
                return null;
        }
        #endregion

        #region CheckKey
        /// <summary>
        /// Set Key Log
        /// </summary>
        /// <param name="pRecord">record to get key</param>
        private bool CheckKey(ORMRecordBase pRecord)
        {
            return (!ReferenceEquals(pRecord.PrimaryKeyList, null) && pRecord.PrimaryKeyList.Count > 0 && pRecord.PrimaryKeyList[0].AsString.Length > 0);
        }
        #endregion

        #region Trace
        /// <summary>
        /// Traces a verbose into a function
        /// </summary>
        /// <param name="pFunction">Name of the function</param>
        /// <param name="pText">Message text</param>
        /// <param name="pArgs">Values to tracing</param>
        protected void TraceVerbose(string pFunction, string pText)
        {
            if (!ReferenceEquals(this.vTrace, null))
            {
                this.vTrace.TraceVerbose(string.Format("{0}-{1}",pFunction, pText));
            }
        }
        /// <summary>
        /// Traces an exception and return the error code for the exception
        /// </summary>
        /// <param name="pFunction">Name of the function</param>
        /// <param name="pException">Exceptino Message text</param>
        protected int TraceException(string pFunction, string pException)
        {
            if (!ReferenceEquals(this.vTrace, null))
            {
                this.vTrace.TraceException(string.Format("{0}-{1}", pFunction, pException));
            }
            return ErrorCode.ERROR_EXCEPTION;
        }
        /// <summary>
        ///  Trace the modified fields in the list
        /// </summary>
        /// <param name="pFieldList">Field list to trace</param>
        /// <returns>String with the trace</returns>
        protected string TraceFields(IList<ORMField> pFieldList)
        {
            int vIndex;
            ORMField vField;
            StringBuilder vArgs = new StringBuilder();

            if (!ReferenceEquals(this.vTrace, null) && !ReferenceEquals(pFieldList, null))
            {
                object value = null;

                for (vIndex = 0; vIndex < pFieldList.Count; vIndex++)
                {
                    vField = pFieldList[vIndex];
                    if (vField.IsModified)
                    {
                        vArgs.Append(vField.FieldName + ":");

                        if (!vField.IsNull)
                        {
                            if (vField is ORMByteField)
                            {
                                value = ((ORMByteField)vField).Value;
                            }
                            if (vField is ORMIntegerField)
                            {
                                value = ((ORMIntegerField)vField).Value;
                            }
                            if (vField is ORMSmallIntegerField)
                            {
                                value = ((ORMSmallIntegerField)vField).Value;
                            }
                            if (vField is ORMBigIntegerField)
                            {
                                value = ((ORMBigIntegerField)vField).Value;
                            }
                            if (vField is ORMFloatField)
                            {
                                value = ((ORMFloatField)vField).Value;
                            }
                            if (vField is ORMDecimalField)
                            {
                                value = ((ORMDecimalField)vField).Value;
                            }
                            if (vField is ORMStringField)
                            {
                                value = ((ORMStringField)vField).Value;
                            }
                            if (vField is ORMDateTimeField)
                            {
                                value = ((ORMDateTimeField)vField).Value;
                            }
                            if (vField is ORMBooleanField)
                            {
                                value = ((ORMBooleanField)vField).Value;
                            }
                            if (vField is ORMGuidField)
                            {
                                value = ((ORMGuidField)vField).Value;
                            }
                            if (value != null)
                            {
                                vArgs.Append(value.ToString());
                            }
                        }
                        else
                        {
                            vArgs.Append("Null");
                        }
                        //add separator item
                        vArgs.Append(";");
                    }
                }
            }
            return vArgs.ToString();
        }
        protected string TraceFields(ORMField[] pFieldList)
        {
            if (!ReferenceEquals(pFieldList, null))
            {
                return this.TraceFields(new List<ORMField>(pFieldList));
            }
            else
            {
                return "";
            }
        }
        /// <summary>
        ///  Trace the modified fields in the record
        /// </summary>
        /// <param name="pORMRecord">Record to trace</param>
        /// <returns>String with the trace</returns>
        protected string TraceRecord(ORMRecordBase pORMRecord)
        {
            if (!ReferenceEquals(pORMRecord, null))
            {
                return this.TraceFields(pORMRecord.FieldList.Values);
            }
            else
            {
                return "";
            }
        }
        /// <summary>
        ///  Trace the filter for a sentence
        /// </summary>
        /// <param name="pORMFilter">Filter to trace</param>
        /// <returns>String with the trace</returns>
        protected string TraceFilter(ORMFilter pORMFilter)
        {
            if (!ReferenceEquals(this.vTrace, null) && !ReferenceEquals(pORMFilter, null))
            {
                return "SQL" + pORMFilter.SQLText + this.TraceFields(pORMFilter.Params);
            }
            else
            {
                return "";
            }
        }
        #endregion

        #region CreateSelect
        /// <summary>
        /// Creates a new Select Query
        /// </summary>
        /// <returns>The new filter</returns>
        public ORMSelectQuery CreateSelect()
        {
            return new ORMSelectQuery();
        }
        #endregion  

        #region ORMRecordToORMFilter
        /// <summary>
        /// Convert a database record (modified fields) to a database filter
        /// </summary>
        /// <param name="pORMRecord">Database record to convert</param>
        /// <param name="pOnlyPrimaryKey">Only convert the primary key fields</param>
        /// <returns>The filter created</returns>
        protected ORMFilter ORMRecordToORMFilter(ORMRecordBase pORMRecord, bool pOnlyPrimaryKey)
        {
            ORMFilter vORMFilter = new ORMFilter();
            IList<ORMField> vFieldList;
            int vIndex;

            // Get the all field list or only the primary key list
            vFieldList = pOnlyPrimaryKey ? pORMRecord.PrimaryKeyList : pORMRecord.FieldList.Values;

            // Create the filter with the fields of database record
            for (vIndex = 0; vIndex < vFieldList.Count; vIndex++)
            {
                if (vFieldList[vIndex].IsModified || pOnlyPrimaryKey)
                {
                    if (!vFieldList[vIndex].IsNull)
                    {
                        if (vFieldList[vIndex] is ORMIntegerField)
                        {
                            vORMFilter = vORMFilter.And_(vFieldList[vIndex]).EQ((vFieldList[vIndex] as ORMIntegerField).Value);
                        }
                        if (vFieldList[vIndex] is ORMSmallIntegerField)
                        {
                            vORMFilter = vORMFilter.And_(vFieldList[vIndex]).EQ((vFieldList[vIndex] as ORMSmallIntegerField).Value);
                        }
                        if (vFieldList[vIndex] is ORMBigIntegerField)
                        {
                            vORMFilter = vORMFilter.And_(vFieldList[vIndex]).EQ((vFieldList[vIndex] as ORMBigIntegerField).Value);
                        }
                        if (vFieldList[vIndex] is ORMFloatField)
                        {
                            vORMFilter = vORMFilter.And_(vFieldList[vIndex]).EQ((vFieldList[vIndex] as ORMFloatField).Value);
                        }
                        if (vFieldList[vIndex] is ORMDecimalField)
                        {
                            vORMFilter = vORMFilter.And_(vFieldList[vIndex]).EQ((vFieldList[vIndex] as ORMDecimalField).Value);
                        }
                        if (vFieldList[vIndex] is ORMStringField)
                        {
                            vORMFilter = vORMFilter.And_(vFieldList[vIndex]).EQ((vFieldList[vIndex] as ORMStringField).Value);
                        }
                        if (vFieldList[vIndex] is ORMDateTimeField)
                        {
                            vORMFilter = vORMFilter.And_(vFieldList[vIndex]).EQ((vFieldList[vIndex] as ORMDateTimeField).Value);
                        }
                        if (vFieldList[vIndex] is ORMBooleanField)
                        {
                            vORMFilter = vORMFilter.And_(vFieldList[vIndex]).EQ((vFieldList[vIndex] as ORMBooleanField).Value);
                        }
                        if (vFieldList[vIndex] is ORMByteField)
                        {
                            vORMFilter = vORMFilter.And_(vFieldList[vIndex]).EQ((vFieldList[vIndex] as ORMByteField).Value);
                        }
                        if (vFieldList[vIndex] is ORMGuidField)
                        {
                            vORMFilter = vORMFilter.And_(vFieldList[vIndex]).EQ((vFieldList[vIndex] as ORMGuidField).Value);
                        }
                    }
                    else
                    {
                        vORMFilter.And_(vFieldList[vIndex]).IsNull();
                    }
                }
            }
            return vORMFilter;
        }
        #endregion

        //public functions

        #region CreateFilter
        /// <summary>
        /// Creates a new filter
        /// </summary>
        /// <returns>The new filter</returns>
        public ORMFilter CreateFilter()
        {
            return new ORMFilter();
        }
        #endregion

        #region GetDBConnection
        /// <summary>
        /// Function to get a free database resource.
        /// </summary>
        /// <param name="pDBConnection">Database connection to use</param>
        /// <param name="pGetDBConn">Indicates if needs a new connection</param>
        /// <returns>True if there is a free database connection in the pool</returns>
        public bool GetDBConnection(ref DBConnection pDBConnection, bool pGetDBConn)
        {
            bool vResult = true;

            if (pGetDBConn)
            {
                this.vDBConnection.Open();
                pDBConnection = this.vDBConnection;
            }

            return vResult;
        }
        /// <summary>
        /// Function to get a free database resource. Nedds create a new connection
        /// </summary>
        /// <param name="pDBConnection">Database connection to use</param>
        /// <returns>True if there is a free database connection in the pool</returns>
        public bool GetDBConnection(ref DBConnection pDBConnection)
        {
            return this.GetDBConnection(ref pDBConnection, true);
        }
        #endregion

        #region ReleaseDBConnection
        /// <summary>
        /// Function to release a database connection
        /// </summary>
        /// <param name="pDBConnection">Database connection to use</param>
        /// <param name="pReleaseDBConn">Database connection to release</param>
        public void ReleaseDBConnection(ref DBConnection pDBConnection, bool pReleaseDBConn)
        {
            if (pReleaseDBConn)
            {
                this.vDBConnection.Close();
            }
        }
        /// <summary>
        /// Function to release a database connection.
        /// </summary>
        /// <param name="pDBConnection">Database connection to release</param>
        public void ReleaseDBConnection(ref Database.DBConnection pDBConnection)
        {
            this.ReleaseDBConnection(ref pDBConnection, true);
        }
        #endregion

        #region GetDataTable
        /// <summary>
        /// Get DataTable associated with Query. Use or create Connection
        /// </summary>
        /// <param name="vQuery">Query to execute</param>
        /// <param name="pDBConnection">connection to use</param>
        /// <returns></returns>
        public DataTable GetDataTable(ORMSelectQuery vQuery, Database.DBConnection pDBConnection)
        {
            bool vDBNotAssigned;
            DataSet vResult = null;

            this.TraceVerbose("Enter GetDataTable", this.TraceFilter(vQuery.ORMFilter));

            vDBNotAssigned = (ReferenceEquals(pDBConnection, null));

            if (this.GetDBConnection(ref pDBConnection, vDBNotAssigned))
            {
                try
                {
                    if (pDBConnection.Connected) // Check if database is connected.
                    {
                        try
                        {
                            vQuery.Open(out vResult, pDBConnection);
                        }
                        catch (Exception ex)
                        {
                            this.TraceException("GetDataTable", ex.Message);
                        }

                        vQuery.Dispose();
                        vQuery = null;
                    }
                    else
                        return null;
                }
                finally
                {
                    this.ReleaseDBConnection(ref pDBConnection, vDBNotAssigned);
                }
            }
            else
                return null;

            this.TraceVerbose("Leave GetDataTable", ErrorCode.GetError(ErrorCode.NO_ERROR));
            return vResult.Tables[0];
        }
        /// <summary>
        /// Get DataTable associated to query. Create connection
        /// </summary>
        /// <param name="vQuery">query to execute</param>
        /// <returns>datat6able</returns>
        public DataTable GetDataTable(ORMSelectQuery vQuery)
        {
            return this.GetDataTable(vQuery, null);
        }
        #endregion

        #region ToString
        /// <summary>
        /// Return table as string, with important info
        /// </summary>
        /// <returns>return table description</returns>
        public override string ToString()
        {
            return string.Format
                (
                    "[{0}].[{1}]",
                    this.DatabaseName ?? "Database",
                    this.TableName
                );
        }
        #endregion
    }
}
