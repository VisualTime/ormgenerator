using System;
using System.Data.Common;
using System.Data.SqlClient;

namespace Robotics.DataLayer.ORM.Database
{
    /// <summary>
    ///  Class to manage a database connection
    /// </summary>
    public class DBConnection : IDisposable
    {
        private DbCommand dbQuery;

        private bool vDisposed; // variable for free memory

        /// <summary>
        ///  Database manager connection constructor
        /// </summary>
        /// <param name="pDBConnection">Database connection string</param>
        public DBConnection()
        {
            this.vDisposed = false;
            this.InTransaction = false;
        }

        /// <summary>
        /// Open connection
        /// </summary>
        /// <returns>if true connection opened successfully</returns>
        public bool Open()
        {
            var result = true;
            if (Connection==null || !Connected)
            {
                try
                {
                    this.Connection = AccessHelper.CreateConnection();
                    this.dbQuery = new SqlCommand
                    {
                        CommandType = System.Data.CommandType.Text,
                        Connection = (this.Connection as SqlConnection)
                    };
                }
                catch (Exception)
                {
                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Close Connection
        /// </summary>
        /// <returns></returns>
        public bool Close()
        {
            var result = true;
            try
            {
                if (this.BDTransaction != null)
                {
                    this.BDTransaction.Dispose();
                }
                if (this.Connection != null && this.Connected)
                {
                    this.Connection.Close();
                    this.Connection.Dispose();
                }
                if (DBQuery != null)
                {
                    this.DBQuery.Dispose();
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        ///  Destructor
        /// </summary>
        ~DBConnection()
        {
            this.Dispose(false);
        }

        // BBDD

        /// <summary>
        /// sql transaction
        /// </summary>
        public DbTransaction BDTransaction { get; private set; }

        /// <summary>
        ///  boolean for indicates if conection is opened
        /// </summary>
        public bool Connected => this.GetConnected();

        /// <summary>
        ///  get the SQL Connection object
        /// </summary>
        public DbConnection Connection { get; protected set; }

        /// <summary>
        ///  get the SQL Query object
        /// </summary>
        public DbCommand DBQuery
        {
            get
            {
                return this.dbQuery;
            }

            private set { this.dbQuery = value; }
        }

        /// <summary>
        /// boolean that indicates if connection is on transaction
        /// </summary>
        public bool InTransaction { get; private set; }

        /// <summary>
        ///  Commit pending operations
        /// </summary>
        public void Commit()
        {
            if (this.InTransaction)
            {
                this.BDTransaction.Commit();
                this.InTransaction = false;
            }
        }

        /// <summary>
        /// Method IDisposable for destroy the class
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Check if the database is connected
        /// </summary>
        /// <returns>True if the database is connected</returns>
        public bool GetConnected()
        {
            return this.Connection.State == System.Data.ConnectionState.Open;
        }

        /// <summary>
        ///  Rollback pending operations
        /// </summary>
        public void Rollback()
        {
            if (this.InTransaction)
            {
                this.BDTransaction.Rollback();
                this.InTransaction = false;
            }
        }

        /// <summary>
        ///  init a transaction
        /// </summary>
        /// <returns>identifier of transaction</returns>
        public bool StartTransaction()
        {
            if (!this.InTransaction)
            {
                this.BDTransaction = this.Connection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
                this.InTransaction = true;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Ends a transaction.
        /// </summary>
        /// <param name="vCommit">Commits the transaction. If false then Rollbacks</param>
        public void EndTransaction(bool pCommit)
        {
            if (pCommit)
            {
                this.Commit();
            }
            else
            {
                this.Rollback();
            }
        }

        /// <summary>
        /// Override Method for free all resources
        /// Controls that execute this process only one time and stop, if is necessary, the call of destructor by Garbage Collector
        /// </summary>
        /// <param name="pDisposable">
        /// The p Disposable.
        /// </param>
        protected virtual void Dispose(bool pDisposable)
        {
            if (this.vDisposed)
            {
                return;
            }

            this.vDisposed = true;

            if (pDisposable)
            {
                Close();
            }
        }
    }
}