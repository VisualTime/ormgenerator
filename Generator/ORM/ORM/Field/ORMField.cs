﻿using System;
using System.Data;

namespace Robotics.DataLayer.ORM.Field
{
    using System.Data.Common;

    #region ORMField
    /// <summary>
    ///  Generic class that represented one field in database
    /// </summary>
    public abstract class ORMField
    {
        #region Variables
        protected bool vIsModified;
        protected bool vIsNull;
        protected byte vFilterType;
        #endregion

        #region Property

        /// <summary>
        ///  property who indicates that value is null
        /// </summary>
        public bool IsNull
        {
            get { return vIsNull; }
            set { SetIsNull(value); }
        }
        /// <summary>
        ///  indicates if value is modified
        /// </summary>
        public bool IsModified
        {
            get { return vIsModified; }
            set { vIsModified = value; }
        }
        /// <summary>
        ///  name of field
        /// </summary>
        public string FieldName { get; }

        /// <summary>
        ///  name of field witn tablename how to prefix
        /// </summary>
        public string FullName { get; }

        /// <summary>
        ///  type of filter
        /// </summary>
        public byte FilterType => vFilterType;

        /// <summary>
        ///  set if column is sensitive
        /// </summary>
        public bool CaseSensitive { get; set; }

        /// <summary>
        ///  set if column is sensitive
        /// </summary>
        public string AsString
        {
            get
            {
                return GetAsString();
            }
            set
            {
                SetAsString(value);
            }
        }

        #endregion

        #region ClrType
        /// <summary>
        /// Gets the Common Language Runtime (CLR) type of current
        /// instance, formerly the .NET Framework type.
        /// </summary>
        public Type ClrType
        {
            get
            {
                if (this is ORMIntegerField)
				{
                    return typeof(int);
				}
				if (this is ORMSmallIntegerField)
				{
                    return typeof(short);
				}
				if (this is ORMBigIntegerField)
				{
                    return typeof(long);
				}
                if (this is ORMFloatField)
				{
                    return typeof(double);
				}
				 if (this is ORMDecimalField)
				{
                    return typeof(decimal);
				}
                if (this is ORMStringField)
				{
                    return typeof(string);
				}
                if (this is ORMDateTimeField)
				{
                    return typeof(DateTime);
				}
                if (this is ORMBooleanField)
				{
                    return typeof(bool);
				}
                if (this is ORMByteField)
				{
                    return typeof(byte);
				}
				if (this is ORMBinaryField)
				{
                    return typeof(byte[]);
				}
				if (this is ORMGuidField)
				{
                    return typeof(Guid);
				}
                else
				{
                    throw new NotSupportedException();
				}
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        ///  default constructor
        /// </summary>
        /// <param name="pFieldName">the name of field</param>
        /// <param name="pTableName">Table where the field is added</param>
        protected ORMField(string pFieldName, string pTableName)
        {
            FieldName = pFieldName.ToUpper();
            FullName = pFieldName.ToUpper();

            //check if have table name
            if (pTableName.Length > 0)
			{
                FullName = string.Format("{0}.{1}", pTableName.ToUpper(), FullName);
			}

            CaseSensitive = true;
            vIsNull = true;
            vIsModified = false;
        }
        /// <summary>
        ///  default constructor
        /// </summary>
        /// <param name="pFieldName">the name of field</param>
        protected ORMField(string pFieldName)
            : this(pFieldName, "")
        { }
        #endregion

        //abstract functions

        #region GetValueFrom

        /// <summary>
        /// Gets the field value from a Query
        /// </summary>
        /// <param name="pSqlDataReader">Query to get the field value</param>
        public abstract void GetValueFrom(DbDataReader pSqlDataReader);
        #endregion

        #region GetAsString
        /// <summary>
        ///  Get the field value converting in string type
        /// </summary>
        /// <returns></returns>
        protected abstract string GetAsString();
        #endregion

        #region SetValueTo

        /// <summary>
        /// Set the field value into a Query
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        /// <param name="pParamName">Parameter name to assign</param>
        public abstract void SetValueTo(DbParameterCollection pSqlParameters, string pParamName);

        /// <summary>
        /// Set the field value into a Query 
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        public virtual void SetValueTo(DbParameterCollection pSqlParameters)
        {
            this.SetValueTo(pSqlParameters, FieldName);
        }
        /// <summary>
        /// Set the field value into a DataRow
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        /// <param name="pParamName">Parameter name to assign</param>
        public abstract void SetValueTo(ref DataRow pRow);
        #endregion

        #region SetAsString
        /// <summary>
        ///  Set the field with string value
        /// </summary>
        /// <returns></returns>
        protected abstract void SetAsString(string pValue);
        #endregion

        #region InitializeValue
        /// <summary>
        /// In inherited classes, initializes current instance
        /// by setting default values to those represents the value
        /// of this field.
        /// </summary>
        protected abstract void InitializeValue();
        #endregion

        public abstract object RawValue
        {
            get;
        }

        //private functions

        #region SetIsNull
        /// <summary>
        ///  set the value at NULL
        /// </summary>
        private void SetIsNull(bool pValue)
        {
            vIsNull = pValue;
            vIsModified = true;
        }
        #endregion

        //public functions

        #region SetDirty
        /// <summary>
        /// Sets current instance as dirty, turning flags to
        /// notify that this object has not been changed.
        /// </summary>
        public virtual void SetDirty()
        {
            vIsModified = false;
            vIsNull = true;

            InitializeValue();
        }
        #endregion
    }
    #endregion
}
