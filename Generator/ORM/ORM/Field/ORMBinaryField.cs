using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Robotics.DataLayer.ORM.Field
{
    /// <summary>
    ///  Specific class that represented one binary field in database
    /// </summary>
    public class ORMBinaryField : ORMField
    {
        #region variables
        //variables
        private byte[] vValue;
        private byte[] vOldValue;
        #endregion

        #region Property
        /// <summary>
        ///  get or set the original field
        /// </summary>
        public byte[] Value
        {
            get { return vValue; }
            set { SetAsBinary(value); }
        }
        /// <summary>
        ///  get or set the old value field
        /// </summary>
        public byte[] OldValue
        {
            get { return this.vOldValue; }
        }
        /// <summary>
        ///  get or set the value as integer
        /// </summary>
        public int AsInteger
        {
            get { return this.GetAsInteger(); }
            set { this.SetAsInteger(value); }
        }
        /// <summary>
        ///  get or set the value as float
        /// </summary>
        public double AsFloat
        {
            get { return this.GetAsFloat(); }
            set { this.SetAsFloat(value); }
        }
        /// <summary>
        ///  get or set the value as float
        /// </summary>
        public decimal Asdecimal
        {
            get { return this.GetAsDecimal(); }
            set { this.SetAsDecimal(value); }
        }
        /// <summary>
        /// Get as object
        /// </summary>
        public override object RawValue => this.Value;

        #endregion

        #region Constructor
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pTableName">Table where the field is added</param>
        public ORMBinaryField(string pFieldName, string pTableName)
            : base(pFieldName, pTableName)
        {
            this.vOldValue = new byte[0];
            this.vValue = new byte[0];
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        public ORMBinaryField(string pFieldName)
            : this(pFieldName, "")
        {
        }
        #endregion

        //protected functions

        #region Gets
        /// <summary>
        ///  get the integer
        /// </summary>
        /// <returns>integer</returns>
        protected int GetAsInteger()
        {
            int vResult;

            return int.TryParse(AsString, out vResult) ? vResult : 0;
        }
        /// <summary>
        ///  get the float
        /// </summary>
        /// <returns>double</returns>
        protected double GetAsFloat()
        {
            double vResult;

            return double.TryParse(AsString, out vResult) ? vResult : 0;
        }
        /// <summary>
        ///  get the decimal
        /// </summary>
        /// <returns>decimal</returns>
        protected decimal GetAsDecimal()
        {
            decimal vResult;

            return decimal.TryParse(AsString, out vResult) ? vResult : 0;
        }
        /// <summary>
        ///  get the string
        /// </summary>
        /// <returns>string</returns>
        protected override string GetAsString()
        {
            return !this.IsNull ? System.Text.Encoding.UTF8.GetString(this.vValue, 0, this.vValue.Length) : "";
        }

        /// <summary>
        ///  get size of type
        /// </summary>
        /// <returns>size of int</returns>
        protected int GetDataSize()
        {
            return this.vValue.Length;
        }

        /// <summary>
        ///  initialize the binary value from one Recordet BBDD
        /// </summary>
        /// <param name="pSqlDataReader"></param>
        /// <param name="pSqlQuery">Recordet to get value</param>
        public override void GetValueFrom(DbDataReader pSqlDataReader)
        {
            if (!ReferenceEquals(pSqlDataReader, null))
            {
                this.vIsModified = false;
                this.vIsNull = (pSqlDataReader[this.FieldName] == DBNull.Value);
                if (!this.IsNull)
                {
                    this.vValue = pSqlDataReader[FieldName] as byte[];
                }
            }
            else
            {
                throw new Exception("Sql Parameters not assigned");
            }
        }
        #endregion

        #region Sets
        /// <summary>
        ///  set the field as integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsInteger(int pValue)
        {
            this.SetAsString(pValue.ToString());
        }
        /// <summary>
        ///  set the field from double number
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsFloat(double pValue)
        {
            this.SetAsString(pValue.ToString());
        }
        /// <summary>
        ///  set the field from string
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected override void SetAsString(string pValue)
        {
            SetAsBinary(System.Text.Encoding.UTF8.GetBytes(pValue));
        }
        /// <summary>
        ///  set the field from string
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsBinary(byte[] pValue)
        {
            this.vOldValue = this.vValue;
            this.vValue = pValue;
            this.IsNull = false;
        }

        /// <summary>
        ///  set the field from decimal
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsDecimal(decimal pValue)
        {
            this.SetAsString(pValue.ToString());
        }

        /// <summary>
        ///  Set the field value into a Query
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        /// <param name="pParameterName">Parameter name to assign</param>
        public override void SetValueTo(DbParameterCollection pSqlParameters, string pParameterName)
        {
            if (!ReferenceEquals(pSqlParameters, null))
            {
                //check is value is null
                var vParameter = this.IsNull
                    ? new SqlParameter(pParameterName, DBNull.Value)
                    : new SqlParameter(pParameterName, SqlDbType.Binary) {Value = this.vValue};
                pSqlParameters.Add(vParameter);
            }
            else
            {
                throw new Exception("Sql Parameters not assigned");
            }
        }
        /// <summary>
        ///  Set the field into a DataRow
        /// </summary>
        /// <param name="pRow">Row to set info</param>
        public override void SetValueTo(ref DataRow pRow)
        {
            if (pRow.Table.Columns.IndexOf(base.FieldName) >= 0 && !this.IsNull)
            {
                pRow[base.FieldName] = this.vValue;
            }
        }
        #endregion

        //override functions

        #region InitializeValue
        /// <summary>
        /// Initialize Value
        /// </summary>
        protected override void InitializeValue()
        {
            this.vValue = new byte[0];
            this.vOldValue = new byte[0];
        }
        #endregion

        #region ToString
        /// <summary>
        /// Return value as string
        /// </summary>
        /// <returns>return value</returns>
        public override string ToString()
        {
            return AsString;
        }
        #endregion
    }
}