using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Robotics.DataLayer.ORM.Field
{
    /// <summary>
    ///  Specific class that represented one datetime field in database
    /// </summary>
    public class ORMDateTimeField : ORMField
    {
        #region variables
        //variables
        private DateTime vValue;
        private DateTime vOldValue;
        #endregion

        #region Property
        /// <summary>
        ///  get or set the value
        /// </summary>
        public DateTime Value
        {
            get { return this.GetAsDateTime(); }
            set { this.SetAsDatetime(value); }
        }
        /// <summary>
        ///  get or set the old value field
        /// </summary>
        public DateTime OldValue
        {
            get { return this.vOldValue; }
        }
        /// <summary>
        ///  get or set the value as datetime
        /// </summary>
        public DateTime AsDateTime
        {
            get { return this.GetAsDateTime(); }
            set { this.SetAsDatetime(value); }
        }
        /// <summary>
        ///  get or set the value as float
        /// </summary>
        public double AsFloat
        {
            get { return this.GetAsFloat(); }
            set { this.SetAsFloat(value); }
        }
        /// <summary>
        /// get as an object
        /// </summary>
        public override object RawValue => this.Value;
        #endregion

        #region Constructor
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pTableName">Table where the field is added</param>
        public ORMDateTimeField(string pFieldName, string pTableName)
            : base(pFieldName, pTableName)
        {
            this.vOldValue = DateTime.MinValue;
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        public ORMDateTimeField(string pFieldName)
            : this(pFieldName, "")
        {
        }
        #endregion

        //protected functions

        #region Gets
        /// <summary>
        ///  get the string
        /// </summary>
        /// <returns>string</returns>
        protected double GetAsFloat()
        {
            return !this.IsNull ? this.vValue.ToOADate() : 0;
        }

        /// <summary>
        ///  get the string
        /// </summary>
        /// <returns>string</returns>
        protected override string GetAsString()
        {
            return !this.IsNull ? this.vValue.ToString() : "";
        }

        /// <summary>
        ///  get the datetime
        /// </summary>
        /// <returns>datetime</returns>
        protected DateTime GetAsDateTime()
        {
            return !this.IsNull ? this.vValue : DateTime.MinValue;
        }

        /// <summary>
        ///  initialize the integer value from one Recordet BDBD
        /// </summary>
        /// <param name="pSqlDataReader"></param>
        /// <param name="pSqlQuery">Recordet to get value</param>
        public override void GetValueFrom(DbDataReader pSqlDataReader)
        {
            if (!ReferenceEquals(pSqlDataReader, null))
            {
                this.vIsModified = false;
                this.vIsNull = (pSqlDataReader[this.FieldName] == DBNull.Value);
                if (!this.IsNull)
                {
                    DateTime.TryParse(pSqlDataReader[this.FieldName].ToString(), out this.vValue);
                }
            }
            else
            {
                throw new Exception("Sql Parameters not assigned");
            }
        }
        #endregion

        #region Sets
        /// <summary>
        ///  set the field from float
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsFloat(double pValue)
        {
            this.SetAsDatetime(DateTime.FromOADate(pValue));
        }
        /// <summary>
        ///  set the field from string
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected override void SetAsString(string pValue)
        {
            DateTime vResult;

            if (DateTime.TryParse(pValue, out vResult))
            {
                this.SetAsDatetime(vResult);
            }
        }
        /// <summary>
        ///  set the field from datetime
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsDatetime(DateTime pValue)
        {
            this.vOldValue = this.vValue;
            this.vValue = pValue;
            this.IsNull = false;
        }

        /// <summary>
        ///  Set the field value into a Query
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        /// <param name="pParameterName">Parameter name to assign</param>
        public override void SetValueTo(DbParameterCollection pSqlParameters, string pParameterName)
        {
            if (!ReferenceEquals(pSqlParameters, null))
            {
                //check is value is null
                SqlParameter vParameter;
                if (this.IsNull)
                {
                    vParameter = new SqlParameter(pParameterName, DBNull.Value);
                }
                else
                {
                    vParameter = new SqlParameter(pParameterName, SqlDbType.DateTime)
                    {
                        Value = DateTime.Parse(this.vValue.ToString("G"))
                    };
                }
                pSqlParameters.Add(vParameter);
            }
            else
            {
                throw new Exception("Sql Parameters not assigned");
            }
        }
        /// <summary>
        ///  Set the field into a DataRow
        /// </summary>
        /// <param name="pRow">Row to set info</param>
        public override void SetValueTo(ref DataRow pRow)
        {
            if (pRow.Table.Columns.IndexOf(base.FieldName) >= 0 && !this.IsNull)
                pRow[base.FieldName] = this.vValue;
        }
        #endregion

        //override functions

        #region InitializeValue
        /// <summary>
        /// Initialize Value
        /// </summary>
        protected override void InitializeValue()
        {
            this.vValue = DateTime.MinValue;
            this.vOldValue = DateTime.MinValue;
        }
        #endregion

        #region ToString
        /// <summary>
        /// Return value as string
        /// </summary>
        /// <returns>return value</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
        #endregion

       
    }
}