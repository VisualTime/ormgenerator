using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Robotics.DataLayer.ORM.Field
{ 
    /// <summary>
    ///  Specific class that represented one byte field in database
    /// </summary>
    public class ORMByteField : ORMField
    {
        #region variables
        //variables
        private readonly byte vMinRange;
        private readonly byte vMaxRange;
        private byte vMinValue = 0;
        private byte vMaxValue = 0;
        private byte vValue;
        private byte vOldValue;
        #endregion

        #region Property
        /// <summary>
        ///  get or set the original field
        /// </summary>
        public byte Value
        {
            get { return this.GetAsbyte(); }
            set { this.SetAsbyte(value); }
        }
        /// <summary>
        ///  get or set the old value field
        /// </summary>
        public byte OldValue
        {
            get { return this.vOldValue; }
        }
        /// <summary>
        ///  get or set max value
        /// </summary>
        public byte MaxValue
        {
            get { return this.vMaxValue; }
            set { this.SetMaxValue(value); }
        }
        /// <summary>
        ///  get or set min value
        /// </summary>
        public byte MinValue
        {
            get { return this.vMinValue; }
            set { this.SetMinValue(value); }
        }
        /// <summary>
        ///  get or set the value as integer
        /// </summary>
        public int AsInteger
        {
            get { return this.GetAsInteger(); }
            set { this.SetAsInteger(value); }
        }
        /// <summary>
        ///  get or set the value as integer
        /// </summary>
        public uint AsUnInteger
        {
            get { return this.GetAsUnInteger(); }
            set { this.SetAsUnInteger(value); }
        }
        /// <summary>
        ///  get or set the value as small integer
        /// </summary>
        public short ASmallInteger
        {
            get { return this.GetAsSmallInteger(); }
            set { this.SetAsSmallInteger(value); }
        }
        /// <summary>
        ///  get or set the value as big integer
        /// </summary>
        public long AsBigInteger
        {
            get { return this.GetAsBigInteger(); }
            set { this.SetAsBigInteger(value); }
        }
        /// <summary>
        ///  get or set the value as byte
        /// </summary>
        public byte Asbyte
        {
            get { return this.GetAsbyte(); }
            set { this.SetAsbyte(value); }
        }
        /// <summary>
        ///  get or set the value as float
        /// </summary>
        public double AsFloat
        {
            get { return this.GetAsFloat(); }
            set { this.SetAsFloat(value); }
        }
        /// <summary>
        /// Get as object
        /// </summary>
        public override object RawValue => this.Value;
        #endregion

        #region Constructor
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pTableName">Table where the field is added</param>
        public ORMByteField(string pFieldName, string pTableName)
            : base(pFieldName, pTableName)
        {
            this.vMinRange = byte.MinValue;
            this.vMaxRange = byte.MaxValue;
            this.vMinValue = this.vMinRange;
            this.vMaxValue = this.vMaxRange;
            this.vOldValue = 0;
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        public ORMByteField(string pFieldName)
            : this(pFieldName, "")
        {
        }
        #endregion

        //protected functions

        #region Gets
        /// <summary>
        ///  get the byte
        /// </summary>
        /// <returns>byte</returns>
        protected byte GetAsbyte()
        {
            return (byte) (!this.IsNull ? this.vValue : 0);
        }

        /// <summary>
        ///  get the integer
        /// </summary>
        /// <returns>integer</returns>
        protected int GetAsInteger()
        {
            return this.GetAsbyte();
        }
        /// <summary>
        ///  get the unsigned integer
        /// </summary>
        /// <returns>integer</returns>
        protected uint GetAsUnInteger()
        {
            return this.GetAsbyte();
        }
        /// <summary>
        ///  get the small integer
        /// </summary>
        /// <returns>integer</returns>
        protected short GetAsSmallInteger()
        {
            return this.GetAsbyte();
        }
        /// <summary>
        ///  get the big integer
        /// </summary>
        /// <returns>integer</returns>
        protected long GetAsBigInteger()
        {
            return this.GetAsbyte();
        }
        /// <summary>
        ///  get the float
        /// </summary>
        /// <returns>double</returns>
        protected double GetAsFloat()
        {
            return this.GetAsbyte();
        }
        /// <summary>
        ///  get the string
        /// </summary>
        /// <returns>string</returns>
        protected override string GetAsString()
        {
            return !this.IsNull ? this.vValue.ToString() : "";
        }

        /// <summary>
        ///  get size of type
        /// </summary>
        /// <returns>size of byte</returns>
        protected int GetDataSize()
        {
            return sizeof(byte);
        }

        /// <summary>
        ///  initialize the integer value from one Recordet BDBD
        /// </summary>
        /// <param name="pSqlDataReader"></param>
        /// <param name="pSqlQuery">Recordet to get value</param>
        public override void GetValueFrom(DbDataReader pSqlDataReader)
        {
            string vNumber;
            bool vIsNumber;
            double vAux;

            if (!ReferenceEquals(pSqlDataReader, null))
            {
                this.vIsModified = false;
                this.vIsNull = (pSqlDataReader[this.FieldName] == DBNull.Value);
                if (!this.IsNull)
                {
                    if (pSqlDataReader[this.FieldName] is bool)
                    {
                        this.vValue = (byte) (pSqlDataReader[this.FieldName].ToString().Equals("True") ? 1 : 0);
                    }
                    else
                    {
                        vNumber = pSqlDataReader[this.FieldName].ToString();
                        vIsNumber = byte.TryParse(vNumber, out this.vValue);
                        //check if parse is correct
                        if (!vIsNumber)
                        {
                            //value has decimals (e.g. AVG operations), convert without decimals
                            if (double.TryParse(vNumber, out vAux))
                            {
                                byte.TryParse(Math.Round(vAux, MidpointRounding.ToEven).ToString(), out this.vValue);
                            }
                        }
                    }
                }
            }
            else
            {
                throw new Exception("Sql Parameters not assigned");
            }
        }
        #endregion

        #region Sets
        /// <summary>
        ///  set the field as integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsbyte(byte pValue)
        {
            //check range
            this.CheckRange(pValue, this.vMinValue, this.vMaxValue);

            // load the new value and keep the old value
            this.vOldValue = this.vValue;
            this.vValue = pValue;
            //the values is not null
            this.IsNull = false;
        }
        /// <summary>
        ///  set the field as integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsInteger(int pValue)
        {
            byte vResult;
            if (byte.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsbyte(vResult);
            }
        }
        /// <summary>
        ///  set the field as unsigned integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsUnInteger(uint pValue)
        {
            byte vResult;
            if (byte.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsbyte(vResult);
            }
        }
        /// <summary>
        ///  set the field as unsigned integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsSmallInteger(short pValue)
        {
            byte vResult;
            if (byte.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsbyte(vResult);
            }
        }
        /// <summary>
        ///  set the field as unsigned integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsBigInteger(long pValue)
        {
            byte vResult;
            if (byte.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsbyte(vResult);
            }
        }
        /// <summary>
        ///  set the field from double number
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsFloat(double pValue)
        {
            byte vResult;
            if (byte.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsbyte(vResult);
            }
        }
        /// <summary>
        ///  set the field from string
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected override void SetAsString(string pValue)
        {
            byte vResult;
            if (byte.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsbyte(vResult);
            }
        }

        /// <summary>
        ///  Set the field value into a Query
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        /// <param name="pParamName"></param>
        /// <param name="pParameterName">Parameter name to assign</param>
        public override void SetValueTo(DbParameterCollection pSqlParameters, string pParamName)
        {
            SqlParameter vParameter;

            if (!ReferenceEquals(pSqlParameters, null))
            {
                //check is value is null
                vParameter = this.IsNull
                    ? new SqlParameter(pParamName, DBNull.Value)
                    : new SqlParameter(pParamName, SqlDbType.TinyInt) {Value = this.vValue};
                pSqlParameters.Add(vParameter);
            }
            else
            {
                throw new Exception("Sql Parameters not assigned");
            }
        }
        /// <summary>
        ///  Set the field into a DataRow
        /// </summary>
        /// <param name="pRow">Row to set info</param>
        public override void  SetValueTo(ref DataRow pRow)
        {
            if (pRow.Table.Columns.IndexOf(base.FieldName) >= 0 && !this.IsNull)
                pRow[base.FieldName] = this.vValue;
        }
        #endregion

        //private functions

        #region Range
        /// <summary>
        ///  Check the field value is between the minimum and maximum value.
        /// </summary>
        /// <param name="pValue">Field value to check</param>
        /// <param name="pMin">Minimum value for the field value</param>
        /// <param name="pMax">Maximum value for the field value</param>
        private void CheckRange(byte pValue, byte pMin, byte pMax)
        {
            //check that if int and check valid range
            if (pValue < pMin || pValue > pMax)
                throw new Exception("Check range error");
        }
        /// <summary>
        ///  Sets the maximum value for the field value
        /// </summary>
        /// <param name="pValue">Maximum value</param>
        private void SetMaxValue(byte pValue)
        {
            //check range
            this.CheckRange(pValue, this.vMinRange, this.vMaxRange);
            this.vMaxValue = pValue;
        }
        /// <summary>
        ///  Sets the minimum value for the field value
        /// </summary>
        /// <param name="pValue">Minimum value</param>
        private void SetMinValue(byte pValue)
        {
            //check and assign
            this.CheckRange(pValue, this.vMinRange, this.vMaxRange);
            this.vMinValue = pValue;
        }
        #endregion

        //override functions

        #region InitializeValue
        /// <summary>
        /// Initialize Value
        /// </summary>
        protected override void InitializeValue()
        {
            this.vValue = 0;
            this.vOldValue = 0;
        }
        #endregion

        #region ToString
        /// <summary>
        /// Return value as string
        /// </summary>
        /// <returns>return value</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
        #endregion

        
    }
}