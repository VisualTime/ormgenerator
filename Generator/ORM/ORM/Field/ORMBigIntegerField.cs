using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Robotics.DataLayer.ORM.Field
{
    /// <summary>
    ///  Specific class that represented one integer field in database
    /// </summary>
    public class ORMBigIntegerField : ORMField
    {
        #region variables
        //variables
        private readonly long vMinRange;
        private readonly long vMaxRange;
        private long vMinValue = 0;
        private long vMaxValue = 0;
        private long vValue;
        private long vOldValue;
        #endregion

        #region Property
        /// <summary>
        ///  get or set the original field
        /// </summary>
        public long Value
        {
            get { return this.GetAsBigInteger(); }
            set { this.SetAsBigInteger(value); }
        }
        /// <summary>
        ///  get or set the old value field
        /// </summary>
        public long OldValue
        {
            get { return this.vOldValue; }
        }
        /// <summary>
        ///  get or set max value
        /// </summary>
        public long MaxValue
        {
            get { return this.vMaxValue; }
            set { this.SetMaxValue(value); }
        }
        /// <summary>
        ///  get or set min value
        /// </summary>
        public long MinValue
        {
            get { return this.vMinValue; }
            set { this.SetMinValue(value); }
        }
        /// <summary>
        ///  get or set the value as integer
        /// </summary>
        public int AsInteger
        {
            get { return this.GetAsInteger(); }
            set { this.SetAsInteger(value); }
        }
		/// <summary>
        ///  get or set the value as integer
        /// </summary>
        public long AsBigInteger
        {
            get { return this.GetAsBigInteger(); }
            set { this.SetAsBigInteger(value); }
        }
        /// <summary>
        ///  get or set the value as integer
        /// </summary>
        public uint AsUnInteger
        {
            get { return this.GetAsUnInteger(); }
            set { this.SetAsUnInteger(value); }
        }
        /// <summary>
        ///  get or set the value as byte
        /// </summary>
        public byte AsByte
        {
            get { return this.GetAsByte(); }
            set { this.SetAsByte(value); }
        }
        /// <summary>
        ///  get or set the value as float
        /// </summary>
        public double AsFloat
        {
            get { return this.GetAsFloat(); }
            set { this.SetAsFloat(value); }
        }
		/// <summary>
        ///  get or set the value as float
        /// </summary>
        public decimal AsDecimal
        {
            get { return this.GetAsDecimal(); }
            set { this.SetAsDecimal(value); }
        }
		/// <summary>
        ///  get as object
        /// </summary>
		public override object RawValue => this.Value;

        #endregion

        #region Constructor
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pTableName">Table where the field is added</param>
        public ORMBigIntegerField(string pFieldName, string pTableName)
            : base(pFieldName, pTableName)
        {
            this.vMinRange = long.MinValue;
            this.vMaxRange = long.MaxValue;
            this.vMinValue = this.vMinRange;
            this.vMaxValue = this.vMaxRange;
            this.vOldValue = 0;
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        public ORMBigIntegerField(string pFieldName)
            : this(pFieldName, "")
        {
        }
        #endregion

        //protected functions

        #region Gets
        /// <summary>
        ///  get the integer
        /// </summary>
        /// <returns>integer</returns>
        protected int GetAsInteger()
        {
            var vResult = 0;
            if (!this.IsNull)
            {
                int.TryParse(this.vValue.ToString(), out vResult);
            }

            return vResult;
        }
        /// <summary>
        ///  get the byte
        /// </summary>
        /// <returns>byte</returns>
        protected byte GetAsByte()
        {
			byte vResult=0;
            if (!this.IsNull)
            {
                byte.TryParse(this.vValue.ToString(), out vResult);
            }

            return vResult;
        }
        /// <summary>
        ///  get the uninteger
        /// </summary>
        /// <returns>integer</returns>
        protected uint GetAsUnInteger()
        {
			UInt32 vResult=0;
            if (!this.IsNull)
            {
                UInt32.TryParse(this.vValue.ToString(), out vResult);
            }

            return vResult;
        }
		/// <summary>
        ///  get the small integer
        /// </summary>
        /// <returns>small integer</returns>
        protected short GetAsSmallInteger()
        {
            short vResult = 0;
            if (!this.IsNull)
            {
                short.TryParse(this.vValue.ToString(), out vResult);
            }

            return vResult;
        }
		/// <summary>
        ///  get the small integer
        /// </summary>
        /// <returns>small integer</returns>
        protected long GetAsBigInteger()
        {
			return !this.IsNull ? this.vValue : 0;
        }
        /// <summary>
        ///  get the float
        /// </summary>
        /// <returns>double</returns>
        protected double GetAsFloat()
        {
			return !this.IsNull ? this.vValue : 0;
        }
		/// <summary>
        ///  get the decimal
        /// </summary>
        /// <returns>decimal</returns>
        protected decimal GetAsDecimal()
        {
			return !this.IsNull ? this.vValue : 0;
        }
        /// <summary>
        ///  get the string
        /// </summary>
        /// <returns>string</returns>
        protected override string GetAsString()
        {
			return !this.IsNull ? this.vValue.ToString(): string.Empty;
        }
        /// <summary>
        ///  get size of type
        /// </summary>
        /// <returns>size of int</returns>
        protected int GetDataSize()
        {
            return sizeof(long);
        }

        /// <summary>
        ///  initialize the integer value from one Recordet BDBD
        /// </summary>
        /// <param name="pSqlDataReader"></param>
        /// <param name="pSqlQuery">Recordet to get value</param>
        public override void GetValueFrom(DbDataReader pSqlDataReader)
        {
            string vNumber;
			double vAux;
			bool vIsNumber;

            if (!ReferenceEquals(pSqlDataReader, null))
            {
                this.vIsModified = false;
                this.vIsNull = (pSqlDataReader[this.FieldName] == DBNull.Value);
                if (!this.IsNull)
                {
                    if (pSqlDataReader[this.FieldName] is bool)
                    {
                        this.vValue = pSqlDataReader[this.FieldName].ToString().Equals("True") ? 1 : 0;
                    }
                    else
                    {
                        vNumber = pSqlDataReader[this.FieldName].ToString();
						vIsNumber = long.TryParse(vNumber, out this.vValue);
						//check if parse is correct
						if (!vIsNumber)
						{
							//value has decimals (e.g. AVG operations), convert without decimals
							if (double.TryParse(vNumber,out vAux))
							{
                                long.TryParse(Math.Round(vAux, MidpointRounding.ToEven).ToString(), out this.vValue);
							}
						}
                    }
                }
            }
            else
			{
                throw new Exception("Sql Parameters not assigned");
			}
        }
        #endregion

        #region Sets
        /// <summary>
        ///  set the field as integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsInteger(int pValue)
        {
            long vResult;
            if (long.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsBigInteger(vResult);
            }
        }
        /// <summary>
        ///  set the field as integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsByte(byte pValue)
        {
            long vResult;
            if (long.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsBigInteger(vResult);
            }
        }
        /// <summary>
        ///  set the field as unsigned integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsUnInteger(uint pValue)
        {
            long vResult;
            if (long.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsBigInteger(vResult);
            }
        }
		/// <summary>
        ///  set the field as small integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsSmallInteger(short pValue)
        {
            long vResult;
            if (long.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsBigInteger(vResult);
            }
        }
		/// <summary>
        ///  set the field as big integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsBigInteger(long pValue)
        {
            //check range
            this.CheckRange(pValue, this.vMinValue, this.vMaxValue);

            // load the new value and keep the old value
            this.vOldValue = this.vValue;
            this.vValue = pValue;
            //the values is not null
            this.IsNull = false;
        }
        /// <summary>
        ///  set the field from double number
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsFloat(double pValue)
        {
            long vResult;
            if (long.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsBigInteger(vResult);
            }
        }
		/// <summary>
        ///  set the field from decimal number
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsDecimal(decimal pValue)
        {
            long vResult;
            if (long.TryParse(pValue.ToString(), out vResult))
			{
                this.SetAsBigInteger(vResult);
			}
        }
        /// <summary>
        ///  set the field from string
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected override void SetAsString(string pValue)
        {
            long vResult;
			if (long.TryParse(pValue, out vResult))
			{			
				this.SetAsBigInteger(vResult);
			}
        }

        /// <summary>
        ///  Set the field value into a Query
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        /// <param name="pParamName"></param>
        /// <param name="pParameterName">Parameter name to assign</param>
        public override void SetValueTo(DbParameterCollection pSqlParameters, string pParamName)
        {
            SqlParameter vParameter;

            if (!ReferenceEquals(pSqlParameters, null))
            {
                //check is value is null
                vParameter = this.IsNull ? new SqlParameter(pParamName, DBNull.Value) : new SqlParameter(pParamName, SqlDbType.BigInt) {Value = this.vValue};
                pSqlParameters.Add(vParameter);
            }
            else
			{
                throw new Exception("Sql Parameters not assigned");
			}
        }
        /// <summary>
        ///  Set the field into a DataRow
        /// </summary>
        /// <param name="pRow">Row to set info</param>
        public override void SetValueTo(ref DataRow pRow)
        {
            if (pRow.Table.Columns.IndexOf(base.FieldName) >= 0 && !this.IsNull)
			{
                pRow[base.FieldName] = this.vValue;
			}
        }
        #endregion

        //private functions

        #region Range
		/// <summary>
        ///  Check the field value is between the minimum and maximum value.
        /// </summary>
        /// <param name="pValue">Field value to check</param>
        /// <param name="pMin">Minimum value for the field value</param>
        /// <param name="pMax">Maximum value for the field value</param>
        private void CheckRange(long pValue, long pMin, long pMax)
        {
            if (pValue < pMin || pValue > pMax)
			{
                throw new Exception("Check range error");
			}
        }
        /// <summary>
        ///  Sets the maximum value for the field value
        /// </summary>
        /// <param name="pValue">Maximum value</param>
        private void SetMaxValue(long pValue)
        {
            //check range
            this.CheckRange(pValue, this.vMinRange, this.vMaxRange);
            this.vMaxValue = pValue;
        }
        /// <summary>
        ///  Sets the minimum value for the field value
        /// </summary>
        /// <param name="pValue">Minimum value</param>
        private void SetMinValue(long pValue)
        {
            //check and assign
            this.CheckRange(pValue, this.vMinRange, this.vMaxRange);
            this.vMinValue = pValue;
        }
        #endregion

        //override functions

        #region InitializeValue
        /// <summary>
        /// Initialize Value
        /// </summary>
        protected override void InitializeValue()
        {
            this.vValue = 0;
            this.vOldValue = 0;
        }
        #endregion

        #region ToString
        /// <summary>
        /// Return value as string
        /// </summary>
        /// <returns>return value</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
        #endregion
    }
}