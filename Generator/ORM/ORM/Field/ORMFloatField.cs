using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Robotics.DataLayer.ORM.Field
{
    /// <summary>
    ///  Specific class that represented one float field in database (float with high precision)
    /// </summary>
    public class ORMFloatField : ORMField
    {
        #region Variables
        private readonly double vMinRange;
        private readonly double vMaxRange;
        private double vMinValue = 0;
        private double vMaxValue = 0;
        private double vValue;
        private double vOldValue;
        #endregion

        #region Property
        /// <summary>
        ///  get or set the original field
        /// </summary>
        public double Value
        {
            get { return this.GetAsFloat(); }
            set { this.SetAsFloat(value); }
        }
        /// <summary>
        ///  get or set the old value field
        /// </summary>
        public double OldValue
        {
            get { return this.vOldValue; }
        }
        /// <summary>
        ///  get or set max value
        /// </summary>
        public double MaxValue
        {
            get { return this.vMaxValue; }
            set { this.SetMaxValue(value); }
        }
        /// <summary>
        ///  get or set min value
        /// </summary>
        public double MinValue
        {
            get { return this.vMinValue; }
            set { this.SetMinValue(value); }
        }
        /// <summary>
        ///  get or set the value as integer
        /// </summary>
        public int AsInteger
        {
            get { return this.GetAsInteger(); }
            set { this.SetAsInteger(value); }
        }
        /// <summary>
        ///  get or set the value as float
        /// </summary>
        public double AsFloat
        {
            get { return this.GetAsFloat(); }
            set { this.SetAsFloat(value); }
        }
        /// <summary>
        ///  get or set the value as float
        /// </summary>
        public decimal AsDecimal
        {
            get { return this.GetAsDecimal(); }
            set { this.SetAsDecimal(value); }
        }
		/// <summary>
        /// Return value as object
        /// </summary>
        /// <returns>return value</returns>
        public override object RawValue => this.Value;

        #endregion

        #region Constructor
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pTableName">Table where the field is added</param>
        public ORMFloatField(string pFieldName, string pTableName)
            : base(pFieldName, pTableName)
        {
            this.vMinRange = double.MinValue;
            this.vMaxRange = double.MaxValue;
            this.vMinValue = this.vMinRange;
            this.vMaxValue = this.vMaxRange;
            this.vOldValue = 0;
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        public ORMFloatField(string pFieldName)
            : this(pFieldName, "")
        {
        }
        #endregion

        //protected functions

        #region Gets
        /// <summary>
        ///  get the decimal
        /// </summary>
        /// <returns>double</returns>
        protected decimal GetAsDecimal()
        {
            decimal vResult=0;
            if (!this.IsNull)
            {
                decimal.TryParse(this.vValue.ToString(), out vResult);
            }

            return vResult;
        }
        /// <summary>
        ///  get the float
        /// </summary>
        /// <returns>double</returns>
        protected double GetAsFloat()
        {
			return !this.IsNull ? this.vValue : 0;
        }
        /// <summary>
        ///  get the integer
        /// </summary>
        /// <returns>integer</returns>
        protected int GetAsInteger()
        {
			var vResult=0;
            if (!this.IsNull)
            {
                int.TryParse(this.vValue.ToString(), out vResult);
            }

            return vResult;
        }
		/// <summary>
        ///  get the small integer
        /// </summary>
        /// <returns>integer</returns>
        protected int GetAsSmallInteger()
        {
			short vResult=0;
            if (!this.IsNull)
            {
                short.TryParse(this.vValue.ToString(), out vResult);
            }

            return vResult;
        }
		/// <summary>
        ///  get the big integer
        /// </summary>
        /// <returns>integer</returns>
        protected long GetAsBigInteger()
        {
			long vResult=0;
            if (!this.IsNull)
            {
                long.TryParse(this.vValue.ToString(), out vResult);
            }

            return vResult;
        }
        /// <summary>
        ///  get the string
        /// </summary>
        /// <returns>string</returns>
        protected override string GetAsString()
        {
			return !this.IsNull ? this.vValue.ToString() : string.Empty;
        }

        /// <summary>
        ///  initialize the integer value from one Recordet BDBD
        /// </summary>
        /// <param name="pSqlDataReader"></param>
        /// <param name="pSqlQuery">Recordet to get value</param>
        public override void GetValueFrom(DbDataReader pSqlDataReader)
        {
            if (!ReferenceEquals(pSqlDataReader, null))
            {
                this.vIsModified = false;
                this.vIsNull = (pSqlDataReader[this.FieldName] == DBNull.Value);
                if (!this.IsNull)
				{
                    double.TryParse(pSqlDataReader[this.FieldName].ToString(), out this.vValue);
				}
            }
            else
			{
                throw new Exception("Sql Parameters not assigned");
			}
        }
        #endregion

        #region Sets
        /// <summary>
        ///  set the field from double number
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsFloat(double pValue)
        {
            //check range
            this.CheckRange(pValue, this.vMinValue, this.vMaxValue);

            // load the new value and keep the old value
            this.vOldValue = this.vValue;
            this.vValue = pValue;
            //the values is not null
            this.IsNull = false;
        }
        /// <summary>
        ///  set the field as decimal
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsDecimal(decimal pValue)
        {
            double vResult;
            if (double.TryParse(pValue.ToString(), out vResult))
			{
                this.SetAsFloat(vResult);
			}
        }
        /// <summary>
        ///  set the field as integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsInteger(int pValue)
        {
            double vResult;
            if (double.TryParse(pValue.ToString(), out vResult))
			{
                this.SetAsFloat(vResult);
			}
        }
		/// <summary>
        ///  set the field as small integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsSmallInteger(short pValue)
        {
            double vResult;
            if (double.TryParse(pValue.ToString(), out vResult))
			{
                this.SetAsFloat(vResult);
			}
        }
		 /// <summary>
        ///  set the field as big integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsBigInteger(long pValue)
        {
            double vResult;
            if (double.TryParse(pValue.ToString(), out vResult))
			{
                this.SetAsFloat(vResult);
			}
        }
        /// <summary>
        ///  set the field from string
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected override void SetAsString(string pValue)
        {
            double vResult;
			if (double.TryParse(pValue, out vResult))
			{
				this.SetAsFloat(vResult);
			}
        }

        /// <summary>
        ///  Set the field value into a Query
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        /// <param name="pParamName"></param>
        /// <param name="pParameterName">Parameter name to assign</param>
        public override void SetValueTo(DbParameterCollection pSqlParameters, string pParamName)
        {
            SqlParameter vParameter;

            if (!ReferenceEquals(pSqlParameters, null))
            {
                //check is value is null
                vParameter = this.IsNull
                    ? new SqlParameter(pParamName, DBNull.Value)
                    : new SqlParameter(pParamName, SqlDbType.Float) {Value = this.vValue};
                pSqlParameters.Add(vParameter);
            }
            else
			{
                throw new Exception("Sql Parameters not assigned");
			}
        }
        /// <summary>
        ///  Set the field into a DataRow
        /// </summary>
        /// <param name="pRow">Row to set info</param>
        public override void SetValueTo(ref DataRow pRow)
        {
            if (pRow.Table.Columns.IndexOf(base.FieldName) >= 0 && !this.IsNull)
			{
                pRow[base.FieldName] = this.vValue;
			}
        }
        #endregion

        #region Range
        /// <summary>
        ///  Check the field value is between the minimum and maximum value.
        /// </summary>
        /// <param name="pValue">Field value to check</param>
        /// <param name="pMin">Minimum value for the field value</param>
        /// <param name="pMax">Maximum value for the field value</param>
        private void CheckRange(double pValue, double pMin, double pMax)
        {
            if (pValue < pMin || pValue > pMax)
			{
                throw new Exception("Check range error");
			}
        }
        /// <summary>
        ///  Sets the maximum value for the field value
        /// </summary>
        /// <param name="pValue">Maximum value</param>
        private void SetMaxValue(double pValue)
        {
            //check range
            this.CheckRange(pValue, this.vMinRange, this.vMaxRange);
            this.vMaxValue = pValue;
        }
        /// <summary>
        ///  Sets the minimum value for the field value
        /// </summary>
        /// <param name="pValue">Minimum value</param>
        private void SetMinValue(double pValue)
        {
            //check and assign
            this.CheckRange(pValue, this.vMinRange, this.vMaxRange);
            this.vMinValue = pValue;
        }
        #endregion

        //override functions

        #region InitializeValue
        /// <summary>
        /// Initialize Value
        /// </summary>
        protected override void InitializeValue()
        {
            this.vValue =0;
            this.vOldValue = 0;
        }
        #endregion

        #region ToString
        /// <summary>
        /// Return value as string
        /// </summary>
        /// <returns>return value</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
        #endregion
    }
}