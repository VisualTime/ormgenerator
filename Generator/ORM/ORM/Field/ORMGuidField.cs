using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Robotics.DataLayer.ORM.Field
{
    /// <summary>
    ///  Specific class that represented one string field in database
    /// </summary>
    public class ORMGuidField : ORMField
    {
        #region variables
        //variables
        private Guid vValue;
        private Guid vOldValue;
        private int vSize;
        #endregion

        #region Property
        /// <summary>
        ///  get or set the original field
        /// </summary>
        public Guid Value
        {
            get { return this.GetAsGuid(); }
            set { this.SetAsGuid(value); }
        }
        /// <summary>
        ///  get or set the old value field
        /// </summary>
        public Guid OldValue
        {
            get { return this.vOldValue; }
        }
        /// <summary>
        ///  get or set the value as boolean
        /// </summary>
        public Guid AsGuid
        {
            get { return this.GetAsGuid(); }
            set { this.SetAsGuid(value); }
        }
        /// <summary>
        ///  Size of string
        /// </summary>
        public int Size
        {
            get { return this.vSize; }
            set { this.SetSize(value); }
        }
		/// <summary>
        ///  Get as Object
        /// </summary>
		public override object RawValue => this.Value;

        #endregion

        #region Constructor
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pTableName">Table where the field is added</param>
        /// <param name="pSize">Size of String</param>
        public ORMGuidField(string pFieldName, string pTableName, int pSize)
            : base(pFieldName, pTableName)
        {
            this.vOldValue = Guid.Empty;
            this.vSize = pSize;
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pTableName">Table where the field is added</param>
        public ORMGuidField(string pFieldName, string pTableName)
            : this(pFieldName, pTableName, 255)
        {
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pSize">Size of String</param>
        public ORMGuidField(string pFieldName, int pSize)
            : this(pFieldName, "", pSize)
        {
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        public ORMGuidField(string pFieldName)
            : this(pFieldName, "", 255)
        {
        }
        #endregion

        //private functions

        #region SetSize
        /// <summary>
        ///  set Size of string
        /// </summary>
        /// <param name="pSize">new size to set</param>
        private void SetSize(int pSize)
        {
            this.vSize = pSize;
        }
        #endregion

        //protected functions

        #region Gets
        /// <summary>
        ///  get the string
        /// </summary>
        /// <returns>string</returns>
        protected Guid GetAsGuid()
        {
			return !this.IsNull ?  this.vValue : Guid.Empty;
        }
        /// <summary>
        ///  get size of type
        /// </summary>
        /// <returns>size of int</returns>
        protected int GetDataSize()
        {
            return this.vValue.ToString().Length;
        }

        /// <summary>
        ///  initialize the integer value from one Recordet BDBD
        /// </summary>
        /// <param name="pSqlDataReader"></param>
        /// <param name="pSqlQuery">Recordet to get value</param>
        public override void GetValueFrom(DbDataReader pSqlDataReader)
        {
            if (!ReferenceEquals(pSqlDataReader, null))
            {
                this.vIsModified = false;
                this.vIsNull = (pSqlDataReader[this.FieldName] == DBNull.Value);
                if (!this.IsNull)
				{
					Guid.TryParse(pSqlDataReader[this.FieldName].ToString(), out this.vValue);
				}
            }
            else
			{
                throw new Exception("Sql Parameters not assigned");
			}
        }

        /// <summary>
        /// get as string
        /// </summary>
        /// <returns>return string</returns>
        protected override string GetAsString()
        {
            return !this.IsNull ? this.vValue.ToString() : Guid.Empty.ToString();
        }

        #endregion

        #region Sets
        /// <summary>
        ///  set the field from guid
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsGuid(Guid pValue)
        {
            this.vOldValue = this.vValue;
            this.vValue = pValue;
            this.IsNull = false;
        }
		/// <summary>
        ///  set the field from string
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected override void SetAsString(string pValue)
        {
			Guid vResult;
			if (Guid.TryParse(pValue, out vResult))
			{
				this.vOldValue = this.vValue;
				this.vValue = vResult;
				this.IsNull = false;	
			}
        }

        /// <summary>
        ///  Set the field value into a Query
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        /// <param name="pParameterName">Parameter name to assign</param>
        public override void SetValueTo(DbParameterCollection pSqlParameters, string pParameterName)
        {
            SqlParameter vParameter;

            if (!ReferenceEquals(pSqlParameters, null))
            {
                //check is value is null
                vParameter = this.IsNull
                    ? new SqlParameter(pParameterName, DBNull.Value)
                    : new SqlParameter(pParameterName, SqlDbType.UniqueIdentifier) {Value = this.vValue};
                pSqlParameters.Add(vParameter);
            }
            else
			{
                throw new Exception("Sql Parameters not assigned");
			}
        }
        /// <summary>
        ///  Set the field into a DataRow
        /// </summary>
        /// <param name="pRow">Row to set info</param>
        public override void SetValueTo(ref DataRow pRow)
        {
            if (pRow.Table.Columns.IndexOf(base.FieldName) >= 0 && !this.IsNull)
			{
                pRow[base.FieldName] = this.vValue;
			}
        }
        #endregion

        //override functions

        #region InitializeValue
        /// <summary>
        /// Initialize Value
        /// </summary>
        protected override void InitializeValue()
        {
            this.vValue = Guid.Empty;
            this.vOldValue = Guid.Empty;
        }
        #endregion

        #region ToString
        /// <summary>
        /// Return value as string
        /// </summary>
        /// <returns>return value</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
        #endregion
    }
}