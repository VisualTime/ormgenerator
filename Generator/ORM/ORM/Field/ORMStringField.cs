using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Robotics.DataLayer.ORM.Field
{
    /// <summary>
    ///  Specific class that represented one string field in database
    /// </summary>
    public class ORMStringField : ORMField
    {
        #region variables
        //variables
        private string vValue;
        private string vOldValue;
        private int vSize;
        #endregion

        #region Property
        /// <summary>
        ///  get or set the original field
        /// </summary>
        public string Value
        {
            get { return this.GetAsString(); }
            set { this.SetAsString(value); }
        }
        /// <summary>
        ///  get or set the old value field
        /// </summary>
        public string OldValue
        {
            get { return this.vOldValue; }
        }
        /// <summary>
        ///  get or set the value as integer
        /// </summary>
        public int AsInteger
        {
            get { return this.GetAsInteger(); }
            set { this.SetAsInteger(value); }
        }
        /// <summary>
        ///  get or set the value as float
        /// </summary>
        public double AsFloat
        {
            get { return this.GetAsFloat(); }
            set { this.SetAsFloat(value); }
        }
        /// <summary>
        ///  Size of string
        /// </summary>
        public int Size
        {
            get { return this.vSize; }
            set { this.SetSize(value); }
        }
        /// <summary>
        /// get as object
        /// </summary>
        public override object RawValue => this.Value;
        #endregion

        #region Constructor
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pTableName">Table where the field is added</param>
        /// <param name="pSize">Size of String</param>
        public ORMStringField(string pFieldName, string pTableName, int pSize)
            : base(pFieldName, pTableName)
        {
            this.vOldValue = "";
            this.vValue = "";
            this.vSize = pSize;
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pTableName">Table where the field is added</param>
        public ORMStringField(string pFieldName, string pTableName)
            : this(pFieldName, pTableName, 255)
        {
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pSize">Size of String</param>
        public ORMStringField(string pFieldName, int pSize)
            : this(pFieldName, "", pSize)
        {
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        public ORMStringField(string pFieldName)
            : this(pFieldName, "", 255)
        {
        }
        #endregion

        //private functions

        #region SetSize
        /// <summary>
        ///  set Size of string
        /// </summary>
        /// <param name="pSize">new size to set</param>
        private void SetSize(int pSize)
        {
            this.vSize = pSize;
        }
        #endregion

        //protected functions

        #region Gets
        /// <summary>
        ///  get the integer
        /// </summary>
        /// <returns>integer</returns>
        protected int GetAsInteger()
        {
            int vResult;

            return int.TryParse(this.vValue, out vResult) ? vResult : 0;
        }
        /// <summary>
        ///  get the float
        /// </summary>
        /// <returns>double</returns>
        protected double GetAsFloat()
        {
            double vResult;

            return double.TryParse(this.vValue, out vResult) ? vResult : 0;
        }
        /// <summary>
        ///  get the string
        /// </summary>
        /// <returns>string</returns>
        protected override string GetAsString()
        {
            return !this.IsNull ? this.vValue : "";
        }

        /// <summary>
        ///  get size of type
        /// </summary>
        /// <returns>size of int</returns>
        protected int GetDataSize()
        {
            return this.vValue.Length;
        }

        /// <summary>
        ///  initialize the integer value from one Recordet BDBD
        /// </summary>
        /// <param name="pSqlDataReader"></param>
        /// <param name="pSqlQuery">Recordet to get value</param>
        public override void GetValueFrom(DbDataReader pSqlDataReader)
        {
            if (!ReferenceEquals(pSqlDataReader, null))
            {
                this.vIsModified = false;
                this.vIsNull = (pSqlDataReader[this.FieldName] == DBNull.Value);
                if (!this.IsNull)
                {
                    this.vValue = pSqlDataReader[this.FieldName].ToString();
                }
            }
            else
            {
                throw new Exception("Sql Parameters not assigned");
            }
        }
        #endregion

        #region Sets
        /// <summary>
        ///  set the field as integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsInteger(int pValue)
        {
            this.SetAsString(pValue.ToString());
        }
        /// <summary>
        ///  set the field from double number
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsFloat(double pValue)
        {
            this.SetAsString(pValue.ToString());
        }
        /// <summary>
        ///  set the field from string
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected override void SetAsString(string pValue)
        {
            this.vOldValue = this.vValue;
            this.vValue = pValue;
            this.IsNull = false;
        }

        /// <summary>
        ///  Set the field value into a Query
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        /// <param name="pParameterName">Parameter name to assign</param>
        public override void SetValueTo(DbParameterCollection pSqlParameters, string pParameterName)
        {
            SqlParameter vParameter;

            if (!ReferenceEquals(pSqlParameters, null))
            {
                //check is value is null
                vParameter = this.IsNull
                    ? new SqlParameter(pParameterName, DBNull.Value)
                    : new SqlParameter(pParameterName, SqlDbType.NVarChar) {Value = this.vValue};
                pSqlParameters.Add(vParameter);
            }
            else
            {
                throw new Exception("Sql Parameters not assigned");
            }
        }
        /// <summary>
        ///  Set the field into a DataRow
        /// </summary>
        /// <param name="pRow">Row to set info</param>
        public override void SetValueTo(ref DataRow pRow)
        {
            if (pRow.Table.Columns.IndexOf(base.FieldName) >= 0 && !this.IsNull)
            {
                pRow[base.FieldName] = this.vValue;
            }
        }
        #endregion

        //override functions

        #region InitializeValue
        /// <summary>
        /// Initialize Value
        /// </summary>
        protected override void InitializeValue()
        {
            this.vValue = "";
            this.vOldValue = "";
        }
        #endregion

        #region ToString
        /// <summary>
        /// Return value as string
        /// </summary>
        /// <returns>return value</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
        #endregion
    }
}