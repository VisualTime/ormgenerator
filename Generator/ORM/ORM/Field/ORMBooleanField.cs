using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Robotics.DataLayer.ORM.Field
{
    /// <summary>
    ///  Specific class that represented one boolchar field in database
    /// </summary>
    public class ORMBooleanField : ORMField
    {
        #region variables
        //variables
        private bool vValue;
        private bool vOldValue;
        #endregion

        #region Property
        /// <summary>
        ///  get or set the original field
        /// </summary>
        public bool Value
        {
            get { return this.GetAsBoolean(); }
            set { this.SetAsBoolean(value); }
        }
        /// <summary>
        ///  get or set the old value field
        /// </summary>
        public bool OldValue
        {
            get { return this.vOldValue; }
        }
        /// <summary>
        ///  get or set the value as integer
        /// </summary>
        public int AsInteger
        {
            get { return this.GetAsInteger(); }
            set { this.SetAsInteger(value); }
        }
        /// <summary>
        ///  get or set the value as float
        /// </summary>
        public double AsFloat
        {
            get { return this.GetAsFloat(); }
            set { this.SetAsFloat(value); }
        }
        /// <summary>
        ///  get or set the value as boolean
        /// </summary>
        public bool AsBoolean
        {
            get { return this.GetAsBoolean(); }
            set { this.SetAsBoolean(value); }
        }
        /// <summary>
        /// get as object
        /// </summary>
        public override object RawValue => this.Value;
        #endregion

        #region Constructor
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        /// <param name="pTableName">Table where the field is added</param>
        public ORMBooleanField(string pFieldName, string pTableName)
            : base(pFieldName, pTableName)
        {
            this.vOldValue = false;
        }
        /// <summary>
        /// Creates the class to store the database field
        /// </summary>
        /// <param name="pFieldName">Database field name</param>
        public ORMBooleanField(string pFieldName)
            : this(pFieldName, "")
        {
        }
        #endregion

        //protected functions

        #region Gets
        /// <summary>
        ///  get the integer
        /// </summary>
        /// <returns>integer</returns>
        protected int GetAsInteger()
        {
            return !this.IsNull && (this.GetAsBoolean() == true) ? 1 : 0;
        }
        /// <summary>
        ///  get the float
        /// </summary>
        /// <returns>double</returns>
        protected double GetAsFloat()
        {
            return this.GetAsInteger();
        }
        /// <summary>
        ///  get the string
        /// </summary>
        /// <returns>string</returns>
        protected override string GetAsString()
        {
            return !this.IsNull && (this.GetAsBoolean() == true) ? "1" : "0";
        }
        /// <summary>
        ///  get the bool
        /// </summary>
        /// <returns>bool</returns>
        protected bool GetAsBoolean()
        {
            return (!this.IsNull && this.vValue);
        }

        /// <summary>
        ///  initialize the integer value from one Recordet BDBD
        /// </summary>
        /// <param name="pSqlDataReader"></param>
        /// <param name="pSqlQuery">Recordet to get value</param>
        public override void GetValueFrom(DbDataReader pSqlDataReader)
        {
            if (!ReferenceEquals(pSqlDataReader, null))
            {
                this.vIsModified = false;
                this.vIsNull = (pSqlDataReader[this.FieldName] == DBNull.Value);
                if (!this.IsNull)
                {
                    bool.TryParse(pSqlDataReader[this.FieldName].ToString(), out this.vValue);
                }
            }
            else
            {
                throw new Exception("Sql Parameters not assigned");
            }
        }
        #endregion

        #region Sets
        /// <summary>
        ///  set the field as integer
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsInteger(int pValue)
        {
            this.SetAsBoolean(pValue == 1);
        }

        /// <summary>
        ///  set the field from double number
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsFloat(double pValue)
        {
            int vResult;
            if (int.TryParse(pValue.ToString(), out vResult))
            {
                this.SetAsInteger(int.Parse(pValue.ToString()));
            }
        }
        /// <summary>
        ///  set the field from boolean
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected void SetAsBoolean(bool pValue)
        {
            //guardamos el antiguo, y asignamos el nuevo
            this.vOldValue = this.vValue;
            this.vValue = pValue;
            //campo no nulo
            this.IsNull = false;
        }
        /// <summary>
        ///  set the field from string
        /// </summary>
        /// <param name="pValue">the value to set</param>
        protected override void SetAsString(string pValue)
        {
            this.SetAsBoolean(pValue.Equals("1"));
        }

        /// <summary>
        ///  Set the field value into a Query
        /// </summary>
        /// <param name="pSqlParameters">Query to set the field value</param>
        /// <param name="pParameterName">Parameter name to assign</param>
        public override void SetValueTo(DbParameterCollection pSqlParameters, string pParameterName)
        {
            SqlParameter vParameter;

            if (!ReferenceEquals(pSqlParameters, null))
            {
                //check is value is null
                vParameter = this.IsNull
                    ? new SqlParameter(pParameterName, DBNull.Value)
                    : new SqlParameter(pParameterName, SqlDbType.Bit) {Value = this.vValue};
                pSqlParameters.Add(vParameter);
            }
            else
            {
                throw new Exception("Sql Parameters not assigned");
            }
        }
        /// <summary>
        ///  Set the field into a DataRow
        /// </summary>
        /// <param name="pRow">Row to set info</param>
        public override void SetValueTo(ref DataRow pRow)
        {
            if (pRow.Table.Columns.IndexOf(base.FieldName) >= 0 && !this.IsNull)
                pRow[base.FieldName] = this.vValue;
        }
        #endregion

        //override functions

        #region InitializeValue
        /// <summary>
        /// Initialize Value
        /// </summary>
        protected override void InitializeValue()
        {
            this.vValue = false;
            this.vOldValue = false;
        }
        #endregion

        #region ToString
        /// <summary>
        /// Return value as string
        /// </summary>
        /// <returns>return value</returns>
        public override string ToString()
        {
            return this.Value.ToString();
        }
        #endregion
    }
}