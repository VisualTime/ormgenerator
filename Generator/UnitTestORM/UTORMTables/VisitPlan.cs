using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMVisitPlan
	{
		private RandomField rnd;
		private ORMVisitPlanBase table;
		
		/// <summary>
		/// UT from table VisitPlan
		/// </summary>
		public UTORMVisitPlan()
		{
			rnd = new RandomField();
			table = new ORMVisitPlanBase();
		}

        public ORMVisitPlanRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTVisitPlanRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
