using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroFieldsTask
	{
		private RandomField rnd;
		private ORMsysroFieldsTaskBase table;
		
		/// <summary>
		/// UT from table sysroFieldsTask
		/// </summary>
		public UTORMsysroFieldsTask()
		{
			rnd = new RandomField();
			table = new ORMsysroFieldsTaskBase();
		}

        public ORMsysroFieldsTaskRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroFieldsTaskRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
