using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroShiftsCausesRules
	{
		private RandomField rnd;
		private ORMsysroShiftsCausesRulesBase table;
		
		/// <summary>
		/// UT from table sysroShiftsCausesRules
		/// </summary>
		public UTORMsysroShiftsCausesRules()
		{
			rnd = new RandomField();
			table = new ORMsysroShiftsCausesRulesBase();
		}

        public ORMsysroShiftsCausesRulesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroShiftsCausesRulesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
