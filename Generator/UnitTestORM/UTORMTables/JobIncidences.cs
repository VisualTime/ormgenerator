using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMJobIncidences
	{
		private RandomField rnd;
		private ORMJobIncidencesBase table;
		
		/// <summary>
		/// UT from table JobIncidences
		/// </summary>
		public UTORMJobIncidences()
		{
			rnd = new RandomField();
			table = new ORMJobIncidencesBase();
		}

        public ORMJobIncidencesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTJobIncidencesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
