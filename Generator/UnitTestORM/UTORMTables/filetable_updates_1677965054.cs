using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMfiletable_updates_1677965054
	{
		private RandomField rnd;
		private ORMfiletable_updates_1677965054Base table;
		
		/// <summary>
		/// UT from table filetable_updates_1677965054
		/// </summary>
		public UTORMfiletable_updates_1677965054()
		{
			rnd = new RandomField();
			table = new ORMfiletable_updates_1677965054Base();
		}

        public ORMfiletable_updates_1677965054Record RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTfiletable_updates_1677965054RandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
