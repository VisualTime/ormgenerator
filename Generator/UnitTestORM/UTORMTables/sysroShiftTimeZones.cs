using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroShiftTimeZones
	{
		private RandomField rnd;
		private ORMsysroShiftTimeZonesBase table;
		
		/// <summary>
		/// UT from table sysroShiftTimeZones
		/// </summary>
		public UTORMsysroShiftTimeZones()
		{
			rnd = new RandomField();
			table = new ORMsysroShiftTimeZonesBase();
		}

        public ORMsysroShiftTimeZonesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroShiftTimeZonesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
