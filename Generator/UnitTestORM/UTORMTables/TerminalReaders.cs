using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMTerminalReaders
	{
		private RandomField rnd;
		private ORMTerminalReadersBase table;
		
		/// <summary>
		/// UT from table TerminalReaders
		/// </summary>
		public UTORMTerminalReaders()
		{
			rnd = new RandomField();
			table = new ORMTerminalReadersBase();
		}

        public ORMTerminalReadersRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTTerminalReadersRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
