using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMJobIncidenceCategories
	{
		private RandomField rnd;
		private ORMJobIncidenceCategoriesBase table;
		
		/// <summary>
		/// UT from table JobIncidenceCategories
		/// </summary>
		public UTORMJobIncidenceCategories()
		{
			rnd = new RandomField();
			table = new ORMJobIncidenceCategoriesBase();
		}

        public ORMJobIncidenceCategoriesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTJobIncidenceCategoriesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
