using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMGuideExtractionFields
	{
		private RandomField rnd;
		private ORMGuideExtractionFieldsBase table;
		
		/// <summary>
		/// UT from table GuideExtractionFields
		/// </summary>
		public UTORMGuideExtractionFields()
		{
			rnd = new RandomField();
			table = new ORMGuideExtractionFieldsBase();
		}

        public ORMGuideExtractionFieldsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTGuideExtractionFieldsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
