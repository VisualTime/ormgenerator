using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMEntriesCaptures
	{
		private RandomField rnd;
		private ORMEntriesCapturesBase table;
		
		/// <summary>
		/// UT from table EntriesCaptures
		/// </summary>
		public UTORMEntriesCaptures()
		{
			rnd = new RandomField();
			table = new ORMEntriesCapturesBase();
		}

        public ORMEntriesCapturesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTEntriesCapturesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
