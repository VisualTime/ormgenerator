using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroScheduleTemplates_Detail
	{
		private RandomField rnd;
		private ORMsysroScheduleTemplates_DetailBase table;
		
		/// <summary>
		/// UT from table sysroScheduleTemplates_Detail
		/// </summary>
		public UTORMsysroScheduleTemplates_Detail()
		{
			rnd = new RandomField();
			table = new ORMsysroScheduleTemplates_DetailBase();
		}

        public ORMsysroScheduleTemplates_DetailRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroScheduleTemplates_DetailRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
