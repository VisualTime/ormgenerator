using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroSecurityNode_Groups
	{
		private RandomField rnd;
		private ORMsysroSecurityNode_GroupsBase table;
		
		/// <summary>
		/// UT from table sysroSecurityNode_Groups
		/// </summary>
		public UTORMsysroSecurityNode_Groups()
		{
			rnd = new RandomField();
			table = new ORMsysroSecurityNode_GroupsBase();
		}

        public ORMsysroSecurityNode_GroupsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroSecurityNode_GroupsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
