using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMAccrualsRules
	{
		private RandomField rnd;
		private ORMAccrualsRulesBase table;
		
		/// <summary>
		/// UT from table AccrualsRules
		/// </summary>
		public UTORMAccrualsRules()
		{
			rnd = new RandomField();
			table = new ORMAccrualsRulesBase();
		}

        public ORMAccrualsRulesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTAccrualsRulesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
