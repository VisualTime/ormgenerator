using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMPunchesCaptures
	{
		private RandomField rnd;
		private ORMPunchesCapturesBase table;
		
		/// <summary>
		/// UT from table PunchesCaptures
		/// </summary>
		public UTORMPunchesCaptures()
		{
			rnd = new RandomField();
			table = new ORMPunchesCapturesBase();
		}

        public ORMPunchesCapturesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTPunchesCapturesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
