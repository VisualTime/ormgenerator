using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroProductionFields
	{
		private RandomField rnd;
		private ORMsysroProductionFieldsBase table;
		
		/// <summary>
		/// UT from table sysroProductionFields
		/// </summary>
		public UTORMsysroProductionFields()
		{
			rnd = new RandomField();
			table = new ORMsysroProductionFieldsBase();
		}

        public ORMsysroProductionFieldsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroProductionFieldsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
