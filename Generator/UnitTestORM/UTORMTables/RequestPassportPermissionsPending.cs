using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMRequestPassportPermissionsPending
	{
		private RandomField rnd;
		private ORMRequestPassportPermissionsPendingBase table;
		
		/// <summary>
		/// UT from table RequestPassportPermissionsPending
		/// </summary>
		public UTORMRequestPassportPermissionsPending()
		{
			rnd = new RandomField();
			table = new ORMRequestPassportPermissionsPendingBase();
		}

        public ORMRequestPassportPermissionsPendingRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTRequestPassportPermissionsPendingRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
