using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroGUI_Actions
	{
		private RandomField rnd;
		private ORMsysroGUI_ActionsBase table;
		
		/// <summary>
		/// UT from table sysroGUI_Actions
		/// </summary>
		public UTORMsysroGUI_Actions()
		{
			rnd = new RandomField();
			table = new ORMsysroGUI_ActionsBase();
		}

        public ORMsysroGUI_ActionsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroGUI_ActionsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
