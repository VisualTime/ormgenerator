using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMVisit_Visitor_Punch
	{
		private RandomField rnd;
		private ORMVisit_Visitor_PunchBase table;
		
		/// <summary>
		/// UT from table Visit_Visitor_Punch
		/// </summary>
		public UTORMVisit_Visitor_Punch()
		{
			rnd = new RandomField();
			table = new ORMVisit_Visitor_PunchBase();
		}

        public ORMVisit_Visitor_PunchRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTVisit_Visitor_PunchRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
