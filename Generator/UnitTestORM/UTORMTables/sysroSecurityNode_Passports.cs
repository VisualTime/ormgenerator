using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroSecurityNode_Passports
	{
		private RandomField rnd;
		private ORMsysroSecurityNode_PassportsBase table;
		
		/// <summary>
		/// UT from table sysroSecurityNode_Passports
		/// </summary>
		public UTORMsysroSecurityNode_Passports()
		{
			rnd = new RandomField();
			table = new ORMsysroSecurityNode_PassportsBase();
		}

        public ORMsysroSecurityNode_PassportsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroSecurityNode_PassportsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
