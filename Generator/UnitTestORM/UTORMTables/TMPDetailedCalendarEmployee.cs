using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMTMPDetailedCalendarEmployee
	{
		private RandomField rnd;
		private ORMTMPDetailedCalendarEmployeeBase table;
		
		/// <summary>
		/// UT from table TMPDetailedCalendarEmployee
		/// </summary>
		public UTORMTMPDetailedCalendarEmployee()
		{
			rnd = new RandomField();
			table = new ORMTMPDetailedCalendarEmployeeBase();
		}

        public ORMTMPDetailedCalendarEmployeeRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTTMPDetailedCalendarEmployeeRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
