using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMTaskAssignments
	{
		private RandomField rnd;
		private ORMTaskAssignmentsBase table;
		
		/// <summary>
		/// UT from table TaskAssignments
		/// </summary>
		public UTORMTaskAssignments()
		{
			rnd = new RandomField();
			table = new ORMTaskAssignmentsBase();
		}

        public ORMTaskAssignmentsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTTaskAssignmentsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
