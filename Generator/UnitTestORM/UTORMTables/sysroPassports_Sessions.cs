using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroPassports_Sessions
	{
		private RandomField rnd;
		private ORMsysroPassports_SessionsBase table;
		
		/// <summary>
		/// UT from table sysroPassports_Sessions
		/// </summary>
		public UTORMsysroPassports_Sessions()
		{
			rnd = new RandomField();
			table = new ORMsysroPassports_SessionsBase();
		}

        public ORMsysroPassports_SessionsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroPassports_SessionsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
