using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMVisit_Fields
	{
		private RandomField rnd;
		private ORMVisit_FieldsBase table;
		
		/// <summary>
		/// UT from table Visit_Fields
		/// </summary>
		public UTORMVisit_Fields()
		{
			rnd = new RandomField();
			table = new ORMVisit_FieldsBase();
		}

        public ORMVisit_FieldsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTVisit_FieldsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
