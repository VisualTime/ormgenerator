using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMEmployeeCostCenters
	{
		private RandomField rnd;
		private ORMEmployeeCostCentersBase table;
		
		/// <summary>
		/// UT from table EmployeeCostCenters
		/// </summary>
		public UTORMEmployeeCostCenters()
		{
			rnd = new RandomField();
			table = new ORMEmployeeCostCentersBase();
		}

        public ORMEmployeeCostCentersRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTEmployeeCostCentersRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
