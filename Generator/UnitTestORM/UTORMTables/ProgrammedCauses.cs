using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMProgrammedCauses
	{
		private RandomField rnd;
		private ORMProgrammedCausesBase table;
		
		/// <summary>
		/// UT from table ProgrammedCauses
		/// </summary>
		public UTORMProgrammedCauses()
		{
			rnd = new RandomField();
			table = new ORMProgrammedCausesBase();
		}

        public ORMProgrammedCausesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTProgrammedCausesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
