using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroPassports_PermissionsOverFeatures
	{
		private RandomField rnd;
		private ORMsysroPassports_PermissionsOverFeaturesBase table;
		
		/// <summary>
		/// UT from table sysroPassports_PermissionsOverFeatures
		/// </summary>
		public UTORMsysroPassports_PermissionsOverFeatures()
		{
			rnd = new RandomField();
			table = new ORMsysroPassports_PermissionsOverFeaturesBase();
		}

        public ORMsysroPassports_PermissionsOverFeaturesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroPassports_PermissionsOverFeaturesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
