using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMTMP_DailyTaskAccruals_OLD
	{
		private RandomField rnd;
		private ORMTMP_DailyTaskAccruals_OLDBase table;
		
		/// <summary>
		/// UT from table TMP_DailyTaskAccruals_OLD
		/// </summary>
		public UTORMTMP_DailyTaskAccruals_OLD()
		{
			rnd = new RandomField();
			table = new ORMTMP_DailyTaskAccruals_OLDBase();
		}

        public ORMTMP_DailyTaskAccruals_OLDRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTTMP_DailyTaskAccruals_OLDRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
