using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroPassports_Centers
	{
		private RandomField rnd;
		private ORMsysroPassports_CentersBase table;
		
		/// <summary>
		/// UT from table sysroPassports_Centers
		/// </summary>
		public UTORMsysroPassports_Centers()
		{
			rnd = new RandomField();
			table = new ORMsysroPassports_CentersBase();
		}

        public ORMsysroPassports_CentersRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroPassports_CentersRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
