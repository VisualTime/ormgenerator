using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroSecurityNode_Passports_PermissionsOverEmployeesExceptions
	{
		private RandomField rnd;
		private ORMsysroSecurityNode_Passports_PermissionsOverEmployeesExceptionsBase table;
		
		/// <summary>
		/// UT from table sysroSecurityNode_Passports_PermissionsOverEmployeesExceptions
		/// </summary>
		public UTORMsysroSecurityNode_Passports_PermissionsOverEmployeesExceptions()
		{
			rnd = new RandomField();
			table = new ORMsysroSecurityNode_Passports_PermissionsOverEmployeesExceptionsBase();
		}

        public ORMsysroSecurityNode_Passports_PermissionsOverEmployeesExceptionsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroSecurityNode_Passports_PermissionsOverEmployeesExceptionsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
