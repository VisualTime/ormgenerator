using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMEmployeeUserFieldValues
	{
		private RandomField rnd;
		private ORMEmployeeUserFieldValuesBase table;
		
		/// <summary>
		/// UT from table EmployeeUserFieldValues
		/// </summary>
		public UTORMEmployeeUserFieldValues()
		{
			rnd = new RandomField();
			table = new ORMEmployeeUserFieldValuesBase();
		}

        public ORMEmployeeUserFieldValuesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTEmployeeUserFieldValuesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
