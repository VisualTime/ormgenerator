using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroPassports_AuthenticationMethods
	{
		private RandomField rnd;
		private ORMsysroPassports_AuthenticationMethodsBase table;
		
		/// <summary>
		/// UT from table sysroPassports_AuthenticationMethods
		/// </summary>
		public UTORMsysroPassports_AuthenticationMethods()
		{
			rnd = new RandomField();
			table = new ORMsysroPassports_AuthenticationMethodsBase();
		}

        public ORMsysroPassports_AuthenticationMethodsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroPassports_AuthenticationMethodsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
