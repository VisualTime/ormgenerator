using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMVisit_Visitor
	{
		private RandomField rnd;
		private ORMVisit_VisitorBase table;
		
		/// <summary>
		/// UT from table Visit_Visitor
		/// </summary>
		public UTORMVisit_Visitor()
		{
			rnd = new RandomField();
			table = new ORMVisit_VisitorBase();
		}

        public ORMVisit_VisitorRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTVisit_VisitorRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
