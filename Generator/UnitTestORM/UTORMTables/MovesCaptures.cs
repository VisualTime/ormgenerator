using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMMovesCaptures
	{
		private RandomField rnd;
		private ORMMovesCapturesBase table;
		
		/// <summary>
		/// UT from table MovesCaptures
		/// </summary>
		public UTORMMovesCaptures()
		{
			rnd = new RandomField();
			table = new ORMMovesCapturesBase();
		}

        public ORMMovesCapturesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTMovesCapturesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
