using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroGroupFeatures_PermissionsOverFeatures
	{
		private RandomField rnd;
		private ORMsysroGroupFeatures_PermissionsOverFeaturesBase table;
		
		/// <summary>
		/// UT from table sysroGroupFeatures_PermissionsOverFeatures
		/// </summary>
		public UTORMsysroGroupFeatures_PermissionsOverFeatures()
		{
			rnd = new RandomField();
			table = new ORMsysroGroupFeatures_PermissionsOverFeaturesBase();
		}

        public ORMsysroGroupFeatures_PermissionsOverFeaturesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroGroupFeatures_PermissionsOverFeaturesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
