using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMDailyAccruals
	{
		private RandomField rnd;
		private ORMDailyAccrualsBase table;
		
		/// <summary>
		/// UT from table DailyAccruals
		/// </summary>
		public UTORMDailyAccruals()
		{
			rnd = new RandomField();
			table = new ORMDailyAccrualsBase();
		}

        public ORMDailyAccrualsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTDailyAccrualsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
