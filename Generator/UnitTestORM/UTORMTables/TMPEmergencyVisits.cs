using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMTMPEmergencyVisits
	{
		private RandomField rnd;
		private ORMTMPEmergencyVisitsBase table;
		
		/// <summary>
		/// UT from table TMPEmergencyVisits
		/// </summary>
		public UTORMTMPEmergencyVisits()
		{
			rnd = new RandomField();
			table = new ORMTMPEmergencyVisitsBase();
		}

        public ORMTMPEmergencyVisitsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTTMPEmergencyVisitsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
