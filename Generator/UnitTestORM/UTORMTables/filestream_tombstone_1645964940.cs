using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMfilestream_tombstone_1645964940
	{
		private RandomField rnd;
		private ORMfilestream_tombstone_1645964940Base table;
		
		/// <summary>
		/// UT from table filestream_tombstone_1645964940
		/// </summary>
		public UTORMfilestream_tombstone_1645964940()
		{
			rnd = new RandomField();
			table = new ORMfilestream_tombstone_1645964940Base();
		}

        public ORMfilestream_tombstone_1645964940Record RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTfilestream_tombstone_1645964940RandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
