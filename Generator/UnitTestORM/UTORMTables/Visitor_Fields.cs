using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMVisitor_Fields
	{
		private RandomField rnd;
		private ORMVisitor_FieldsBase table;
		
		/// <summary>
		/// UT from table Visitor_Fields
		/// </summary>
		public UTORMVisitor_Fields()
		{
			rnd = new RandomField();
			table = new ORMVisitor_FieldsBase();
		}

        public ORMVisitor_FieldsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTVisitor_FieldsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
