using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroPassports_SaaS_Status
	{
		private RandomField rnd;
		private ORMsysroPassports_SaaS_StatusBase table;
		
		/// <summary>
		/// UT from table sysroPassports_SaaS_Status
		/// </summary>
		public UTORMsysroPassports_SaaS_Status()
		{
			rnd = new RandomField();
			table = new ORMsysroPassports_SaaS_StatusBase();
		}

        public ORMsysroPassports_SaaS_StatusRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroPassports_SaaS_StatusRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
