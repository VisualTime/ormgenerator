using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMTMPSchedulingCost
	{
		private RandomField rnd;
		private ORMTMPSchedulingCostBase table;
		
		/// <summary>
		/// UT from table TMPSchedulingCost
		/// </summary>
		public UTORMTMPSchedulingCost()
		{
			rnd = new RandomField();
			table = new ORMTMPSchedulingCostBase();
		}

        public ORMTMPSchedulingCostRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTTMPSchedulingCostRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
