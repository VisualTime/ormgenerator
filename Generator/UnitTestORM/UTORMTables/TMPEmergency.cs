using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMTMPEmergency
	{
		private RandomField rnd;
		private ORMTMPEmergencyBase table;
		
		/// <summary>
		/// UT from table TMPEmergency
		/// </summary>
		public UTORMTMPEmergency()
		{
			rnd = new RandomField();
			table = new ORMTMPEmergencyBase();
		}

        public ORMTMPEmergencyRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTTMPEmergencyRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
