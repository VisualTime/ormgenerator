using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMEventAccessAuthorization
	{
		private RandomField rnd;
		private ORMEventAccessAuthorizationBase table;
		
		/// <summary>
		/// UT from table EventAccessAuthorization
		/// </summary>
		public UTORMEventAccessAuthorization()
		{
			rnd = new RandomField();
			table = new ORMEventAccessAuthorizationBase();
		}

        public ORMEventAccessAuthorizationRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTEventAccessAuthorizationRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
