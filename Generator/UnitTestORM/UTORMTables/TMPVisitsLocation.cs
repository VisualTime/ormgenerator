using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMTMPVisitsLocation
	{
		private RandomField rnd;
		private ORMTMPVisitsLocationBase table;
		
		/// <summary>
		/// UT from table TMPVisitsLocation
		/// </summary>
		public UTORMTMPVisitsLocation()
		{
			rnd = new RandomField();
			table = new ORMTMPVisitsLocationBase();
		}

        public ORMTMPVisitsLocationRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTTMPVisitsLocationRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
