using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMTMPSHIFTDEFINITIONS
	{
		private RandomField rnd;
		private ORMTMPSHIFTDEFINITIONSBase table;
		
		/// <summary>
		/// UT from table TMPSHIFTDEFINITIONS
		/// </summary>
		public UTORMTMPSHIFTDEFINITIONS()
		{
			rnd = new RandomField();
			table = new ORMTMPSHIFTDEFINITIONSBase();
		}

        public ORMTMPSHIFTDEFINITIONSRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTTMPSHIFTDEFINITIONSRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
