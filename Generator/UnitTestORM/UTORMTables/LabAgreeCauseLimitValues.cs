using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMLabAgreeCauseLimitValues
	{
		private RandomField rnd;
		private ORMLabAgreeCauseLimitValuesBase table;
		
		/// <summary>
		/// UT from table LabAgreeCauseLimitValues
		/// </summary>
		public UTORMLabAgreeCauseLimitValues()
		{
			rnd = new RandomField();
			table = new ORMLabAgreeCauseLimitValuesBase();
		}

        public ORMLabAgreeCauseLimitValuesRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTLabAgreeCauseLimitValuesRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
