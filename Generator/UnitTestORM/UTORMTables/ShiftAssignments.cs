using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMShiftAssignments
	{
		private RandomField rnd;
		private ORMShiftAssignmentsBase table;
		
		/// <summary>
		/// UT from table ShiftAssignments
		/// </summary>
		public UTORMShiftAssignments()
		{
			rnd = new RandomField();
			table = new ORMShiftAssignmentsBase();
		}

        public ORMShiftAssignmentsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTShiftAssignmentsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
