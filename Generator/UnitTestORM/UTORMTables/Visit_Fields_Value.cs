using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMVisit_Fields_Value
	{
		private RandomField rnd;
		private ORMVisit_Fields_ValueBase table;
		
		/// <summary>
		/// UT from table Visit_Fields_Value
		/// </summary>
		public UTORMVisit_Fields_Value()
		{
			rnd = new RandomField();
			table = new ORMVisit_Fields_ValueBase();
		}

        public ORMVisit_Fields_ValueRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTVisit_Fields_ValueRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
