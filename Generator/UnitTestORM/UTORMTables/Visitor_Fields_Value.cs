using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMVisitor_Fields_Value
	{
		private RandomField rnd;
		private ORMVisitor_Fields_ValueBase table;
		
		/// <summary>
		/// UT from table Visitor_Fields_Value
		/// </summary>
		public UTORMVisitor_Fields_Value()
		{
			rnd = new RandomField();
			table = new ORMVisitor_Fields_ValueBase();
		}

        public ORMVisitor_Fields_ValueRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTVisitor_Fields_ValueRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
