using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMsysroPassports_PermissionsOverGroups
	{
		private RandomField rnd;
		private ORMsysroPassports_PermissionsOverGroupsBase table;
		
		/// <summary>
		/// UT from table sysroPassports_PermissionsOverGroups
		/// </summary>
		public UTORMsysroPassports_PermissionsOverGroups()
		{
			rnd = new RandomField();
			table = new ORMsysroPassports_PermissionsOverGroupsBase();
		}

        public ORMsysroPassports_PermissionsOverGroupsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTsysroPassports_PermissionsOverGroupsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
