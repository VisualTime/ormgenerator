using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMqueue_messages_1547868581
	{
		private RandomField rnd;
		private ORMqueue_messages_1547868581Base table;
		
		/// <summary>
		/// UT from table queue_messages_1547868581
		/// </summary>
		public UTORMqueue_messages_1547868581()
		{
			rnd = new RandomField();
			table = new ORMqueue_messages_1547868581Base();
		}

        public ORMqueue_messages_1547868581Record RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTqueue_messages_1547868581RandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
