using NUnit.Framework;
using ORMGenerator.Random;
using Robotics.DataLayer.ORM.Table;

namespace Robotics.DataLayer.ORM.Table
{
	[TestFixture]
	public class UTORMCausesDocuments
	{
		private RandomField rnd;
		private ORMCausesDocumentsBase table;
		
		/// <summary>
		/// UT from table CausesDocuments
		/// </summary>
		public UTORMCausesDocuments()
		{
			rnd = new RandomField();
			table = new ORMCausesDocumentsBase();
		}

        public ORMCausesDocumentsRecord RandomRecord()
        {
            var record = table.CreateRecord();

            for(var i=0;i<record.FieldList.Count;i++)
            { 
                //add random values
                rnd.GenerateRandom(ref record.FieldListToArray[i]);
            }

			return record;
        }

		[Test]
        public void UTCausesDocumentsRandomFields()
        {
            var record = RandomRecord();
            Assert.IsTrue(record.FieldListToArray[0].IsModified == true, "RandomFields: Modified");
            Assert.IsTrue(record.FieldListToArray[0].IsNull == false, "RandomFields: IsNull");
        }

	}
}
