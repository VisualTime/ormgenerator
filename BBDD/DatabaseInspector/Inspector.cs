﻿using System;
using System.Collections.Generic;
using DatabaseInspector.Database;

namespace DatabaseInspector
{
    public class Inspector
    {
        private Server server { get; set; }
        public bool state { get; set; }

        public Inspector()
        {     
        }

        public void Initialize(string pUsername, string pPasword, string pDataSorce, string pDatabase)
        {
            this.server=new Server(pUsername, pPasword, pDataSorce, pDatabase);
            state = this.server.Connect();
        }

        public List<string> GetAllTables()
        {
            return server.GetAllTables();
        }

        public List<TableColumn> GetAllColumnsFromTable(string pTableName)
        {
            return server.GetAllColumnsFromTable(pTableName);
        }
    }
}