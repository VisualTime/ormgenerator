﻿using SQLDMO;

namespace DatabaseInspector.Database
{
    /// <summary>
    /// represent column information
    /// </summary>
    public class TableColumn
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public int Size { get; set; }

        public bool IsPrimaryKey { get; set; }

        public bool IsIdentity { get; set; }

        public bool AllowNulls { get; set; }

        public TableColumn()
        {
        }

        public TableColumn(Column item)
        {
            Name = item.Name;
            Type = item.Datatype;
            IsPrimaryKey = item.InPrimaryKey;
            IsIdentity = item.Identity;
            Size = item.Length;
            AllowNulls = item.AllowNulls;
        }
    }
}