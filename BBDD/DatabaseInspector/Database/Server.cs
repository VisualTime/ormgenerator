﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLDMO;

namespace DatabaseInspector.Database
{
    public class Server
    {
        public string username { get; set; }
        public string password { get; set; }
        public string datasource { get; set; }
        public string databasename { get; set; }
        private SQLServer server { get; set; }
        private SQLDMO.Database database;


        public Server(string pUsername, string pPasword, string pDataSorce, string pDataBase)
        {
            this.username = pUsername;
            this.password = pPasword;
            this.datasource = pDataSorce;
            this.databasename = pDataBase;
            server = new SQLServer();
        }
        

        /// <summary>
        /// Connect to server
        /// </summary>
        /// <returns></returns>
        public bool Connect()
        {
            var result = true;

            try
            {
                server.Connect(datasource, username, password);
                database = (SQLDMO.Database)server.Databases.Item(databasename, null);
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Return all tables
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllTables()
        {
            var result = new List<string>();

            try
            {
                result = (from Table item in database.Tables where (!item.Name.StartsWith("sys") || item.Name.StartsWith("sysro")) && !item.Name.StartsWith("sqlagent_") select item.Name).ToList();
            }
            catch (Exception)
            {
            }

            return result;

        }

        /// <summary>
        /// Return all tables
        /// </summary>
        /// <returns></returns>
        public List<TableColumn> GetAllColumnsFromTable(string pTableName)
        {
            var result = new List<TableColumn>();

            try
            {
                foreach (Table item in database.Tables)
                {
                    if (item.Name == pTableName)
                    {
                        foreach (Column column in item.Columns)
                        {
                            result.Add(new TableColumn(column));
                        }
                        break;
                    }
                }
            }
            catch (Exception)
            {
            }

            return result;
        }
    }
}