﻿using DatabaseInspector;
using NUnit.Framework;

namespace UnitTestDatabaseInspector
{
    [TestFixture]
    public class Connect
    {
        private Inspector inspector;

        [SetUp]
        public void Initialize()
        {
            inspector=new Inspector();
        }

        [Test]
        public void ConnectOK()
        {
            inspector.Initialize("userVTLive", "userVTLive123","HWFPRG01\\SQL2014", "VisualTime");
            Assert.IsTrue(inspector.state,"ConnectOK");
        }

        [Test]
        public void ConnectERROR()
        {
            inspector.Initialize("userVTLive", "userVTLive123456", "HWFPRG01\\SQL2014", "VisualTime");
            Assert.IsFalse(inspector.state, "ConnectERROR");
        }

    }
}