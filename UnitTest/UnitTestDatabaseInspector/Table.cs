﻿using System.Linq;
using DatabaseInspector;
using NUnit.Framework;

namespace UnitTestDatabaseInspector
{
    [TestFixture]
    public class Table
    {
        private Inspector inspector;

        [SetUp]
        public void Initialize()
        {
            inspector = new Inspector();
        }

        [Test]
        public void GetTablesOK()
        {
            inspector.Initialize("userVTLive", "userVTLive123", "HWFPRG01\\SQL2014", "VisualTime");
            var tables = inspector.GetAllTables();
            Assert.IsTrue(tables.Any(), "GetTablesOK");
        }

        [Test]
        public void GetTablesERROR()
        {
            inspector.Initialize("userVTLive", "userVTLive123", "HWFPRG01\\SQL2014", "VisualTime2");
            var tables = inspector.GetAllTables();
            Assert.IsFalse(tables.Any(), "GetTablesERROR");
        }

        [Test]
        public void GetTablesColumnsOK()
        {
            inspector.Initialize("userVTLive", "userVTLive123", "HWFPRG01\\SQL2014", "VisualTime");
            var columns = inspector.GetAllColumnsFromTable("Employees");
            Assert.IsTrue(columns.Any(), "GetTablesOK");
        }
    }
}